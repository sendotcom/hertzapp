-- MySQL dump 10.13  Distrib 5.7.9, for Win32 (AMD64)
--
-- Host: 127.0.0.1    Database: hertz
-- ------------------------------------------------------
-- Server version	5.6.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `apikey`
--

DROP TABLE IF EXISTS `apikey`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apikey` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(40) NOT NULL,
  `level` int(2) NOT NULL,
  `ignore_limits` tinyint(1) NOT NULL DEFAULT '0',
  `is_private_key` tinyint(1) NOT NULL DEFAULT '0',
  `ip_addresses` text,
  `date_created` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_apikey_user` (`user_id`),
  CONSTRAINT `fk_apikey_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `apikey`
--

LOCK TABLES `apikey` WRITE;
/*!40000 ALTER TABLE `apikey` DISABLE KEYS */;
INSERT INTO `apikey` VALUES (1,'8$LV(96k0^ZdrwPpM%2bfcjq4QFzN@AHWUaGli53',0,0,0,NULL,0,1);
/*!40000 ALTER TABLE `apikey` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (1,'Noticias');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `location`
--

DROP TABLE IF EXISTS `location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `location` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(20) DEFAULT NULL,
  `name` varchar(65) DEFAULT NULL,
  `address_1` varchar(128) DEFAULT NULL,
  `address_2` varchar(65) DEFAULT NULL,
  `zip` varchar(20) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `city` varchar(45) DEFAULT NULL,
  `hours` varchar(128) DEFAULT NULL,
  `lat` varchar(20) DEFAULT NULL,
  `lon` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=211 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `location`
--

LOCK TABLES `location` WRITE;
/*!40000 ALTER TABLE `location` DISABLE KEYS */;
INSERT INTO `location` VALUES (109,'ACA50','ACAPULCO AEROPUERTO','BLVD DE LAS NACIONES S/NRNACIONAL','PLAN DE LOS AMATES','39300','01-800-709-5000','ACAPULCO','MON-SUN 06:00-24:00 HRS','16,761935','-99,754273'),(110,'AGU50','AGUASCALIENTES AEROPUERTO','CARRETERA PANAMERICANA KM.22','EJIDO BUENAVISTA DE PE?UELAS','20340','01-800-709-5000','AGUASCALIENTES','MON-SUN 07:00 -23:00 HRS','21,701533','-102,314012'),(111,'CAN50','CANCUN AEROPUERTO','CARRETERA CANCUN-CHETUMAL KM 22','BENITO JUAREZ ','75220','01-800-709-5000','CANCUN','MON-SUN 24/7 HRS.','21,041982','-86,874892'),(112,'CAN63','CANCUN PLAZA LAS AMERICAS','AVE. LEYES DE REFORMA MZA 1 L1','SUPERMANZANA 7','77500','01-800-709-5000','CANCUN','MON-SUN 08:00-1800 HRS ','21,149335','-86,822017'),(113,'CAN66','CANCUN WALMART','AV COBA LOTE 2 MZ 2 ','SUPERMANZANA 21','77500','01-800-709-5000','CANCUN','MON-SUN 07:00-22:00 HRS','21,155213','-86,833279'),(114,'CAN67','CANCUN  HOTEL VILLAS DEL PALMAR','CARRETERA A PUNTA SAM KM.5.2 MZ 9 LOTE 3 ','SUPERMANZANA 2','77440','01-800-709-5000','CANCUN','MON-SUN 08:00 - 18:00 HRS','21,232018','-86,803783'),(115,'CAN69','CANCUN HOTEL REAL INN','BOULEVARD KUKULKAN KM.5.5 ','ZONA HOTELERA ','77500','01-800-709-5000','CANCUN','MON-SUN 07:00-17:00 HRS','21,142931','-86,777421'),(116,'CAN71','CANCUN LA ISLA','BOULEVARD KUKULKAN KM 12.5 ','ZONA HOTELERA ','77500','01-800-709-5000','CANCUN','MON-SUN 07:00-22:00 HRS','21,110216','-86,763405'),(117,'CAN72','CANCUN XCARET-CENTRO DE CONVENCIONES','BOULEVARD KUKULKAN KM 8.5','ZONA HOTELERA ','77500','01-800-709-5000','CANCUN','MON-SUN 07:00-22:00 HRS','21,135906','-86,746916'),(118,'CAN76','CANCUN PLAZA FLAMINGO','BOULEVARD KUKULKAN KM 11.5','ZONA HOTELERA ','77500','01-800-709-5000','CANCUN','MON-SUN 07:00-22:00 HRS','21,116246','-86,757997'),(119,'CAN80','P.MORELOS HOTEL ROYALTON','CARRETERA FEDERAL 307 KM 332','SUPERMANZANA 31','77580','01-800-709-5000','CANCUN','MON-SUN 07:00 - 19:00 HRS','20,944457','-86,839053'),(120,'CDC50','CIUDAD DEL CARMEN AEROPUERTO','AVENIDA 31 S/N ','ZONA FEDERAL','24119','01-800-709-5000','CIUDAD DEL CARMEN','MON-FRI 07:00 -13:00 HRS & 15:00 -19:00HRS./  SAT-SUN 10:00 -18:00HRS','18,651137','-91,802433'),(121,'CEN50','OBREGON AEROPUERTO','CARRETERA CIUDAD OBREG?N-NAVOJOA KM 18.40','','85000','01-800-709-5000','CIUDAD OBREGON','MON-SUN 06:00 - 22:00 HRS','27,398112','-109,83342'),(122,'CPE50','CAMPECHE AEROPUERTO','AV. LOPEZ PORTILLO SN ','AVIACION','24570','01-800-709-5000','CAMPECHE','MON-SUN 24/7 HRS','19,814044','-90,503813'),(123,'CTM50','CHETUMAL AEROPUERTO','PROLONGACION AVE. REVOLUCION N660','INDUSTRIAL','77049','01-800-709-5000','CHETUMAL','MON-SUN 24/7 HRS','18,506437','-88,324217'),(124,'COA50','COATZACOALCOS AIRPORT','CARRETERA ANTIGUA A MINATOTLAN KM. 21.5','CANTICAS','96340','01-800-709-5000','COATZACOALCOS','MON-FRI 07:00-20:30 HRS /SAT 11:00 - 19:00 HRS / SUN 11:00-20:30 HRS','18,102349','-94,577087'),(125,'COZ50','COZUMEL AEROPUERTO','AVE. 65 Y BOULEVARD AEROPUERTO COZUMEL','ZONA CENTRO','77600','01-800-709-5000','COZUMEL','MON-SUN 08:00 ? 19:00 HRS','20,511561','-86,930605'),(126,'CSL60','CABO SAN LUCAS CENTRO','BOULEVARD LAZAROCARDENAS S/N LOC. 5 Y 6','ZONA CENTRO','23400','01-800-709-5000','CABO SAN LUCAS','MON-SUN 08:00-18.00 HRS','22,891728','-109,908791'),(127,'CJS50','CIUDAD JUAREZ AEROPUERTO','CARRETERA PANAMERICANA KM 18.5','FRACC. COLINAS DE JUAREZ','32690','01-800-709-5000','CIUDAD JUAREZ','MON-SUN 24/7 HRS','31,635742','-106,435875'),(128,'CJS60','CIUDAD JUAREZ CENTRO','PASEO TRIUNFO DE LA REPUBLICA 2408','CENTRO','32330','01-800-709-5000','CIUDAD JUAREZ','MON-SAT 0900- 18:000 HRS ','31,737186','-106,455583'),(129,'CSL63','LOS CABOS HOTEL VILLAS DEL PALMAR','CAMINO VIEJO A SAN JOSE KM 0.5 ','EL MEDANO','23450','01-800-709-5000','CABO SAN LUCAS','MON-SUN 08:00-14.00 &  16:00-18:00 HRS','22,895624','-109,897106'),(130,'CUL50','CULIACAN AEROPUERTO','CARRETERA NAVOLATO KM 4.5 ','BACHIGUALATO','80130','01-800-709-5000','CULIACAN','MON-SUN 07:00  - 22:00 HRS.','24,766755','-107,469743'),(131,'CUU50','CHIHUAHUA AEROPUERTO','GRAL. ROBERTO FIERRO ','CENTRO','31390','01-800-709-5000','CHIHUAHUA','MON-SUN 0600 -22:00 HRS','28,703897','-105,968985'),(132,'CUU60','CHIHUAHUA CENTRAL','AVE. TEOFILO BORUNDA 514 ','CENTRO','31000','01-800-709-5000','CHIHUAHUA','MON-SUN 0700 - 22:00 HRS','28,641088','-106,077216'),(133,'DGO50','AUTOPISTA GOMEZ PALACIOS DURANGO','KM 15.5  ','','34319','01-800-709-5000','DURANGO','MON-SUN 07:00 - 23:00 HRS','24,126219','-104,533965'),(134,'ENS60','ENSENADA HOTEL BAHIA','AVE. A LOPEZ MATEOS 850 ','ZONA CENTRO','22800','01-800-709-5000','ENSENADA','MON-SUN 08:00 - 18:00 HRS','31,861344','-116,623879'),(135,'FCAN50','CANCUN AEROPUERTO ','CARRETERA CANCUN-CHETUMAL KM 22','AEROPUERTO','75220','01-800-709-5000','CANCUN','MON -SUN 07:00- 22:00 HRS','21,040266','-86,870402'),(136,'FPDC60','PLAYA DEL CARMEN HOTEL TUKAN','5TA AV. NORTE S/N ','ZONA CENTRO','77710','01-800-709-5000','PLAYA DEL CARMEN','MON-SUN 07:00 - 22:00 HRS.','20,629695','-87,071337'),(137,'FSJD50','SAN JOSE DEL CABO AEROPUERTO','CARRETERA TRANSPENINSULAR KM 435 BODEGA 5 SUITE 1','ZONA INDUSTRIAL AEROPUERTO','23400','01-800-709-5000','SAN JOSE DEL CABO','MON-SUN 07:00 -23:00 HRS.','23,161084','23,161084'),(138,'GDL50','GUADALAJARA AEROPUERTO','CARRETERA GUADALAJARA CHAPALA KM 17.5','TLAJOMULCO DE ZU?IGA','45659','01-800-709-5000','GUADALAJARA','MON-SUN 24/7 HRS','20,525668','-103,302516'),(139,'GDL60','GUADALAJARA HOTEL CAMINO REAL','AVE. NI?O OBRERO DON BOSCO VALLARTA ','CHAPALITA ','45040','01-800-709-5000','GUADALAJARA','MON-SAT 08: 00-20:00 HRS/ SUN 09:00 - 17:00 HRS','20,674959','-103,409241'),(140,'GDL70','GUADALAJARA HOTEL REAL INN ','AVE. MARIANO OTERO #1326','JARDINES DE SAN IGNACIO','45040','01-800-709-5000','GUADALAJARA','MON-SAT 09:00 - 14:00 & 16:00- 19:00 / SUN 09:00 A 17:00 HRS','20,655598','-103,391399'),(141,'GDL71','GUADALAJARA CARSHOP','AVE. IGNACIO L.VALLARTA #6220-B','ZONA CENTRO','83270','01-800-709-5000','GUADALAJARA','MON-SAT 09: 00  17:00 HRS / SUN CLOSED.','20,687706','-103,445174'),(142,'HER61','HERMOSILLO HOTEL HAMPTON','PASEO RIO SONORA #172 ','PROYECTO RIO SONORA ','83270','01-800-709-5000','HERMOSILLO','MON-FRI 07:00 - 20:00  / SAT 08:00 -20:00 / SUN 09:00 -14:00 HRS.','29,068903','-110,951617'),(143,'HER62','HERMOSILLO QUIROGA','BOULEVARD QUIROGA #7 LOC.1 ESQUINA BLVD. GARCIA MORALES','QUINTA EMILIA','83220','01-800-709-5000','HERMOSILLO','MON-SUN 7:00 - 20:00 HRS','29,096048','-111,022302'),(144,'HUA50','HUATULCO AEROPUERTO','CARRETERA COSTERA PINOTERA SALINA CRUZ KM 237 S/N LOC.15','EL ZAPOTE ','70980','01-800-709-5000','HUATULCO','MON-SAT 08:30 - 18:00 HRS / SUN 09:00  - 21:00 HRS.','15,772159','-96,258282'),(145,'HUA61','HUATULCO JUCHITAN','CAMINO A LA COL. 5 DE ABRIL KM. 0.5','','70000','01-800-709-5000','HUATULCO','MON-SAT 09:00-17:00 HRS /  SUN CLOSED','16,452158','-95,027221'),(146,'HUA62','HUATULCO ZONA HOTELERA','PLAZA PUNTA TANGOLUNDA LOTE 3 LOC 5 ','BAHIAS DE TANGOLUNDA','70989','01-800-709-5000','HUATULCO','MON-SAT 8:30 - 13:30 & 15:30 - 19:30 HRS / SUN CLOSED','15,775406','-96,099607'),(147,'IXT50','IXTAPA AEROPUERTO','CARRETERA NACIONAL A ACAPULCO DESVIACION AL AEROPUERTO S/N','AEROPUERTO','40880','01-800-709-5000','IXTAPA','MON-SUN 08:30 - 18:30 HRS','17,606382','-101,463363'),(148,'LAP50','LA PAZ AEROPUERTO','CARRETERA TRANSPENINSULAR KM. 13 ','AEROPUERTO','23201','01-800-709-5000','LA PAZ','MON-SUN 07:00 - 24:00 HRS','24,076284','-110,367384'),(149,'LAP60','LA PAZ WALMART ','CARRETERA TRANSPENINSULAR Y AVE. AGUSTIN OLACHEA S/N ','EL ZACATAL','23088','01-800-709-5000','LA PAZ','MON-SUN 08:00 -19:00 HRS','24,118111','-110,343747'),(150,'LAP68','LA PAZ MALECON ','AVE. ALVARO OBREGON #542 ENTRE GRAL MANUEL MARQUEZ Y MANUEL PINEDA','MALECON ','23000','01-800-709-5000','LA PAZ','MON-SUN-08:00 -19:00 HRS','24,155253','-110,32257'),(151,'LEO50','LEON AEROPUERTO','CARRETERA SILAO-LEOB KM 5.5 ','NUEVO MEXICO','36270','01-800-709-5000','LEON ','MON-SUN 05:00 - 23:30 HRS','20,985819','-101,478796'),(152,'LTO50','LORETO AIRPORT','CARRETERA TRANSPENINSULAR KM. 7','ZONA CENTRO','23880','01-800-709-5000','LORETO','MON,WED, FRI  10:00 - 14:00 HRS / -TUE, THU, SUN 10:00 - 17:00 HRS','25,992346','-111,352903'),(153,'LTO60','LORETO VILLAS DEL PALMAR','CARR. TRASNPENINSULAR KM 84','','23880','01-800-709-5000','LORETO','MON-SUN 09:00 - 17:00 HRS','25,717963','-111,233293'),(154,'MER50','MERIDA AIRPORT','CARRETERA MERIDA UMAN KM 4.5 CALLE 23 ','MANUEL CRESENCIO REJON','97291','01-800-709-5000','MERIDA','MON -SUN 05:30 -24:00 HRS','20,934242','-89,666353'),(155,'MER61','MERIDA HOTEL FIESTA AMERICANA','AVE. COLON Y CALLE 60 ','PASEO DE MONTEJO CENTRO','97000','01-800-709-5000','MERIDA','MON -FRY 07:00 - 22:00 HRS / SAT- SUN 9:00 - 17:00 HRS.','20,986304','-89,619227'),(156,'MEX50','MEXICO AIRPORT TERMINAL #1','AVE. CAPITAN CARLOS LEON S/N Y CALLE SONORA ENTRE PUERTA 1 Y 2','PE?ON DE LOS BA?OS','15620','01-800-709-5000','MEXICO D.F','MON - SUN 24/7 HRS','19,433614','-99,085972'),(157,'MEX51','MEXICO AIRPORT TERMINAL #2','AVE. CAPITAN CARLOS LEON LOCAL AS-11 ENTRE PUERTA 3 Y 4 ','PE?ON DE LOS BA?OS','15520','01-800-709-5000','MEXICO D.F ','MON - SUN 24/7 HRS','19,421555','-99,07801'),(158,'MEX53','MEXICO RETURN CENTER','CALLE SONORA S/N ','PE?ON DE LOS BA?OS','15620','01-800-709-5000','MEXICO D.F','MON - SUN 24/7 HRS','19,440598','-99,076659'),(159,'MEX60','MEXICO DF CAMPOS ELISEOS','AVE. CAMPOS ELISEOS#295  ESQ. JULIO VERNE ','POLANCO ','11550','01-800-709-5000','MEXICO D.F','MON-FRI 07:00-22:00 HRS / SAT - SUN 08:00-15:00HRS','19,427175','-99,196992'),(160,'MEX63','MEXICO  HOTEL CAMINO REAL','LEBINIT 100 LOCAL 11 ','NUEVA ANZURES','11590','01-800-709-5000','MEXICO D.F','MON-FRI 07:00-20:00 HRS / SAT -SUN 08:00-15:00 HRS','19,427686','-99,177759'),(161,'MEX66','MEXICO PATRIOTISMO','AVE. PATRIOTISMO #32 ','ESCANDON ','11800','01-800-709-5000','MEXICO D.F','MON-FRI 08:00 -20:00 HRS / SAT  08:00-15:00 / SUN 09:00 - 15:00 HRS','19,404909','-99,178342'),(162,'MEX67','MEXICO ACOXPA','CALZADA ACOXPA #438','EX HACIENDA COAPA','14300','01-800-709-5000','MEXICO D.F','MON-FRI 09:00-14:00 HRS & 16.00 - 19:00 / SAT 09:00-15:00 HRS / SUN - CLOSED','19,298449','-99,136624'),(163,'MEX68','EDO. MEXICO SATELITE','BOULEVARD MANUEL AVILA CAMACHO #1834 LOC 1832','CIUDAD SATELITE','53100','01-800-709-5000','MEXICO D.F','MON-FRI 08:00 - 20:00 HRS / SAT - SUN 08:00-15:00 HRS.','19,500423','-99,23725'),(164,'MEX69','MEXICO DF. QUEVEDO','AVE. MIGUEL ANGEL QUEVEDO #175 ESQ AVE. UNIVERSIDAD','OXTOPULCO','4390','01-800-709-5000','MEXICO D.F','MON-FRI 09:00-14:00 & 16.00- 19:00 HRS / SAT 09:00 - 15:00 HRS  / SUN-CLOSED','19,345245','-99,181344'),(165,'MEX73','MEXICO AV.UNIVERSIDAD ','BAJO PUENTE X CTO INTERIOR (BICENTENARIO) RIO CHURUBUSCO CRUCE CON EJE 3 PONIENTE ','DEL VALLE ','54080','01-800-709-5000','MEXICO CITY','MON-FRI 09:00 -14:00 & 16.00 -19:00 HRS/ SAT 09:00 - 15:00 HRS / SUN-CLOSED','19,359142','-99,170619'),(166,'MEX76','MEXICO C.REAL STA FE','AVE. GUILLERMO GONZALEZ CAMARENA NO.300 ','ALVARO OBREGON ','1210','01-800-709-5000','MEXICO CITY','MON-FRI 09:00 -14:00 & 16:00-19:00 HRS. / SAT 09:00- 15:00 HRS. / SUN-CLOSED','19,367641','-99,261472'),(167,'MEX77','MEXICO HOTEL REAL INN PERINORTE','VIA. DR. GUSTAVO BAZ NO.4001','INDUSTRIAL SAN NCOLAS','54030','01-800-709-5000','MEXICO CITY','MON-FRI 09:00 -14:00 & 16:00 - 19:00 HRS. /  SAT 09:00-15:00 HRS. / SUN CLOSED','19,549539','-99,207393'),(168,'MEX78','MEXICO HOTEL CAMINO REAL PEDREGAL','PERIFERICO SUR 3647 LA MAGDALENA CONTRERAS','HEROES DE PADIERNA','10700','01-800-709-5000','MEXICO CITY','MON-FRI 09:00 - 14:00 & 16:00- 19:00 / SAT 09:00 A 17:00 HRS / SUN CLOSED','19,314296','-99,221039'),(169,'MEX79','MEXICO CNTRAL DE ABASTOS','AVE. LEYES DE REFORMA MZA 5 LOTE 15 ','CENTRAL DE ABASTOS','9040','01-800-709-5000','MEXICO CITY','MON-FRI 09:00 - 14:00 & 16:00- 19:00 / SAT 09:00 A 13:00 HRS /SUN CLOSED','19,3800725','-99,0877288'),(170,'MEX80','MEXICO ANTARA','AV.EJ?RCITO NACIONAL 843-B LOCAL SE- ','COLONIA GRANADA ','11520','01-800-709-5000','MEXICO CITY','MON-FRI 08:00 - 20:00 HRS / SAT - SUN 09:00-15:00 HRS & 16:00 - 18:00 HRS.','19,438771','-99,203602'),(171,'MLM50','MORELIA AEROPUERTO','CARRETERA MORELIA ZINAPECUARO KM 27','ALVARO OBREGON ','58920','01-800-709-5000','MORELIA','MON-SUN 7:00 - 23:00 HRS.','19,843747','-101,027306'),(172,'MTY50','MONTERREY AIRPORT','CARRETERA MIGUEL ALEMAN KM. 24 ','APOCACA','66600','01-800-709-5000','MONTERREY','MON-SUN 24/7 HRS','25,777178','-100,115038'),(173,'MTY52','MONTERREY AIRPORT TERMINAL B','CARRETERA MIGUEL ALEMAN KM. 24 ','APOCACA','66600','01-800-709-5000','MONTERREY','MON-SUN 7:00 - 23:00 HRS.','25,776817','-100,115852'),(174,'MTY62','MONTERREY HOTEL SHERATON ','AVE. MELCHO O CAMPO 323 ','ZONA CENTRO','64000','01-800-709-5000','MONTERREY','MON-SAT 8:00 -13:00 & 14:00- 18:00 HRS / SUN-CLOSED','25,665959','-100,313717'),(175,'MTY66','MONTERREY MILLENIUM','BOULEVARD T.L.C ESQ AVE DECADA','PARQUE INDUSTRIAL MILLENIUM','66600','01-800-709-5000','MONTERREY','MON-SAT 8:00 -13:00 & 14:00- 18:00 HRS / SUN-CLOSED','25,777178','-100,115038'),(176,'MXI50','MEXICALI AIRPORT','CARRETERA MESA DE ANDRADE KM. 23.5','MARIANO ABASOLO','21600','01-800-709-5000','MEXICALI','MON -SUN 06:00 - 00:15 HRS','32,628648','-115,247856'),(177,'MXI60','MEXICALI DOWN TOWN','AVE. ZARAGOZA #2032 Y CALLE L ','COLONIA NUEVA','21100','01-800-709-5000','MEXICALI','MON -SUN 07:00 - 20:00 HRS.','32,661672','-115,454782'),(178,'MZT50','MAZATLAN AEROPUERTO','CARRETERA INTERNACIONAL AL SUR S/N ','','82000','01-800-709-5000','MAZATLAN','MON-SUN 07:00 - 23:00 HRS','23,167265','-106,267054'),(179,'MTZ63','MAZATLAN SABALO COUNTRY CLUB','AVE. CAMARON SABALO #1500 LOCAL#5','SABALO COUNTRY CLUB','82100','01-800-709-5000','MAZATLAN','MON-SUN 09:00 - 19:00 HRS','23,264926','-106,465161'),(180,'OAX50','OAXACA AEROPUERTO','CARRETERA OAXACA-PTO ANGEL KM 7.5','SANTA CRUZ XOXOCOTLAN','71230','01-800-709-5000','OAXACA','MON-SUN 24/7 HRS','17,001882','-96,721851'),(181,'PAZ50','POZA RICA AEROPUERTO','CARRETERA MEXICO-TUXPAN KM 8 ','TIHUATLAN','92900','01-800-709-5000','POZA RICA','MON-SUN  07:00 - 19:00 HRS','20,599375','-97,461551'),(182,'PDC60','PLAYA CARMEN PLAZA TUKAN','AVE. 10 NORTE ESQ. 14 NORTE BIS ','ZONA CENTRO ','77717','01-800-709-5000','PLAYA DEL CARMEN','MON -SUN 07:00 -22:00 HRS.','20,629571','-87,071494'),(183,'PDC61','PLAYA CARMEN PLAZA PARAISO','AV. 10 SUR  Y CALLE 3 SUR','PLAYACAR ','77710','01-800-709-5000','PLAYA DEL CARMEN','MON -SUN 07:00 -22:00 HRS','20,629571','-87,071494'),(184,'PDC63','PLAYA DEL CARMEN PLAYACAR','AVE. PASEO XAMAN-HA MZA 25','PLAYACAR FASE II','77710','01-800-709-5000','PLAYA DEL CARMEN','MUN-SUN 09:30 - 17:00 HRS','20,609175','-87,090652'),(185,'PDC76','PLAYA DEL CARMEN ADO ','AVE. JUAREZ ENTRE 5TA Y 10 AV','CENTRO','77710','01-800-709-5000','PLAYA DEL CARMEN','MON-SAT 07:00-22:00 HRS / SUN-CLOSED','20,622695','-87,075142'),(186,'PDC78','PLAYA CARMEN LA QUINTA','5TA AV. POR CALLE 28 NORTE ','SUPERMANZANA 69 ','77710','01-800-709-5000','PLAYA DEL CARMEN','MON-SUN 07:00 -21:00 HRS','20,632405','-87,068231'),(187,'PUE50','PUEBLA AEROPUERTO','CARRETERA FEDERAL MEXICO-PUEBLA KM 9.5','HUJOTZINGO','74160','01-800-709-5000','PUEBLA','MON -FRI 06:00-23:00 HRS / SAT- 06:00 -23:00 HRS  / SUN 09:00-22:00 HRS.','19,16398','-98,375949'),(188,'PUE60','PUEBLA CENTRO','BOULEVARD ATLXCO NO. 307 ','LA PAZ','72160','01-800-709-5000','PUEBLA','MON-FRI 08:00-20:00 HRS / SAT- 09:00-17:00 HRS / SUN 09:00-17:00','19,054572','-98,222438'),(189,'PVR50','PUERTO VALLARTA AEROPUERTO','CARRETERA A TEPIC KM 7.5 ','','48311','01-800-709-5000','PUERTO VALLARTA','MON -SUN 06:00 - 22:00 HRS','20,678071','-105,248664'),(190,'PVR74','PTO VALLARTA CENTRO','PASEO LOS COCOTEROS #85 SUR','ZONA HOTELERA','63732','01-800-709-5000','NUEVO VALLERTA','MON-SUN 08:00 - 22:00 HRS.','20,678147','-105,248847'),(191,'QUE51','QUERETARO AEROPUERTO','CARRETERA ESTATAL QUERETARO - TEQUISQUIAPAN NO 22500','','76270','01-800-709-5000','QUERETARO','MON -SUN 07:00 -23:00 HRS','20,678071','-105,248664'),(192,'QUE63','QUERETARO CONSTITUYENTES','CALLE 61-A ','SAN FRANCISQUITO','76040','01-800-709-5000','QUERETARO','MON-SUN 08:00 -20:00 HRS','20.584.448,00','-100,391216'),(193,'REX50','REYNOSA AEROPUERTO','CARRETERA A MATAMOROS-MAZATLAN S/N KM.83','ALMAGER','88780','01-800-709-5000','REYNOSA','MON-SUN 07:00 - 22:00 HRS.','26,012012','-98,227238'),(194,'SAL50','SALTILLO AEROPUERTO','CARRETERA SALTILLO - MOTERREY KM 13.5','RAMOS ARIZPE ','25900','01-800-709-5000','SALTILLO','MON -FRI 07:00 - 22:00 HRS. /  SAT 07:00-1400 HRS / SUN 14:00 ? 22:00 HRS.','25,549011','-100,932157'),(195,'SAN50','SAN JOSE DEL CABO AEROPUERO','CARRETERA TRANSPENINSULAR KM 435 BODEGA 5 SUITE 1','ZONA INDUSTRIAL AEROPUERTO','23400','01-800-709-5000','SAN JOSE DEL CABO','MON-SUN 07:00-23.00 HRS','23,161084','-109,712296'),(196,'SLP50','SAN LUIS POTOSI AEROPUERTO','CARRETERA A MATEHUALA KM 9.5','ZONA DE ARRENDADORAS','78430','01-800-709-5000','SAN LUIS POTOSI','MON-SUN 06:00-23:30 HRS','22,255674','-100,936636'),(197,'SLP60','SAN LUIS  HOTEL HOLLIDAY INN','CARRETERA CENTRAL KM 420 ','ZONA INDUSTRIAL ','78090','01-800-709-5000','SAN LUIS POTOSI','MON -SAT 08:00-20:00 HRS / SUN 09:00-17:00 HRS.','22,132209','-100,929586'),(198,'TAM50','TAMPICO AEROPUERTO','ADOLFO LOPEZ MATEOS BLVD 1001','','89339','01-800-709-5000','TAMPICO','MON-SUN 07:00 - 22:00 HRS.','22,289117','-97,86966'),(199,'TIJ51','TIJUANA AEROPUERTO','CARRETERA AEROPURTO S/N ','NUEVATIJUANA','22435','01-800-709-5000','TIJUANA','MON-FRI 07:30 - 23:00 HRS / SAT-SUN 07:30-23:00 HRS','32,546044','-116,975252'),(200,'TIJ60','TIJUANA HOTEL CAMINO REAL','PASEO DE LOS HEROS NO. 9902','ZONA URBANA RIO TIJUANA','22010','01-800-709-5000','TIJUANA','MON-SAT 08:00 - 17:00 HRS','32,525012','-117,016513'),(201,'TOL51','TOLUCA AEROPUERTO','BOULEVARD AEROPUERTO S/N ','SAN PEDRO TOLTEPEC ','50150','01-800-709-5000','TOLUCA','MON-SUN 07:00 -23:00 HRS.','19,338332','-99,570597'),(202,'TRC50','TORREON AEROPURTO','CARRETERA TORREON SAN PEDRO KM 9','CENTRO','27016','01-800-709-5000','TORREON','MON-SUN 07:00 - 22:00 HRS.','25,562721','-103,39889'),(203,'TUL60','TULUM CENTRO','CARRETERA FEDERAL CANCUN-TULUM','SUPERMANZANA10','77780','01-800-709-5000','TULUM','MON-SUN 07:00-19:00 HRS','20,214468','-87,452686'),(204,'TUL61','TULUM H.ANA Y JOSE','CARR CANCUN-TULUM BOCA PAILA KM 7','PUNTA PIEDRA','77780','01-800-709-5000','TULUM','MON-SUN 07:00-18:00 HRS','20,157124','?-87.455700'),(205,'TUX50','TUXTLA AEROPUERTO','CARRETERA VERGEL LM 12.48 S/N','CHIAPA DE CORZO','29050','01-800-709-5000','TUXTLA GUTIERREZ','MON-SUN 07:00-23:00 HRS','16,560896','-93,019451'),(206,'TUX60','TUXTLA HOTEL CAMINO REAL','BOULEVARD BELISARIO DOMINGUEZ ','SANTA ELENA ','29060','01-800-709-5000','TUXTLA GUTIERREZ','MON-SAT 09:00-14:00 & 16:00- 19:00 HRS /  SUN 09:00-14:00 HRS.','16,753446','-93,144239'),(207,'VER50','VERACRUZ AEROPUERTO','CARRETERA VERACRUZ-XALAPA KM 13.5','','91698','01-800-709-5000','VERACRUZ','MON-SUN 07:00 -23:00 HRS.','19,142321','-96,183577'),(208,'VIL50','VILLAHERMOSA AEROPUERTO','CARRETERA VILLAHERMOSA - MACUSPANA KM 13','DOS MONTES','86280','01-800-709-5000','VILLAHERMOSA','MON-SUN 07:00-10:30 HRS.','17,992907','-92,818903'),(209,'VIL62','VILLAHERMOSA SUPERAMA','PROLONGACION 27 DE FEBRERO 3016','COLONIO GALAXIA / TABASCO 2000','86035','01-800-709-5000','VILLAHERMOSA','MON-FRI 08:00 - 20:00 HRS / SAT- 09:00 - 17:00 HRS / SUN CLOSED ','18,000885','-92,947327'),(210,'ZCL50','ZACATECAS AEROPUERTO','CARRETERA PANAMERICANA TRAMO ZACATECAS FRESNILLO','VICTOR ROSALES CALERA','98500','01-800-709-5000','ZACATECAS','MON-SUN 07:00 - 22:00 HRS.','','');
/*!40000 ALTER TABLE `location` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notification`
--

DROP TABLE IF EXISTS `notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(150) NOT NULL,
  `description` varchar(10000) DEFAULT NULL,
  `creation_date` datetime DEFAULT NULL,
  `version_code` varchar(20) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_notification_user_idx` (`user_id`),
  CONSTRAINT `fk_notification_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notification`
--

LOCK TABLES `notification` WRITE;
/*!40000 ALTER TABLE `notification` DISABLE KEYS */;
INSERT INTO `notification` VALUES (1,'Test 1','test 1','2016-11-08 13:14:31','1478607271',1,1,0),(2,'Another push','Este es otro push','2016-11-08 14:21:26','1478611286',1,1,0),(3,'a','a','2016-11-10 09:38:12','1478767092',1,1,0),(4,'b','b','2016-11-10 09:38:16','1478767096',1,1,0),(5,'c','c','2016-11-10 09:38:24','1478767104',1,1,0),(6,'d','d','2016-11-10 09:38:28','1478767108',1,1,0),(7,'e','e','2016-11-10 09:38:33','1478767113',1,1,0),(8,'f','f','2016-11-10 09:38:37','1478767117',1,1,0),(9,'g','g','2016-11-10 09:38:44','1478767124',1,1,0),(10,'h','h','2016-11-10 09:38:48','1478767128',1,1,0),(11,'i','i','2016-11-10 09:38:52','1478767132',1,1,0);
/*!40000 ALTER TABLE `notification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notification_image`
--

DROP TABLE IF EXISTS `notification_image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notification_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `notification_id` int(11) NOT NULL,
  `path` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_notification_image_notification_idx` (`notification_id`),
  CONSTRAINT `fk_notification_image_notification` FOREIGN KEY (`notification_id`) REFERENCES `notification` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notification_image`
--

LOCK TABLES `notification_image` WRITE;
/*!40000 ALTER TABLE `notification_image` DISABLE KEYS */;
INSERT INTO `notification_image` VALUES (4,1,'/uploads/push/c4ca4238a0b923820dcc509a6f75849b/6a745b6ba94af3c2d282abb7decca9d5.jpg');
/*!40000 ALTER TABLE `notification_image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rol`
--

DROP TABLE IF EXISTS `rol`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rol` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rol`
--

LOCK TABLES `rol` WRITE;
/*!40000 ALTER TABLE `rol` DISABLE KEYS */;
INSERT INTO `rol` VALUES (1,'Admin'),(2,'Gestor');
/*!40000 ALTER TABLE `rol` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `rol_id` int(11) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_user_rol_idx` (`rol_id`),
  CONSTRAINT `fk_user_rol` FOREIGN KEY (`rol_id`) REFERENCES `rol` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'Admin','admin@hertz.com','827ccb0eea8a706c4c34a16891f84e7b',1,0),(2,'test_admin','test_admin@hertz.com','827ccb0eea8a706c4c34a16891f84e7b',2,0);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_device`
--

DROP TABLE IF EXISTS `user_device`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_device` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `surname` varchar(45) DEFAULT NULL,
  `email` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `arn` varchar(256) DEFAULT NULL,
  `platform` varchar(45) DEFAULT NULL,
  `apikey` varchar(255) DEFAULT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_device`
--

LOCK TABLES `user_device` WRITE;
/*!40000 ALTER TABLE `user_device` DISABLE KEYS */;
INSERT INTO `user_device` VALUES (1,'Test_Android',NULL,'test_android@hertz.com','827ccb0eea8a706c4c34a16891f84e7b',NULL,'android','t3kVK_1my/Jx!7z.[opHg{C6a}nG,db5M$)ZD#-F',0),(2,'iOS','Test','test_ios@hertz.com','827ccb0eea8a706c4c34a16891f84e7b',NULL,'ios',NULL,0);
/*!40000 ALTER TABLE `user_device` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-12-20 14:25:47
