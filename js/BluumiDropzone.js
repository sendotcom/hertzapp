/**
 * Plugin Dropzone para imagenes
 * @Author Bluumi
 * @Version 1.0
 */
$.fn.BluumiDropzone = function(options) {
	var settings = $.extend({
		id: 	'',
        icon:	'fa-photo',
		title:	'_t("upload")',
		image:	'',
		accept:	'image/jpeg,image/png',
    }, options );
	var default_image = '_p("images/logo_lg.png")';
	var isNew = settings.image != null && settings.image.length>0 ? false : true;
	var container_id = $(this).attr('id');
	var accept_types = settings.accept.split(',');
	
	if(settings.id==null || settings.id.length<=0){
		$(this).hide();
		console.error('Es obligatorio id para BluumiDropzone');
	}else{
		//Clases
		if(!$(this).hasClass('btn-upload')){
			$(this).addClass('btn-upload');
		}
		if(!$(this).hasClass('bluumi-dropzone')){
			$(this).addClass('bluumi-dropzone');
		}
		
		/*
		 * Añadimos contenido del Plugin
		 */
		$(this).append('\
			<div class="thumbnail">\
				<img src="'+(isNew ? default_image : settings.image)+'" id="'+settings.id+'_thumbnail"/>\
				<a id="'+settings.id+'_remove" class="remove text-danger" '+(isNew ? 'style="display: none;"' : '')+'><i class="fa fa-times-circle fa-2x"></i></a>\
			</div>\
			<i class="fa '+settings.icon+' fa-3x"></i><br/>\
			<label>'+settings.title+'</label>\
			<input type="file"\ id="'+settings.id+'" name="'+settings.id+'" class="form-control" data-container="'+settings.id+'_thumbnail" accept="'+settings.accept+'"/>\
		');
		
		/**
		 * Change de la imagen
		 */
		$('.bluumi-dropzone input[type="file"]').unbind("change");
		$('.bluumi-dropzone input[type="file"]').change(function(e){
			var input = $(this);
			var container = settings.id+'_thumbnail';
			var image = input.prop('files');
			var removeBtn = $('#'+container).parent().find('.remove');
			
			input.parent().find('span.error').remove();
			if(image.length>0){
				//Validacion del tipo
				if(image[0].type.toLowerCase()!='image/jpeg' && image[0].type.toLowerCase()!='image/png'){
					input.parent().append('<span class="error text-danger">_t("validation_photo_accept")</span>');
					$('#'+container).attr('src', default_image);
					input.prop('files', null);
					input.val('');
					removeBtn.hide();
					return;
				}
				//Validacion del tamaño
				else if(image[0].size>1000000){
					input.parent().append('<span class="error text-danger">_t("validation_photo_max_size")</span>');
					$('#'+container).attr('src', default_image);
					input.prop('files', null);
					input.val('');
					removeBtn.hide();
					return;
				}
				//Convertimos a Base64
				var FR= new FileReader();
			    FR.onload = function(e) {
			    	var base64 = e.target.result;
		    		$('#'+container).attr('src', base64);
		    		removeBtn.show();
			    };       
			    FR.readAsDataURL(image[0]);
			}
			
			//Eliminar
			clickRemoveBtn(removeBtn);
		});
		
		/**
		 * Eliminar imagen
		 */
		function clickRemoveBtn(btn){
			btn.unbind("click");
			btn.click(function(e){
				$('#'+settings.id+'_thumbnail').attr('src', default_image);
				$('#'+settings.id).prop('files', null);
				$('#'+settings.id).val('');
				btn.hide();
			});
		}
		if(isNew==false){
			clickRemoveBtn($('#'+settings.id+'_remove'));	
		}
	}
}