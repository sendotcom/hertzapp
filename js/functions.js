$().ready(function() {
	registerDeleteModal();
});


/**
 * Confirmar eliminacion de elemento
 */
function registerDeleteModal(){
	$('.btn-confirm-delete').unbind('click');
	$('.btn-confirm-delete').on('click', function(){
		$('#delete_form').attr('action', $(this).data('action'));
	});
}