<?php
class PHPFatalError {
	public function setHandler()
	{
		register_shutdown_function('handleShutdown');
	}
}

function handleShutdown()
{
  if (($error = error_get_last())) {
      require_once(FCPATH.'application/libraries/SystemLog/SystemLog.php');

      $pos = strripos ($error['file'], "application") + 12;
      $file = substr($error['file'],$pos);

      $systemLog = new systemLog($file);
      $systemLog->error($error['message'].' line: '.$error['line']);
  }
}