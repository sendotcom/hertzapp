<?php

/*
 * Spanish language
 */

$lang['text_rest_invalid_api_key'] = 'API key no válida: %s'; // %s is the REST API key
$lang['text_rest_invalid_credentials'] = 'Credenciales no válida';
$lang['text_rest_ip_denied'] = 'IP denega';
$lang['text_rest_ip_unauthorized'] = 'IP desautorizada';
$lang['text_rest_unauthorized'] = 'Desautorizado';
$lang['text_rest_ajax_only'] = 'Solo permitidas peticiones AJAX';
$lang['text_rest_api_key_unauthorized'] = 'Este API key no tiene acceso a este controlador';
$lang['text_rest_api_key_permissions'] = 'Este API key no tiene suficientes permisos';
$lang['text_rest_api_key_time_limit'] = 'Este API key ha alcanzado el limite de peticiones para este método';
$lang['text_rest_unknown_method'] = 'Método desconocido';
$lang['text_rest_unsupported'] = 'Protocolo no soportado';
