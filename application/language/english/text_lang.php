<?php

/*
 * General
 */
$lang["lang_iso"] = "es";
$lang["home"] = "Inicio";
$lang["title"] = "Título";
$lang["description"] = "Descripción";
$lang["category"] = "Categoría";
$lang["send"] = "Enviar";
$lang["save"] = "Guardar";
$lang["new"] = "Nuevo";
$lang["new_f"] = "Nueva";
$lang["edit"] = "Editar";
$lang["delete"] = "Eliminar";
$lang["back"] = "Volver";
$lang["actions"] = "Acciones";
$lang["sent_at"] = "Fecha de Envío";
$lang["upload"] = "Subir";
$lang["yes"] = "Sí";
$lang["no"] = "No";
$lang["list_of"] = "Lista de ";
$lang["config"] = "Configuración";
$lang["imagen"] = "Imagen";

/**
 * Welcome
 */
$lang["welcome"] = "Bienvenido";
$lang["welcome_message"] = "Panel de administración para gestionar el contenido de la aplicación <i>Hertz</i>.";

/**
 * Notificaciones
 */
$lang["notification"] = "Notificación";
$lang["notifications"] = "Notificaciones";
$lang["notifications_manage"] = "Gestión de Notificaciones";
$lang["notification_saved"] = "La notificación se ha guardado correctamente";
$lang["notification_deleted"] = "La notificación se ha eliminado correctamente";
$lang["notification_delete"] = "¿Está seguro de que desea eliminar esta notificación?";
$lang["notification_delete_msg"] = "Si borra la notificación no podrá volver a recuerarla. Deberá enviarla de nuevo.";
$lang["empty_results"] = "No se encontraron resultados.";

/**
 * Usuarios
 */
$lang["login"] = "Iniciar Sesión";
$lang["user"] = "Usuario";
$lang["password"] = "Contraseña";
$lang["email"] = "Email";
$lang["name"] = "Nombre";
$lang["surname"] = "Apellidos";
$lang["platform"] = "Plataforma";
$lang["rol"] = "Rol";
$lang["users"] = "Usuarios";
$lang["users_manage"] = "Gestión de Usuarios";
$lang["user_saved"] = "El usuario se ha guardado correctamente";
$lang["user_deleted"] = "El usuario se ha eliminado correctamente";
$lang["user_delete"] = "¿Está seguro de que desea eliminar este usuario?";
$lang["user_delete_msg"] = "Si borra el usuario no podrá volver a recuerarla. Deberá registrarlo de nuevo.";
$lang["user_code"] = "Nº Contrato";

/**
 * Gestores
 */
$lang["manager"] = "Gestor";
$lang["managers"] = "Gestores";
$lang["managers_manage"] = "Gestión de Gestores";

/**
 * Descuentos
 */
$lang["discount"] = "Descuento";
$lang["discounts"] = "Descuentos";
$lang["discounts_manage"] = "Gestión de Descuentos";
$lang["discount_saved"] = "El Descuento se ha guardado correctamente";
$lang["discount_deleted"] = "El Descuento se ha eliminado correctamente";
$lang["cdp"] = "CDP";
$lang["validity"] = "Validez";
$lang["user_delete_msg"] = "Si borra el descuento no podrá volver a recuerarlo. Deberá registrarlo de nuevo.";
$lang["import"] = "Importar";
$lang["import_file"] = "Fichero CSV";
$lang["import_note"] = "Formato requerido: CSV. Máximo 1000 filas.";
$lang["discounts_imported"] = "Se han importado <b>%s</b> descuentos de <b>%s</b>.";
$lang["download_import_format"] = "Descargar formato";

/**
 * Vehiculos 
 */
$lang["car"] = "Auto";
$lang["cars"] = "Autos";
$lang["car_saved"] = "El auto se ha guardado correctamente";
$lang["car_deleted"] = "El auto se ha eliminado correctamente";
$lang["code"] = "Código";
$lang["class"] = "Clase";
$lang["transmission"] = "Transmisión";
$lang["manual"] = "Manual";
$lang["automatic"] = "Automática";
$lang["doors"] = "Puertas";
$lang["passengers"] = "Pasajeros";
$lang["bags"] = "Maletas";
$lang["air_conditioner"] = "Aire Acondicionado";
$lang["cars_imported"] = "Se han importado <b>%s</b> autos de <b>%s</b>.";

/**
 * Tipo de Vehículos
 */
$lang["car_class"] = "Clase de Auto";
$lang["car_classes"] = "Clases de Auto";
$lang["car_class_saved"] = "La clase de auto se ha guardado correctamente";
$lang["car_class_deleted"] = "La clase de auto se ha eliminado correctamente";
$lang["car_class_no_deleted"] = "La clase de auto está en uso";
$lang["car_class_delete_msg"] = "Si borra la clase de auto no podrá volver a recuerarla. Deberá registrarla de nuevo.";

/**
 * Extras
 */
$lang["car_extra"] = "Extra";
$lang["car_extras"] = "Extras";
$lang["car_extra_saved"] = "El extra se ha guardado correctamente";
$lang["car_extra_deleted"] = "El extra se ha eliminado correctamente";
$lang["car_extra_no_deleted"] = "El extra está en uso";
$lang["product"] = "Producto";
$lang["insurance"] = "Seguro";
$lang["name_short"] = "Abreviatura";
$lang["type"] = "Tipo";
$lang["security"] = "Seguridad";
$lang["mobility"] = "Movilidad";
$lang["entertainment"] = "Entretenimiento";
$lang["car_extras_imported"] = "Se han importado <b>%s</b> extras de <b>%s</b>.";

/**
 * Alquileres
 */
$lang["see"] = "Ver";
$lang["request"] = "Solicitud de Alquiler";
$lang["requests"] = "Solicitudes de Alquiler";
$lang["start_at"] = "Desde";
$lang["end_at"] = "Hasta";
$lang["ref"] = "Referencia";
$lang["price"] = "Precio";
$lang["days"] = "Días";
$lang["subtotal"] = "Subtotal";
$lang["total"] = "Total";
$lang["rent_cancelled"] = "La reserva ha sido cancelada";
$lang["booking"] = "Reserva";
$lang["data_not_available"] = "Información no disponible";

/**
 * Oficinas
 */
$lang["locations"] = "Oficinas";
$lang["location"] = "Oficina";
$lang["location_saved"] = "La oficina se ha guardado correctamente";
$lang["location_deleted"] = "La oficina se ha eliminado correctamente";
$lang["location_no_deleted"] = "La oficina está en uso";
$lang["phone"] = "Teléfono";
$lang["hours"] = "Horario";
$lang["address_1"] = "Dirección 1";
$lang["address_2"] = "Dirección 2";
$lang["zip"] = "Código postal";
$lang["city"] = "Ciudad";
$lang["lat"] = "Latitud";
$lang["lon"] = "Longitud";
$lang["business"] = "Empresa";
$lang["locations_imported"] = "Se han importado <b>%s</b> oficinas de <b>%s</b>.";

/**
 * Encuestas
 */
$lang["survey"] = "Encuesta";
$lang["surveys"] = "Encuestas";
$lang["survey_saved"] = "La encuesta se ha guardado correctamente";
$lang["survey_deleted"] = "La encuesta se ha eliminado correctamente";
$lang["created_at"] = "F. Alta";
$lang["questions"] = "Preguntas";
$lang["add"] = "Añadir";
$lang["select_type"] = "Seleccione el tipo de pregunta";
$lang["answer_select"] = "Selección";
$lang["answer_text"] = "Texto libre";
$lang["answer_rate"] = "Valoración";
$lang["question_of"] = "Pregunta de ";
$lang["answer_number"] = "Respuesta nº ";
$lang["answers"] = "Respuestas";
$lang["question_delete_confirm"] = "¿Está seguro que desea eliminar la pregunta?";

/**
 * Encuestas
 */
$lang["information"] = "Información";
$lang["information_saved"] = "La información se ha guardado correctamente";
$lang["privacy"] = "Política de privacidad";
$lang["terms"] = "Términos y condiciones";

$lang["email_polls"] = "Encuestas y sugerencias";
$lang["email_sos"] = "SOS request";

/**
 * Eliminar
 */
$lang["delete_confirm"] = '¿Está seguro de que desea eliminar este elemento?';
$lang["delete_note"] = 'Es posible que este elemento esté siendo utilizado por algún usuario.';
$lang["cancel"] = "Cancelar";
$lang["continue"] = "Continuar";

/**
 * Validaciones
 */
$lang["validation_required"] = "El campo %s es obligatorio.";
$lang["validation_max_length"] = "El campo %s supera la longitud máxima permitida.";
$lang["validation_image_accepted"] = "Formatos aceptados: PNG, JPEG. Tamaño máximo: 1MB.";
$lang["validation_resolution_accepted"] = "Ejemplo de proporciones óptimas: 1080x432 px";
$lang["validation_photo_max_size"] = 'El tamaño máximo permitido es de 1MB.';
$lang["validation_photo_accept"] = 'Solo se aceptan los formatos PNG y JPEG';
$lang["validation_login"] = "Usuario y/o contraseña incorrecto/s.";
$lang["validation_exist"] = '<b>%s</b> ya existe';

/**
 * JS
 */
$lang["js_datatable"] = 'js/datatable/es.json';

/**
 * Emails
 */
$lang["email_request_subject"] = 'Solicitud de alquiler recibida';
$lang["email_request"] = '
	<!DOCTYPE html>
	<html lang="es">
	<head>
	<meta charset="utf-8">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Hertz</title>
	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i,700,900" rel="stylesheet">
	</head>
	<body style="background:#eee;font-family: \'Lato\', Arial, Helvetica, sans-serif;font-size:14px;color:#A0A7B3;">
	<table width="100%" border="0" align="center" cellpadding="0" cellspacing="10" style="max-width:600px">
	  <tr>
	    <td colspan="2" bgcolor="#FFFFFF" align="center">
	    	<table cellpadding="40" cellspacing="0">
	            <tr>
	                <td>
	                	<img src="#logo#" width="200" height="91">
	                </td>
	            </tr>
	        </table>
	    </td>
	  </tr>
	  <tr>
	    <td colspan="2" align="justify" bgcolor="#FFFFFF">
	        <table cellpadding="20" cellspacing="0">
	            <tr>
	                <td style="font-family: \'Lato\', Arial, Helvetica, sans-serif;font-size:14px;color:#A0A7B3;">
	                    <p style="color:#1E2E3B;font-size:26px;font-weight:900;">Solicitud de alquiler</p>
	                    <p>#user_name#, aquí tiene los detalles de su solicitud.<br/>
						Referencia: #ref#<br/>
						Salida: #from_name# (#from_date#)<br/>
						Llegada: #to_name# (#to_date#)<br/>
						Auto: #car_name#, #car_class# #car_class_name#<br/>
						Extras: #extras#<br/>
						Total: #amount# MXN<br/>
						</p>
						#user_details#
	                </td>
	            </tr>
	        </table>
		</td>
	  </tr>
	</table>
	</body>
	</html>';

$lang["email_confirmation_subject"] = 'Confirmación de alquiler';
$lang["email_confirmation_line_3"] = "Gracias por viajar a la velocidad de <span class='il'>Hertz</span>";
$lang["email_confirmation_line_4"] = "Tu número de confirmación es el siguiente:";
$lang["email_confirmation_line_5"] = "Tu Itinerario";
$lang["email_confirmation_line_6"] = "Localidad de recolección";
$lang["email_confirmation_line_6.2"] = "Fecha / Hora de recolección";
$lang["email_confirmation_line_7"] = "Localidad de devolución";
$lang["email_confirmation_line_7.2"] = "Fecha / Hora de devolución";
$lang["email_confirmation_line_8"] = "Vehículo";
$lang["email_confirmation_line_9"] = "o similar";
$lang["email_confirmation_details_line_1"] = "Lo que pagaras en el mostrador";
$lang["email_confirmation_details_line_2"] = "Precio de alquiler";
$lang["email_confirmation_details_line_3"] = "Cargos adicionales";
$lang["email_confirmation_details_line_drop_charge"] = "Entrega en otra ciudad";
$lang["email_confirmation_details_line_4"] = "Cuota aeropuerto";
$lang["email_confirmation_details_line_5"] = "VLF";
$lang["email_confirmation_details_line_6"] = "Cargo por servicio";
$lang["email_confirmation_details_line_7"] = "IVA / VAT";
$lang["email_confirmation_new_user"] = "Cuenta de usuario:";

$lang["email_survey_subject"] = '%s ha respondido la encuesta %s';
$lang["email_survey"] = '
	<!DOCTYPE html>
	<html lang="es">
	<head>
	<meta charset="utf-8">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Hertz</title>
	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i,700,900" rel="stylesheet">
	</head>
	<body style="background:#eee;font-family: \'Lato\', Arial, Helvetica, sans-serif;font-size:14px;color:#A0A7B3;">
	<table width="100%" border="0" align="center" cellpadding="0" cellspacing="10" style="max-width:600px">
	  <tr>
	    <td colspan="2" bgcolor="#FFFFFF" align="center">
	    	<table cellpadding="40" cellspacing="0">
	            <tr>
	                <td>
	                	<img src="#logo#" width="200" height="91">
	                </td>
	            </tr>
	        </table>
	    </td>
	  </tr>
	  <tr>
	    <td colspan="2" align="justify" bgcolor="#FFFFFF">
	        <table cellpadding="20" cellspacing="0">
	            <tr>
	                <td style="font-family: \'Lato\', Arial, Helvetica, sans-serif;font-size:14px;color:#A0A7B3;">
	                    <p style="color:#1E2E3B;font-size:26px;font-weight:900;">#survey_name#</p>
	                    <p>El usuario #user_name# ha respondido la encuesta.</p>
						#answers#
	                </td>
	            </tr>
	        </table>
		</td>
	  </tr>
	</table>
	</body>
	</html>';

$lang["email_suggestions_subject"] = '%s ha enviado sugerencias';
$lang["email_suggestions"] = '
	<!DOCTYPE html>
	<html lang="es">
	<head>
	<meta charset="utf-8">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Hertz</title>
	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i,700,900" rel="stylesheet">
	</head>
	<body style="background:#eee;font-family: \'Lato\', Arial, Helvetica, sans-serif;font-size:14px;color:#A0A7B3;">
	<table width="100%" border="0" align="center" cellpadding="0" cellspacing="10" style="max-width:600px">
	  <tr>
	    <td colspan="2" bgcolor="#FFFFFF" align="center">
	    	<table cellpadding="40" cellspacing="0">
	            <tr>
	                <td>
	                	<img src="#logo#" width="200" height="91">
	                </td>
	            </tr>
	        </table>
	    </td>
	  </tr>
	  <tr>
	    <td colspan="2" align="justify" bgcolor="#FFFFFF">
	        <table cellpadding="20" cellspacing="0">
	            <tr>
	                <td style="font-family: \'Lato\', Arial, Helvetica, sans-serif;font-size:14px;color:#A0A7B3;">
	                    <p style="color:#1E2E3B;font-size:26px;font-weight:900;">Sugerencias</p>
	                    <p>El usuario #user_name# ha enviado algunas sugerencias.</p>
						<p>#suggestions#</p>
	                </td>
	            </tr>
	        </table>
		</td>
	  </tr>
	</table>
	</body>
	</html>';

$lang["email_recover_subject"] = 'Solicitud de nueva contraseña';
$lang["email_recover"] = '
	<!DOCTYPE html>
	<html lang="es">
	<head>
	<meta charset="utf-8">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Hertz</title>
	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i,700,900" rel="stylesheet">
	</head>
	<body style="background:#eee;font-family: \'Lato\', Arial, Helvetica, sans-serif;font-size:14px;color:#A0A7B3;">
	<table width="100%" border="0" align="center" cellpadding="0" cellspacing="10" style="max-width:600px">
	  <tr>
	    <td colspan="2" bgcolor="#FFFFFF" align="center">
	    	<table cellpadding="40" cellspacing="0">
	            <tr>
	                <td>
	                	<img src="#logo#" width="200" height="91">
	                </td>
	            </tr>
	        </table>
	    </td>
	  </tr>
	  <tr>
	    <td colspan="2" align="justify" bgcolor="#FFFFFF">
	        <table cellpadding="20" cellspacing="0">
	            <tr>
	                <td style="font-family: \'Lato\', Arial, Helvetica, sans-serif;font-size:14px;color:#A0A7B3;">
	                    <p style="color:#1E2E3B;font-size:26px;font-weight:900;">Solicitud de nueva contraseña</p>
	                    <p>Hola #name#, hemos recibido una solicitud para recuperar su contraseña.<br/>
						<strong>Nueva contraseña:</strong> #password#</p>
	                    <br>
	                </td>
	            </tr>
	        </table>
		</td>
	  </tr>
	</table>
	</body>
	</html>';

	$lang["email_sosrequest_subject"] = '%s ha enviado una solicitud SOS';
	$lang["email_sosrequest"] = '
	<!DOCTYPE html>
	<html lang="es">
	<head>
	<meta charset="utf-8">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Hertz</title>
	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i,700,900" rel="stylesheet">
	</head>
	<body style="background:#eee;font-family: \'Lato\', Arial, Helvetica, sans-serif;font-size:14px;color:#A0A7B3;">
	<table width="100%" border="0" align="center" cellpadding="0" cellspacing="10" style="max-width:600px">
	  <tr>
		<td colspan="2" bgcolor="#FFFFFF" align="center">
			<table cellpadding="40" cellspacing="0">
				<tr>
					<td>
						<img src="#logo#" width="200" height="91">
					</td>
				</tr>
			</table>
		</td>
	  </tr>
	  <tr>
		<td colspan="2" align="justify" bgcolor="#FFFFFF">
			<table cellpadding="20" cellspacing="0">
				<tr>
					<td style="font-family: \'Lato\', Arial, Helvetica, sans-serif;font-size:14px;color:#A0A7B3;">
						<p style="color:#1E2E3B;font-size:26px;font-weight:900;">Solicitud SOS</p>
						<p>El usuario #user_name# ha enviado solicitud SOS.</p>
						<p><b>Número de contrato:<b> #contract_number#</p>
						<p><b>Nombre completo:<b> #full_name#</p>
						<p><b>Saludos:<b> #lat#,#lon#</p>
						<p>Saludos.</p>
					</td>
				</tr>
			</table>
		</td>
	  </tr>
	</table>
	</body>
	</html>';

$lang["api_login_error"] = 'Usuario y/o contraseña es incorrectos';
$lang["api_validation_required"] = 'El %s es obligatorio';
$lang["api_validation_exist"] = 'El %s ya existe';
$lang["api_register_success"] = 'Su registro ha sido completado.';
$lang["api_register_error"] = 'El registro no se pudo realizar';
$lang["api_register_email_exist"] = 'El email ya existe';
$lang["api_recover_success"] = 'Hemos enviado un email con su nueva contraseña.';
$lang["api_recover_error"] = 'El email no pertenece a ningún usuario';
$lang["api_rent_no_results"] = 'No se encontraron autos';
$lang["api_rent_error"] = 'No se pudo realizar su reserva';
$lang["api_rent_dates_error"] = 'El periodo mínimo para una reserva es de 1 día';
$lang["api_cdp_error"] = 'El código promocional no es válido.';
$lang["api_cancel_error"] = 'No se pudo cancelar su reserva';
$lang["api_cancel_success"] = 'Su reserva ha sido cancelada';
$lang["api_survey_answered"] = 'La encuesta se ha enviado correctamente';
$lang["api_suggestion_sent"] = 'Las sugerencias se han enviado correctamente';
$lang["api_sosrequest_sent"] = 'Su solicitud SOS se ha enviado correctamente';
$lang["api_sosrequest_error"] = 'Este botón está activo sólo con contratos de renta abiertos. Para acciones relacionadas con sus reservas entre al apartado Mis Reservas o llame al 018007095000';
$lang["api_rates_error_1657"] = 'La oficina de entrega abre de %s a %s. ';
$lang["api_rates_error_1658"] = 'La oficina de devolución abre de %s a %s. ';
$lang["api_rates_error_1341"] = 'La fecha de entrega no es válida. ';
$lang["api_rates_error_1344"] = 'La fecha de devolución no es válida. ';
$lang["api_rates_error_1343"] = 'La oficina de entrega no es válida. ';
$lang["api_rates_error_1346"] = 'La oficina de devolución no es válida. ';
$lang["api_rates_error_9999"] = 'La oficina de entrega/devolución no es válida. ';
$lang["api_rates_error_1487"] = 'No hemos encontrado ninguna tarifa válida. ';
$lang["api_rates_error_cdp"] = 'El CDP no es aplicable a ninguna tarifa. ';
