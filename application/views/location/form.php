<ol class="breadcrumb">
	<li><a href="<?php echo base_url('welcome');?>"><?php echo $this->lang->line('home')?></a></li>
	<li><a href="<?php echo base_url($class_name);?>"><?php echo $this->lang->line('locations')?></a></li>
	<li class="active"><?php echo $this->lang->line($mode=='create' ? 'new_f' : 'edit')?></li>
</ol>

<div class="panel panel-success">
	<div class="panel-heading">
		<h3 class="panel-title"><i class="fa fa-map-marker"></i> <?php echo $this->lang->line($mode=='create' ? 'new_f' : 'edit').' '.$this->lang->line('location')?></h3>
	</div>
	<div class="panel-body">
		<?php echo form_open_multipart($url);?>
			<fieldset>
			
				<?php 
					$errors = validation_errors();
					if(!empty($errors)){
						echo '<div class="alert alert-danger" role="alert">'.$errors.'</div>';
					}
				?>
				
				<section>
					<label><?php echo $this->lang->line('code')?></label>
					<?php echo form_input(array(
						'id' => 'code',
						'name' => 'code',
						'value' => $code,
						'class' => 'form-control',
						'maxlength' => '20'
					));?>
				</section>
				<br/>
			
				<section>
					<label><?php echo $this->lang->line('name')?></label>
					<?php echo form_input(array(
						'id' => 'name',
						'name' => 'name',
						'value' => $name,
						'class' => 'form-control',
						'maxlength' => '65'
					));?>
				</section>
				<br/>
				
				<section>
					<label><?php echo $this->lang->line('address_1')?></label>
					<?php echo form_input(array(
						'id' => 'address_1',
						'name' => 'address_1',
						'value' => $address_1,
						'class' => 'form-control',
						'maxlength' => '128'
					));?>
				</section>
				<br/>
				
				<section>
					<label><?php echo $this->lang->line('address_2')?></label>
					<?php echo form_input(array(
						'id' => 'address_2',
						'name' => 'address_2',
						'value' => $address_2,
						'class' => 'form-control',
						'maxlength' => '65'
					));?>
				</section>
				<br/>
				
				<section>
					<label><?php echo $this->lang->line('city')?></label>
					<?php echo form_input(array(
						'id' => 'city',
						'name' => 'city',
						'value' => $city,
						'class' => 'form-control',
						'maxlength' => '45'
					));?>
				</section>
				<br/>
				
				<section>
					<label><?php echo $this->lang->line('zip')?></label>
					<?php echo form_input(array(
						'id' => 'zip',
						'name' => 'zip',
						'value' => $zip,
						'class' => 'form-control',
						'maxlength' => '20'
					));?>
				</section>
				<br/>
				
				<section>
					<label><?php echo $this->lang->line('lat')?></label>
					<?php echo form_input(array(
						'id' => 'lat',
						'name' => 'lat',
						'value' => $lat,
						'class' => 'form-control',
						'maxlength' => '20'
					));?>
				</section>
				<br/>
				
				<section>
					<label><?php echo $this->lang->line('lon')?></label>
					<?php echo form_input(array(
						'id' => 'lon',
						'name' => 'lon',
						'value' => $lon,
						'class' => 'form-control',
						'maxlength' => '20'
					));?>
				</section>
				<br/>
				
				<section>
					<label><?php echo $this->lang->line('phone')?></label>
					<?php echo form_input(array(
						'id' => 'phone',
						'name' => 'phone',
						'value' => $phone,
						'class' => 'form-control',
						'maxlength' => '20'
					));?>
				</section>
				<br/>
				
				<section>
					<label><?php echo $this->lang->line('hours')?></label>
					<?php echo form_input(array(
						'id' => 'hours',
						'name' => 'hours',
						'value' => $hours,
						'class' => 'form-control',
						'maxlength' => '128'
					));?>
				</section>
				<br/>
				
				<section>
					<label><?php echo $this->lang->line('business')?></label>
					<?php echo form_dropdown(
						'business',
						array(
							'H' => 'Hertz',
							'F' => 'Firefly',
						),
						array($business),
						'class="form-control"'
					); ?>
				</section>
				<br/>

			</fieldset>
			
			<div class="btn-group">
				<button class="btn btn-blue btn-sm" type="submit" name="save"><i class="fa fa-check"></i> <?php echo $this->lang->line('save')?></button>
				<a href="<?php echo base_url($class_name);?>" class="btn btn-default btn-sm"><?php echo $this->lang->line('back')?></a>
			</div>
		<?php echo form_close();?>
	</div>
</div>