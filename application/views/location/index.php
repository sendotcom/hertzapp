<ol class="breadcrumb">
	<li><a href="<?php echo base_url('welcome');?>"><?php echo $this->lang->line('home')?></a></li>
	<li class="active"><?php echo $this->lang->line('locations')?></li>
</ol>

<div class="panel panel-success panel-btn">
	<div class="panel-heading clearfix">
		<h3 class="panel-title"><i class="fa fa-map-marker"></i> <?php echo $this->lang->line("list_of").$this->lang->line("locations")?></h3>
		<a href="<?php echo base_url("/$class_name/create");?>" class="btn btn-blue btn-sm pull-right"><i class="fa fa-plus"></i> <?php echo $this->lang->line('new')?></a>
		<a href="<?php echo base_url("/$class_name/import");?>" class="btn btn-default btn-sm pull-right"><i class="fa fa-database"></i> <?php echo $this->lang->line('import')?></a>
	</div>
	<div class="panel-body">
		<div class="table-responsive">
			<table class="table table-bordered table-striped table-hover table-condensed" id="grid">
				<thead>
					<tr>
						<th style="width: 150px !important;"><?php echo $this->lang->line('code')?></th>
						<th><?php echo $this->lang->line('name')?></th>
						<th style="width: 150px !important;"><?php echo $this->lang->line('city')?></th>
						<th style="width: 150px !important;"><?php echo $this->lang->line('type')?></th>
						<th style="width: 150px !important;"><?php echo $this->lang->line('actions')?></th>
					</tr>
				</thead>
			</table>
		</div>
	</div>
</div>

<script type="text/javascript">
$(document).ready(function() {
    $('#grid').DataTable({
    	"processing": true,
        "serverSide": true,
        "ajax":{
            url : '<?php echo base_url("/$class_name/pagination");?>', // json datasource
            type: "post",  // method  , by default get
            error: function(data){  // error handling
            	$(".grid-error").empty();
                $("#grid_processing").css("display","none");
            },
            complete: function(data){
            	//Registra el modal de confirmar
            	registerDeleteModal();
            },
        },
        language: {
            url: "<?php echo base_url($this->lang->line("js_datatable"));?>"
        }
	});
});
</script>