<?php echo form_open('login', array('class' => 'form-signin'));?>

	<h2 class="form-signin-heading"><img src="<?php echo base_url("images/logo_lg.png"); ?>"></h2>
    			
	<?php echo form_input(array(
		'name' => 'username',
		'value' => $username,
		'class' => 'form-control',
		'placeholder' => $this->lang->line('user'),
	));?>
				
	<?php echo form_password(array(
		'name' => 'password',
		'value' => $password,
		'class' => 'form-control',
		'placeholder' => $this->lang->line('password'),
	));?>
				
	<?php 
		$errors = validation_errors();
		if(!empty($errors)){
			echo '<div class="alert alert-danger" role="alert">'.$errors.'</div>';
		}
	?>
    		
	<button class="btn btn-lg btn-blue btn-block" type="submit"><i class="fa fa-sign-in"></i> <?php echo $this->lang->line('login')?></button>
<?php echo form_close();?>

