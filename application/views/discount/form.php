<ol class="breadcrumb">
	<li><a href="<?php echo base_url('welcome');?>"><?php echo $this->lang->line('home')?></a></li>
	<li><a href="<?php echo base_url('discount');?>"><?php echo $this->lang->line('discounts')?></a></li>
	<li class="active"><?php echo $this->lang->line($mode=='create' ? 'new' : 'edit')?></li>
</ol>

<div class="panel panel-success">
	<div class="panel-heading">
		<h3 class="panel-title"><i class="fa fa-percent"></i> <?php echo $this->lang->line($mode=='create' ? 'new' : 'edit').' '.$this->lang->line('discount')?></h3>
	</div>
	<div class="panel-body">
		<?php echo form_open_multipart($url, array('class' => ''));?>
			<fieldset>
			
				<?php 
					$errors = validation_errors();
					if(!empty($errors)){
						echo '<div class="alert alert-danger" role="alert">'.$errors.'</div>';
					}
				?>
			
				<section>
					<label><?php echo $this->lang->line('cdp')?></label>
					<?php echo form_input(array(
						'id' => 'cdp',
						'name' => 'cdp',
						'value' => $cdp,
						'class' => 'form-control',
						'maxlength' => '255'
					));?>
				</section>
				<br/>
				
				<section>
					<label><?php echo $this->lang->line('description')?></label>
					<?php echo form_input(array(
						'id' => 'description',
						'name' => 'description',
						'value' => $description,
						'class' => 'form-control',
						'maxlength' => '255'
					));?>
				</section>
				<br/>

				<section>
					<label><?php echo $this->lang->line("validity")?></label><br/>
					<input class="form-control" id="validity" name="validity" type="text" value=
					<?php
					if(isset($validity))
					{
						echo '"'.$validity.'" ';
					}
					else
						echo '"'.date("d-m-Y").'" ';
					?>required>
				</section>
				<br/>

			</fieldset>
			
			<div class="btn-group">
				<button class="btn btn-blue btn-sm" type="submit" name="save"><i class="fa fa-check"></i> <?php echo $this->lang->line('save')?></button>
				<a href="<?php echo base_url('discount');?>" class="btn btn-default btn-sm"><?php echo $this->lang->line('back')?></a>
			</div>
		<?php echo form_close();?>
	</div>
</div>

<script>
	$(function() {
		$.datepicker.setDefaults($.datepicker.regional['es']);
		$( "#validity" ).datepicker({ dateFormat: 'yy-mm-dd' });
	});
</script>