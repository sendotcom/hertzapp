<ol class="breadcrumb">
	<li><a href="<?php echo base_url('welcome');?>"><?php echo $this->lang->line('home')?></a></li>
	<li class="active"><?php echo $this->lang->line('discounts')?></li>
</ol>

<div class="panel panel-success panel-btn">
	<div class="panel-heading clearfix">
		<h3 class="panel-title"><i class="fa fa-percent"></i> <?php echo $this->lang->line('discounts_manage')?></h3>
		<a href="<?php echo base_url('discount/create');?>" class="btn btn-blue btn-sm pull-right"><i class="fa fa-plus"></i> <?php echo $this->lang->line('new')?></a>
		<a href="<?php echo base_url('discount/import');?>" class="btn btn-default btn-sm pull-right"><i class="fa fa-database"></i> <?php echo $this->lang->line('import')?></a>
	</div>
	<div class="panel-body">
		<div class="table-responsive">
			<table class="table table-bordered table-striped table-hover table-condensed" id="grid">
				<thead>
					<tr>
						<th><?php echo $this->lang->line('cdp')?></th>
						<th><?php echo $this->lang->line('description')?></th>
						<th style="width: 75px !important;"><?php echo $this->lang->line('validity')?></th>
						<th style="width: 150px !important;"><?php echo $this->lang->line('actions')?></th>
					</tr>
				</thead>
			</table>
		</div>
	</div>
</div>

<!-- MODAL ELIMINAR -->
<div class="modal bg-default" id="deleteModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<form action="<?php echo base_url('discount/delete');?>" method="post">
				<input type="hidden" name="id" id="discount_id"/>
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title"><?php echo $this->lang->line('delete')?></h4>
				</div>
				<div class="modal-body">
					<h3><?php echo $this->lang->line('discount_delete')?></h3>
					<p><?php echo $this->lang->line('discount_delete_msg')?></p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-success" data-dismiss="modal"><?php echo $this->lang->line('no')?></button>
					<button type="submit" class="btn btn-primary"><?php echo $this->lang->line('yes')?></button>
				</div>
			</form>
		</div>
	</div>
</div>

<script type="text/javascript">
$('#deleteModal').on('show.bs.modal', function (event) {
	var button = $(event.relatedTarget);
	$('#discount_id').val(button.data('id'));
});
$('#deleteModal').on('hide.bs.modal', function (event) {
	$('#discount_id').val('');
});

$(document).ready(function() {
	$('#grid').DataTable({
		"processing": true,
	    "serverSide": true,
	    "ajax":{
			url : '<?php echo base_url('discount/pagination');?>', // json datasource
			type: "post",  // method  , by default get
			error: function(data){  // error handling
				$(".grid-error").html("");
				$("#grid").append('<tbody class="grid-error"><tr><th colspan="3"><?php echo $this->lang->line('empty_results')?></th></tr></tbody>');
				$("#grid_processing").css("display","none");
			},
		},
	    language: {
	        url: "<?php echo base_url($this->lang->line("js_datatable"));?>"
	    }
	});
});
</script>