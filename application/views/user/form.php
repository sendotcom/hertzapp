<ol class="breadcrumb">
	<li><a href="<?php echo base_url('welcome');?>"><?php echo $this->lang->line('home')?></a></li>
	<li><a href="<?php echo base_url('user');?>"><?php echo $this->lang->line('managers')?></a></li>
	<li class="active"><?php echo $this->lang->line($mode=='create' ? 'new' : 'edit')?></li>
</ol>

<div class="panel panel-success">
	<div class="panel-heading">
		<h3 class="panel-title"><i class="fa fa-user-secret"></i> <?php echo $this->lang->line($mode=='create' ? 'new' : 'edit').' '.$this->lang->line('manager')?></h3>
	</div>
	<div class="panel-body">
		<?php echo form_open_multipart($url, array('class' => ''));?>
			<fieldset>
			
				<?php 
					$errors = validation_errors();
					if(!empty($errors)){
						echo '<div class="alert alert-danger" role="alert">'.$errors.'</div>';
					}
				?>
			
				<section>
					<label><?php echo $this->lang->line('name')?></label>
					<?php echo form_input(array(
						'id' => 'name',
						'name' => 'name',
						'value' => $name,
						'class' => 'form-control',
						'maxlength' => '45'
					));?>
				</section>
				<br/>
				
				<section>
					<label><?php echo $this->lang->line('email')?></label>
					<?php echo form_input(array(
						'id' => 'username',
						'name' => 'username',
						'value' => $username,
						'class' => 'form-control',
						'maxlength' => '45'
					));?>
				</section>
				<br/>
				
				<section>
					<label><?php echo $this->lang->line('password')?></label>
					<?php echo form_input(array(
						'id' => 'password',
						'name' => 'password',
						'value' => $password,
						'class' => 'form-control',
						'maxlength' => '45'
					));?>
				</section>
				<br/>
				
				<section>
					<label><?php echo $this->lang->line('rol')?></label>
					<?php echo form_dropdown(
						'rol_id',
						$rols,
						array($rol_id),
						'class="form-control"'
					); ?>
				</section>
				<br/>
				
			</fieldset>
			
			<div class="btn-group">
				<button class="btn btn-blue btn-sm" type="submit" name="save"><i class="fa fa-check"></i> <?php echo $this->lang->line('save')?></button>
				<a href="<?php echo base_url('user');?>" class="btn btn-default btn-sm"><?php echo $this->lang->line('back')?></a>
			</div>
		<?php echo form_close();?>
	</div>
</div>