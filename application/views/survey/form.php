<ol class="breadcrumb">
	<li><a href="<?php echo base_url('welcome');?>"><?php echo $this->lang->line('home')?></a></li>
	<li><a href="<?php echo base_url($class_name);?>"><?php echo $this->lang->line('surveys')?></a></li>
	<li class="active"><?php echo $this->lang->line($mode=='create' ? 'new_f' : 'edit')?></li>
</ol>

<?php echo form_open_multipart($url);?>
	<?php 
		$errors = validation_errors();
		if(!empty($errors)){
			echo '<div class="alert alert-danger" role="alert">'.$errors.'</div>';
		}
	?>
	
	<div class="panel panel-success">
		<div class="panel-heading">
			<h3 class="panel-title"><i class="fa fa-question"></i> <?php echo $this->lang->line($mode=='create' ? 'new_f' : 'edit').' '.$this->lang->line('survey')?></h3>
		</div>
		<div class="panel-body">
			<fieldset>
				<section>
					<label><?php echo $this->lang->line('title')?></label>
					<?php echo form_input(array(
						'id' => 'title',
						'name' => 'title',
						'value' => $title,
						'class' => 'form-control',
						'maxlength' => '45'
					));?>
				</section>
				<br/>
				
				<section>
					<label><?php echo $this->lang->line('business')?></label>
					<?php echo form_dropdown(
						'business',
						array(
							'H' => 'Hertz',
							'F' => 'FireFly',
						),
						array($business),
						'class="form-control"'
					); ?>
				</section>
				<br/>
				
			</fieldset>
			
			<div class="btn-group">
				<button class="btn btn-blue btn-sm" type="submit" name="save"><i class="fa fa-check"></i> <?php echo $this->lang->line('save')?></button>
				<a href="<?php echo base_url($class_name);?>" class="btn btn-default btn-sm"><?php echo $this->lang->line('back')?></a>
			</div>
		</div>
	</div>
	
	<div class="panel panel-success panel-btn">
		<div class="panel-heading clearfix">
			<h3 class="panel-title"><i class="fa fa-question"></i> <?php echo $this->lang->line('questions')?></h3>
			 <div class="dropdown pull-right">
				<button class="btn btn-blue btn-sm dropdown-toggle" type="button" id="types" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
					<i class="fa fa-plus"></i> <?php echo $this->lang->line('add')?>
					<span class="caret"></span>
				</button>
				<ul class="dropdown-menu" aria-labelledby="types" id="types">
					<li><a href="javascript: void(0);" data-type="<?php echo Survey_Question_model::TYPE_RADIO?>">
						<i class="fa fa-hand-pointer-o fa-fw"></i> <?php echo $this->lang->line("answer_select")?>
					</a></li>
					<li><a href="javascript: void(0);" data-type="<?php echo Survey_Question_model::TYPE_INPUT?>">
						<i class="fa fa-pencil fa-fw"></i> <?php echo $this->lang->line("answer_text")?>
					</a></li>
					<li><a href="javascript: void(0);" data-type="<?php echo Survey_Question_model::TYPE_STAR?>">
						<i class="fa fa-star-o fa-fw"></i> <?php echo $this->lang->line("answer_rate")?>
					</a></li>
				</ul>
			</div>
		</div>
		<div class="panel-body">
			<table class="table table-striped table-bordered">
				<thead>
					<tr>
						<th style="width: 120px;"><?php echo $this->lang->line('type')?></th>
						<th><?php echo $this->lang->line('title')?></th>
						<th><?php echo $this->lang->line('answers')?></th>
						<th style="width: 120px;"><?php echo $this->lang->line('actions')?></th>
					</tr>
				</thead>
				<tbody id="questions">
					<?php if(!empty($questions) && is_array($questions)){
						foreach ($questions AS $question){
							$question = (object) $question;
							$type = '';
							switch($question->type){
								case Survey_Question_model::TYPE_RADIO:
									$type= '<i class="fa fa-hand-pointer-o fa-fw"></i> '.$this->lang->line("answer_select");
									break;
								case Survey_Question_model::TYPE_INPUT:
									$type= '<i class="fa fa-pencil fa-fw"></i> '.$this->lang->line("answer_text");
									break;
								case Survey_Question_model::TYPE_STAR:
									$type= '<i class="fa fa-star-o fa-fw"></i> '.$this->lang->line("answer_rate");
									break;
							}
							echo "<tr>
								<td>
									$type
									<input type='hidden' name='questions_id[]' value='$question->id'/>
									<input type='hidden' name='questions_type[]' value='$question->type'/>
								</td>
								<td>
									<input type='text' name='questions_title[]' value='$question->title' maxlength='45' class='form-control' placeholder='".$this->lang->line('title')."'/>
								</td>
								<td>";
							if($question->type==Survey_Question_model::TYPE_RADIO){
								echo "<div class='input-group'>
									<span class='input-group-addon'><i class='fa-fw'>1</i></span>
									<input type='text' name='answer_op1[]' value='$question->opt_1' maxlength='45' class='form-control' placeholder='".$this->lang->line('answer_number')."1'/>
								</div>
								<div class='input-group'>
									<span class='input-group-addon'><i class='fa-fw'>2</i></span>
									<input type='text' name='answer_op2[]' value='$question->opt_2' maxlength='45' class='form-control' placeholder='".$this->lang->line('answer_number')."2'/>
								</div>
								<div class='input-group'>
									<span class='input-group-addon'><i class='fa-fw'>3</i></span>
									<input type='text' name='answer_op3[]' value='$question->opt_3' maxlength='45' class='form-control' placeholder='".$this->lang->line('answer_number')."3'/>
								</div>
								<div class='input-group'>
									<span class='input-group-addon'><i class='fa-fw'>4</i></span>
									<input type='text' name='answer_op4[]' value='$question->opt_4' maxlength='45' class='form-control' placeholder='".$this->lang->line('answer_number')."4'/>
								</div>";
							}else{
								echo "<input type='hidden' name='answer_op1[]' value='' maxlength='45'/>";
								echo "<input type='hidden' name='answer_op2[]' value='' maxlength='45'/>";
								echo "<input type='hidden' name='answer_op3[]' value='' maxlength='45'/>";
								echo "<input type='hidden' name='answer_op4[]' value='' maxlength='45'/>";
							}
							echo "</td>
								<td>
									<button class='btn btn-danger btn-block btn-delete' type='button'><i class='fa fa-trash-o'></i> ".$this->lang->line('delete')."</button>
								</td>
							</tr>";
						}
					}?>
				</tbody>
			</table>
		</div>
	</div>

<?php echo form_close();?>

<script type="text/javascript">
<!--
$(function() {
	$("#types a").click(function() {
		var type = '';
		switch($(this).data('type')){
			case '<?php echo Survey_Question_model::TYPE_RADIO?>':
				type = '<i class="fa fa-hand-pointer-o fa-fw"></i> <?php echo $this->lang->line("answer_select")?>';
				break;
			case '<?php echo Survey_Question_model::TYPE_INPUT?>':
				type = '<i class="fa fa-pencil fa-fw"></i> <?php echo $this->lang->line("answer_text")?>';
				break;
			case '<?php echo Survey_Question_model::TYPE_STAR?>':
				type = '<i class="fa fa-star-o fa-fw"></i> <?php echo $this->lang->line("answer_rate")?>';
				break;
		}
		var block = '';
		block+= '<tr>';
		block+= '<td>' + type;
		block+= '<input type="hidden" name="questions_id[]" value="0"/>';
		block+= '<input type="hidden" name="questions_type[]" value="'+$(this).data('type')+'"/>';
		block+= '</td>';
		block+= '<td>';
		block+= '<input type="text" name="questions_title[]" value="" maxlength="45" class="form-control" placeholder="<?php echo $this->lang->line('title')?>"/>';
		block+= '</td>';
		block+= '<td>';
		if($(this).data('type')=='<?php echo Survey_Question_model::TYPE_RADIO?>'){
			// block+= '<label><?php echo $this->lang->line('answers')?></label>';
			block+= '<div class="input-group">';
			block+= '<span class="input-group-addon"><i class="fa-fw">1</i></span>';
			block+= '<input type="text" name="answer_op1[]" value="" maxlength="45" class="form-control" placeholder="<?php echo $this->lang->line('answer_number')?>1"/>';
			block+= '</div>';
			block+= '<div class="input-group">';
			block+= '<span class="input-group-addon"><i class="fa-fw">2</i></span>';
			block+= '<input type="text" name="answer_op2[]" value="" maxlength="45" class="form-control" placeholder="<?php echo $this->lang->line('answer_number')?>2"/>';
			block+= '</div>';
			block+= '<div class="input-group">';
			block+= '<span class="input-group-addon"><i class="fa-fw">3</i></span>';
			block+= '<input type="text" name="answer_op3[]" value="" maxlength="45" class="form-control" placeholder="<?php echo $this->lang->line('answer_number')?>3"/>';
			block+= '</div>';
			block+= '<div class="input-group">';
			block+= '<span class="input-group-addon"><i class="fa-fw">4</i></span>';
			block+= '<input type="text" name="answer_op4[]" value="" maxlength="45" class="form-control" placeholder="<?php echo $this->lang->line('answer_number')?>4"/>';
			block+= '</div>';
		}else{
			block+= '<input type="hidden" name="answer_op1[]" value="" maxlength="45"/>';
			block+= '<input type="hidden" name="answer_op2[]" value="" maxlength="45"/>';
			block+= '<input type="hidden" name="answer_op3[]" value="" maxlength="45"/>';
			block+= '<input type="hidden" name="answer_op4[]" value="" maxlength="45"/>';
		}
		block+= '</td>';
		block+= '<td>';
		block+= '<button class="btn btn-danger btn-block btn-delete" type="button"><i class="fa fa-trash-o"></i> <?php echo $this->lang->line('delete')?></button>';
		block+= '</td>';
		block+= '</tr>';
		$('#questions').append(block);
		
		//Reinicio de eventos:
		deleteEvent();
	});

	//Registro de eventos:
	deleteEvent();
});

function deleteEvent(){
	$('#questions .btn-delete').unbind('click');
	$('#questions .btn-delete').click(function() {
		if(confirm('<?php echo $this->lang->line('question_delete_confirm')?>')){
			$(this).parent().parent().remove();
		}
	});
}
//-->
</script>