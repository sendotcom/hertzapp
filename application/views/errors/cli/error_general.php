<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<img src="<?php echo base_url()."images/logo_lg.png";?>" class="error-logo">
<div class="error-box">
	<h2><?php echo $heading;?></h2>
	<?php echo $message;?>
</div>