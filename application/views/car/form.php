<ol class="breadcrumb">
	<li><a href="<?php echo base_url('welcome');?>"><?php echo $this->lang->line('home')?></a></li>
	<li><a href="<?php echo base_url($class_name);?>"><?php echo $this->lang->line('cars')?></a></li>
	<li class="active"><?php echo $this->lang->line('new')?></li>
</ol>

<div class="panel panel-success">
	<div class="panel-heading">
		<h3 class="panel-title"><i class="fa fa-car"></i> <?php echo $this->lang->line($mode=='create' ? 'new' : 'edit').' '.$this->lang->line('car')?></h3>
	</div>
	<div class="panel-body">
		<?php echo form_open_multipart($url);?>
			<fieldset>
			
				<?php 
					$errors = validation_errors();
					if(!empty($errors)){
						echo '<div class="alert alert-danger" role="alert">'.$errors.'</div>';
					}
				?>
			
				<section>
					<label><?php echo $this->lang->line('name')?></label>
					<?php echo form_input(array(
						'id' => 'name',
						'name' => 'name',
						'value' => $name,
						'class' => 'form-control',
						'maxlength' => '65'
					));?>
				</section>
				<br/>
				
				<section>
					<label><?php echo $this->lang->line('transmission')?></label>
					<?php echo form_dropdown(
						'transmission',
						array(
							'M' => $this->lang->line('manual'),
							'A' => $this->lang->line('automatic'),
						),
						array($transmission),
						'class="form-control"'
					); ?>
				</section>
				<br/>
				
				<section>
					<label><?php echo $this->lang->line('class')?></label>
					<?php echo form_dropdown(
						'class_id',
						$classes,
						array($class_id),
						'class="form-control"'
					); ?>
				</section>
				<br/>
				
				<section>
					<label><?php echo $this->lang->line('code')?></label>
					<?php echo form_input(array(
						'id' => 'sipcode',
						'name' => 'sipcode',
						'value' => $sipcode,
						'class' => 'form-control',
						'maxlength' => '20'
					));?>
				</section>
				<br/>
				
				<section>
					<label><?php echo $this->lang->line('doors')?></label>
					<?php echo form_input(array(
						'id' => 'doors',
						'name' => 'doors',
						'value' => $doors,
						'class' => 'form-control',
						'maxlength' => '11'
					));?>
				</section>
				<br/>
				
				<section>
					<label><?php echo $this->lang->line('passengers')?></label>
					<?php echo form_input(array(
						'id' => 'passengers',
						'name' => 'passengers',
						'value' => $passengers,
						'class' => 'form-control',
						'maxlength' => '11'
					));?>
				</section>
				<br/>
				
				<section>
					<label><?php echo $this->lang->line('bags')?></label>
					<?php echo form_input(array(
						'id' => 'bags',
						'name' => 'bags',
						'value' => $bags,
						'class' => 'form-control',
						'maxlength' => '11'
					));?>
				</section>
				<br/>
				
				<section>
					<label><?php echo $this->lang->line('imagen')?></label>
					<div class="btn btn-default btn-block btn-upload" id="dropzone"></div>
					<div class="note"><?php echo $this->lang->line('validation_image_accepted')?></div>
				</section>
				<br/>
				
			</fieldset>
			
			<div class="btn-group">
				<button class="btn btn-blue btn-sm" type="submit" name="save"><?php echo $this->lang->line('save')?></button>
				<a href="<?php echo base_url($class_name);?>" class="btn btn-default btn-sm"><?php echo $this->lang->line('back')?></a>
			</div>
		<?php echo form_close();?>
	</div>
</div>

<script type="text/javascript">
<!--
$(function() {
	//Subida de imagenes
	$('#dropzone').BluumiDropzone({
		id: 'file',
		image: '<?php echo !empty($image) ? base_url($image) : ''?>',
	});
});
//-->
</script>