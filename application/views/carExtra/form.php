<ol class="breadcrumb">
	<li><a href="<?php echo base_url('welcome');?>"><?php echo $this->lang->line('home')?></a></li>
	<li><a href="<?php echo base_url($class_name);?>"><?php echo $this->lang->line('car_extras')?></a></li>
	<li class="active"><?php echo $this->lang->line($mode=='create' ? 'new' : 'edit')?></li>
</ol>

<div class="panel panel-success">
	<div class="panel-heading">
		<h3 class="panel-title"><i class="fa fa-tag"></i> <?php echo $this->lang->line($mode=='create' ? 'new' : 'edit').' '.$this->lang->line('car_extra')?></h3>
	</div>
	<div class="panel-body">
		<?php echo form_open_multipart($url);?>
			<fieldset>
			
				<?php 
					$errors = validation_errors();
					if(!empty($errors)){
						echo '<div class="alert alert-danger" role="alert">'.$errors.'</div>';
					}
				?>
				
				<section>
					<label><?php echo $this->lang->line('code')?></label>
					<?php echo form_input(array(
						'id' => 'code',
						'name' => 'code',
						'value' => $code,
						'class' => 'form-control',
						'maxlength' => '10'
					));?>
				</section>
				<br/>
			
				<section>
					<label><?php echo $this->lang->line('name')?></label>
					<?php echo form_input(array(
						'id' => 'name',
						'name' => 'name',
						'value' => $name,
						'class' => 'form-control',
						'maxlength' => '128'
					));?>
				</section>
				<br/>
				
				<section>
					<label><?php echo $this->lang->line('name_short')?></label>
					<?php echo form_input(array(
						'id' => 'name_short',
						'name' => 'name_short',
						'value' => $name_short,
						'class' => 'form-control',
						'maxlength' => '10'
					));?>
				</section>
				<br/>
				
				<section>
					<label><?php echo $this->lang->line('description')?></label>
					<?php echo form_input(array(
						'id' => 'description',
						'name' => 'description',
						'value' => $description,
						'class' => 'form-control',
						'maxlength' => '512'
					));?>
				</section>
				<br/>
				
				<section>
					<label><?php echo $this->lang->line('price')?></label>
					<div class="input-group">
						<?php echo form_input(array(
							'id' => 'price',
							'name' => 'price',
							'value' => $price,
							'class' => 'form-control text-right',
							'maxlength' => '15'
						));?>
						<span class="input-group-addon">MXN</span>
					</div>
				</section>
				<br/>
				
				<section>
					<label><?php echo $this->lang->line('type')?></label>
					<?php echo form_dropdown(
						'type',
						array(
							'security' => $this->lang->line('security'),
							'mobility' => $this->lang->line('mobility'),
							'entertainment' => $this->lang->line('entertainment'),
						),
						array($type),
						'class="form-control"'
					); ?>
				</section>
				<br/>
				
				<section>
					<label><?php echo $this->lang->line('imagen')?></label>
					<div class="btn btn-default btn-block btn-upload" id="dropzone"></div>
					<div class="note"><?php echo $this->lang->line('validation_image_accepted')?></div>
				</section>
				<br/>

			</fieldset>
			
			<div class="btn-group">
				<button class="btn btn-blue btn-sm" type="submit" name="save"><i class="fa fa-check"></i> <?php echo $this->lang->line('save')?></button>
				<a href="<?php echo base_url($class_name);?>" class="btn btn-default btn-sm"><?php echo $this->lang->line('back')?></a>
			</div>
		<?php echo form_close();?>
	</div>
</div>

<script type="text/javascript">
<!--
$(function() {
	//Subida de imagenes
	$('#dropzone').BluumiDropzone({
		id: 'file',
		image: '<?php echo !empty($image) ? base_url($image) : ''?>',
	});
});
//-->
</script>