<ol class="breadcrumb">
	<li><a href="<?php echo base_url('welcome');?>"><?php echo $this->lang->line('home')?></a></li>
	<li><a href="<?php echo base_url($class_name);?>"><?php echo $this->lang->line('car_extras')?></a></li>
	<li class="active"><?php echo $this->lang->line('import')?></li>
</ol>

<div class="panel panel-success">
	<div class="panel-heading">
		<h3 class="panel-title"><i class="fa fa-database"></i> <?php echo $this->lang->line('import').' '.$this->lang->line('car_extras')?></h3>
	</div>
	<div class="panel-body">
		<?php echo form_open_multipart($url, array('class' => ''));?>
			<fieldset>
			
				<?php 
					$errors = validation_errors();
					if(!empty($errors)){
						echo '<div class="alert alert-danger" role="alert">'.$errors.'</div>';
					}
				?>
			
				<section>
					<label><?php echo $this->lang->line('import_file')?></label>
					<?php echo form_upload(array(
						'id' => "file",
						'name' => "file",
			        	'accept' => 'csv',
			        	'data-accept' => 'text/x-comma-separated-values,text/comma-separated-values,application/octet-stream,application/vnd.ms-excel,application/x-csv,text/x-csv,text/csv,application/csv,application/excel,application/vnd.msexcel,text/plain',
					));?>
				</section>
				<br/>
				
				<p><?php echo $this->lang->line('import_note')?> <a href="<?php echo base_url("$class_name/import_format")?>" target="_blank"><?php echo $this->lang->line('download_import_format')?></a></p>

			</fieldset>
			
			<div class="btn-group">
				<button class="btn btn-blue btn-sm" type="submit" name="save"><i class="fa fa-check"></i> <?php echo $this->lang->line('import')?></button>
				<a href="<?php echo base_url($class_name);?>" class="btn btn-default btn-sm"><?php echo $this->lang->line('back')?></a>
			</div>
		<?php echo form_close();?>
	</div>
</div>

<script type="text/javascript">
<!--
$(document).ready(function() {
	//Validar formatos de documentos y videos
	$('#file').change(function(e){
		var files = $(this).prop('files');
		if(files.length > 0){
			var types = $(this).data('accept').split(',');
			if(!types.includes(files[0].type)){
				$(this).prop('files', null);
				$(this).val('');
			}
		}
	});
});
//-->
</script>