<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Hertz</title>
		<link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i,700,900" rel="stylesheet">
	</head>
	<body>
		<div align='center'>
			<div style='font-family:Arial,sans-serif;font-size:11px;max-width:600px'>
				<table width='600px' align='center' cellpadding='0' cellspacing='0'>
					<tbody>
						<tr>               
							<td align='left' style='padding-top:10px;padding-right:0px;padding-bottom:10px;padding-left:10px;font-family:Arial,Helvetica,sans-serif;font-size:11px;color:#000000'></td>
						</tr>
					</tbody>
				</table>
			</div>
                
			<table bgcolor='#FFFFFF' border='0' cellpadding='0' cellspacing='0' style='width:600px'>
				<tbody>
					<tr>
						<td>
							<table style='background-color:#000000;height:70px;width:100%'>
								<tbody>
									<tr>
										<td style='float:left;margin-left:20px;margin-top:10px'>
											<img style='margin-top:7px;height:30px' src='http://hertzmx.cocreaxp.com/public/img/hertz_mail.png'>
										</td>
									</tr>
								</tbody>
							</table>
							<div>&nbsp;</div>
							<table bgcolor='FFFFFF' cellpadding='0' cellspacing='0' style='margin-left:2%;margin-right:2%;font-family:sans-serif;font-size:12px;border-bottom:thin solid #ffd100;text-align:center;width:96%'>
								<tbody>
									<tr>
										<td style='font-weight:bold;padding:10px 0 0 0;margin:0'>
											<?php /*
											<div style='font-family:Arial;font-size:16pt;font-weight:bold'>
												$felicidades
											</div>
											<div style='font-family:Arial;font-size:16pt;font-weight:bold'>
												$upg
											</div>
											*/?>
											<?php echo $this->lang->line('email_confirmation_line_3')?>, <?php echo trim("$user->name $user->surname")?>
										</td>
									</tr>
									<tr>
										<td style='font-size:12px;font-family:sans-serif;padding:0;margin:0'>
											<div style='font-weight:bold'>
												<?php echo $this->lang->line('email_confirmation_line_4')?> <span style='margin-left:5px;font-size:23px'><?php echo $rent->ref_thermeon?></span>
											</div>
										</td>
									</tr>
								</tbody>
							</table>
						</td>
					</tr>
					<tr>
						<td style='height:10px'>
							<div style='padding-top:10px'></div>
						</td>
					</tr>
            		<tr>
               			<td style='border-bottom:thin solid #ffd100'>
                  			<table style='margin-right:2%;margin-left:2%;width:96%' cellspacing='0'>
                     			<tbody>
                     				<tr>
                        				<td style='background-color:#ffffff;padding:10px;text-align:left;font-family:sans-serif;font-size:18px;font-weight:bold;text-transform:UPPERCASE'>
                           					<?php echo $this->lang->line('email_confirmation_line_5')?>
                        				</td>
									</tr>
								</tbody>
							</table>
							<table bgcolor='#FFFFFF' align='center' style='font-family:sans-serif;font-size:12px;margin-right:2%;margin-left:2%;width:96%'>
                     			<tbody>
                     				<tr>
										<td style='width:33%;padding-top:10px;vertical-align:top' align='left'> 
                           					<table>
                              					<tbody>
                              						<tr style='font-weight:bold;font-family:sans-serif;font-size:12px'>
                                 						<td style='padding-bottom:10px'>
                                    						<?php echo $this->lang->line('email_confirmation_line_6')?>
                                						</td>
                              						</tr>
                              						<tr>
                                 						<td style='font-family:sans-serif;font-size:12px;padding:0 0 10px'>
                                    						<div style='font-weight:bold;font-family:sans-serif'><?php echo "$location_from->code - $location_from->name"?></div>
                                 						</td>
													</tr>
                              						<tr>
                                    					<td style='padding:0 0 10px;font-family:sans-serif;font-size:12px'><?php echo $location_from->address_1?></td>
                                 					</tr>
                              						<tr>
                                    					<td style='padding:0 0 10px;font-family:sans-serif;font-size:12px'><?php echo $location_from->address_2?><br></td>
                                 					</tr>
                              						<tr>
														<td style='padding:0 0 10px;font-family:sans-serif'><?php echo $location_from->city?></td>
                                 					</tr>
													<tr>
														<td style='padding:0 0 10px;font-family:sans-serif'><?php echo $location_from->hours?></td>
													</tr>
													<tr style='font-weight:bold;font-family:sans-serif;font-size:12px'>
														<td style='padding-bottom:10px'>
															<?php echo $this->lang->line('email_confirmation_line_7')?>
														</td>
													</tr>
													<tr>
														<td style='font-family:sans-serif;font-size:12px;padding:0 0 10px'>
															<div style='font-weight:bold;font-family:sans-serif'><?php echo "$location_to->code - $location_to->name"?></div>
														</td>
													</tr>
													<tr>
														<td style='padding:0 0 10px;font-family:sans-serif;font-size:12px'><?php echo $location_to->address_1?></td>
													</tr>
													<tr>
														<td style='padding:0 0 10px;font-family:sans-serif;font-size:12px'><?php echo $location_to->address_2?><br></td>
													</tr>
													<tr>
														<td style='padding:0 0 10px;font-family:sans-serif'><?php echo $location_to->city?></td>
													</tr>
													<tr>
														<td style='padding:0 0 10px;font-family:sans-serif'><?php echo $location_to->hours?></td>
													</tr>
												</tbody>
											</table>
										</td>
										<td style='vertical-align:top' align='left'>
											<table style='font-family:sans-serif;font-size:12px;padding-top:10px'>
												<tbody>
													<tr style='font-family:sans-serif;font-size:12px'>
														<td style='padding-bottom:10px;font-weight:bold'>
															<label><?php echo $this->lang->line('email_confirmation_line_6.2')?></label>
															<?php $start_at = new DateTime($rent->start_at);?>
															<div style='font-weight:normal'><?php echo $start_at->format('d/m/Y H:i');?></div>
														</td>
													</tr>
													<tr style='text-align:left;font-family:sans-serif;font-size:12px'>
														<td style='padding-bottom:10px;font-weight:bold'>
															<label><?php echo $this->lang->line('email_confirmation_line_7.2')?></label>
															<?php $end_at = new DateTime($rent->end_at);?>
															<div style='font-weight:normal'><?php echo $end_at->format('d/m/Y H:i');?></div>
														</td>
													</tr>
													<tr></tr>
													<?php if($user->is_new){?>
														<tr style='font-weight:bold;font-family:sans-serif;font-size:12px'>
															<td style='padding-bottom:10px'>
																<?php echo $this->lang->line('email_confirmation_new_user')?>
															</td>
														</tr>
														<tr>
															<td style='padding:0 0 10px;font-family:sans-serif'>
																<div style='font-weight:bold;font-family:sans-serif'>
																	<?php echo $this->lang->line('email')?>
																</div>
																<?php echo $user->email?>
															</td>
														</tr>
														<tr>
															<td style='padding:0 0 10px;font-family:sans-serif'>
																<div style='font-weight:bold;font-family:sans-serif'>
																	<?php echo $this->lang->line('password')?>
																</div>
																<?php echo $user->password?>
															</td>
														</tr>
													<?php }?>
												</tbody>
											</table>
										</td>
									</tr>
								</tbody>
							</table>
						</td>
					</tr>
					<tr>
						<td style='height:10px'>
							<div style='padding-top:10px'></div>
						</td>
					</tr>
					<tr>
						<td>
							<table style='margin-right:2%;margin-left:2%;width:96%' border='0' cellpadding='0' cellspacing='0'>
								<tbody>
									<tr>
										<td align='left' style='width:32.5%;vertical-align:top'>
											<table bgcolor='#FFFFFF' width='100%' border='0' cellpadding='0' cellspacing='0'>
												<tbody style='font-size:12px;font-family:sans-serif'>
													<tr>
														<td bgcolor='#FFFFFF' colspan='2' style='padding:10px;text-align:center'>
															<img src='<?php echo base_url($car->image);?>'>
														</td>
													</tr>
													<tr>
														<td colspan='2' style='padding-left:10px;padding-right:10px;padding-top:10px;text-transform:uppercase;color:#000000;font-weight:bold'>
															<?php echo $this->lang->line('email_confirmation_line_8')?>
														</td>
													</tr>
													<tr>
														<td colspan='2' style='padding-right:10px;padding-left:10px;padding-bottom:5px'>
															<?php echo "$car->class_name $car->sipcode"?>
														</td>                                        
													</tr>
													<tr>
														<td colspan='2' style='border-bottom:1px solid #ddd;padding:0 10px 10px 10px;font-weight:bold'>
															<?php echo "($car->class_id) $car->name".' '.$this->lang->line('email_confirmation_line_9')?>
															<ul style='margin:0px;padding:0px'>
																<li><?php echo $car->passengers.' '.$this->lang->line('passengers')?></li>
																<li><?php echo $car->bags.' '.$this->lang->line('bags')?></li>
																<li><?php echo $this->lang->line('transmission').': '.($car->transmission=='A' ? $this->lang->line('automatic') : $this->lang->line('manual'));?></li>
																<li><?php echo $this->lang->line('air_conditioner')?></li>
																<li><?php echo $car->doors.' '.$this->lang->line('doors')?></li>
															</ul>
														</td>
													</tr>
												</tbody>
											</table>
										</td>
										<td align='left' style='width:7%;padding:0 10px'>&nbsp;</td>
										<td align='left' style='width:32.5%;vertical-align:top;padding-left:1.25%'>
											<table bgcolor='#FFFFFF' width='100%' border='0' cellpadding='0' cellspacing='0'>
												<tbody style='font-size:12px;font-family:sans-serif'>
													<tr>
														<td bgcolor='#000000' style='text-align:center;font-size:14px;padding:10px;color:#ffffff;text-transform:UPPERCASE' colspan='2'> 
															<?php echo $this->lang->line('total')?><br><br>
															<div style='font-size:20px'><?php echo number_format($rent->amount, 2, ',', '.')?>&nbsp;MXN</div>
														</td>
													</tr>
													<?php /*
													<tr>
														<td colspan='2' style='padding:10px'>
															<table cellpadding='0' cellspacing='0' style='font-family:sans-serif;font-size:12px'>
																<tbody>
																	<tr>
																		<td colspan='2' style='font-size:11px;padding-bottom:10px'>
																			$cuatro
																		</td>
																	</tr>
																</tbody>
															</table>
														</td>
													</tr>
													*/?>
												</tbody>
											</table>
										</td>
									</tr>
								</tbody>
							</table>
						</td>
					</tr>
					
					
					<tr>
						<td style='height:10px'>
							<div style='padding-top:10px'></div>
						</td>
					</tr>
					<tr>
						<td>
							<table bgcolor='#FFFFFF' align='center' style='font-family:sans-serif;font-size:12px;margin-right:2%;margin-left:2%;width:96%'>
                     			<tbody>
                     				<tr>
										<td style='width:33%;padding-top:10px;vertical-align:top;' align='left'> 
											<label><?php echo $this->lang->line('email_confirmation_details_line_1')?></label>
										</td>
										<td style='width:33%;padding-top:10px;vertical-align:top;font-weight:bold' align='right'> 
										</td>
									</tr>
                     				<tr>
										<td style='width:33%;padding-top:10px;vertical-align:top;font-weight:bold' align='left'> 
											<label><?php echo $this->lang->line('email_confirmation_details_line_2')?></label>
										</td>
										<td style='width:33%;padding-top:10px;vertical-align:top;font-weight:bold' align='right'> 
											<div style='font-weight:normal'><?php echo number_format($rent->amount_price, 2, ',', '.');?>&nbsp;MXN</div>
										</td>
									</tr>
									<tr>
										<td style='width:33%;padding-top:10px;vertical-align:top;font-weight:bold' align='left'> 
											<label><?php echo $this->lang->line('email_confirmation_details_line_3')?></label>
										</td>
										<td style='width:33%;padding-top:10px;vertical-align:top;font-weight:bold' align='right'> 
										</td>
									</tr>
									<tr>
										<td style='width:33%;padding-top:10px;vertical-align:top;font-size: 9px;' align='left'> 
											<label><?php echo $this->lang->line('email_confirmation_details_line_drop_charge')?></label>
										</td>
										<td style='width:33%;padding-top:10px;vertical-align:top;font-weight:bold' align='right'> 
											<div style='font-weight:normal'><?php echo number_format($rent->amount_drop_charge, 2, ',', '.');?>&nbsp;MXN</div>
										</td>
									</tr>
									<tr>
										<td style='width:33%;padding-top:10px;vertical-align:top;font-size: 9px;' align='left'> 
											<label><?php echo $this->lang->line('email_confirmation_details_line_4')?></label>
										</td>
										<td style='width:33%;padding-top:10px;vertical-align:top;font-weight:bold' align='right'> 
											<div style='font-weight:normal'><?php echo number_format($rent->amount_airport, 2, ',', '.');?>&nbsp;MXN</div>
										</td>
									</tr>
									<tr>
										<td style='width:33%;padding-top:10px;vertical-align:top;font-size: 9px;' align='left'> 
											<label><?php echo $this->lang->line('email_confirmation_details_line_5')?></label>
										</td>
										<td style='width:33%;padding-top:10px;vertical-align:top;font-weight:bold' align=right> 
											<div style='font-weight:normal'><?php echo number_format($rent->amount_vlf, 2, ',', '.');?>&nbsp;MXN</div>
										</td>
									</tr>
									<tr>
										<td style='width:33%;padding-top:10px;vertical-align:top;font-size: 9px;' align='left'> 
											<label><?php echo $this->lang->line('email_confirmation_details_line_6')?></label>
										</td>
										<td style='width:33%;padding-top:10px;vertical-align:top;font-weight:bold' align='right'> 
											<div style='font-weight:normal'><?php echo number_format($rent->amount_service, 2, ',', '.');?>&nbsp;MXN</div>
										</td>
									</tr>
									<tr>
										<td style='width:33%;padding-top:10px;vertical-align:top;font-size: 9px;' align='left'> 
											<label><?php echo $this->lang->line('email_confirmation_details_line_7')?></label>
										</td>
										<td style='width:33%;padding-top:10px;vertical-align:top;font-weight:bold' align='right'> 
											<div style='font-weight:normal'><?php echo number_format($rent->amount_vat, 2, ',', '.');?>&nbsp;MXN</div>
										</td>
									</tr>
									<tr>
										<td style='width:33%;padding-top:10px;vertical-align:top;font-weight:bold' align='left'> 
											<label><?php echo $this->lang->line('total')?></label>
										</td>
										<td style='width:33%;padding-top:10px;vertical-align:top;font-weight:bold' align='right'> 
											<div style='font-weight:normal'><?php echo number_format($rent->amount, 2, ',', '.')?>&nbsp;MXN</div>
										</td>
									</tr>
								</tbody>
							</table>
						</td>
					</tr>
					
					<tr>
						<td style='height:10px'>
							<div style='padding-top:10px'></div>
						</td>
					</tr>
 					<tr>
						<td style='padding-top:15px;padding-bottom:15px;background-color:#000000'>
							<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center'>
								<tbody>
									<tr>
										<td align='center'>
											<table width='150' border='0' cellspacing='0' cellpadding='0' align='center' style='margin:0 auto'>
												<tbody>
													<tr>
														<td width='50' align='center'>
															<a href=''>
																<img src="<?php echo base_url('/images/facebook.png');?>" alt='Facebook' height='40' width='40' border='0' style='display:inline-block;border:none' >
															</a>
														</td>
														<td width='50' align='center'>
															<a href=""> 
																<img src="<?php echo base_url('/images/twitter.png');?>" alt='Twitter' height='40' width='40' border='0' style='display:inline-block;border:none' class='CToWUd'>
															</a>
														</td>
														<td width='50' align='center'>
															<a href=""> 
																<img src="<?php echo base_url('/images/instagram.png');?>" alt='Instagram' height='40' width='40' border='0' style='display:inline-block;border:none' class='CToWUd'>
															</a>
														</td>
													</tr>
												</tbody>
											</table>
										</td>
									</tr>
								</tbody>
							</table>
						</td>
					</tr>
				</tbody>
			</table>
		</div>  
	</body>
</html>