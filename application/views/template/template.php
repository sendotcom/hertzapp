<html lang="es">
	<head>
		<!-- METAS -->
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><?php print $title; ?></title>
		<link rel="icon" href="<?php echo base_url('images/favicon.ico');?>">

		<!-- CSS -->
		<link href="<?php echo base_url('css/bootstrap.min.css');?>" rel="stylesheet">
		<link rel="stylesheet" href="<?php echo base_url('css/font-awesome.min.css');?>">
		<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/s/bs/jqc-1.11.3,dt-1.10.10/datatables.min.css"/>
		<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.1/themes/base/jquery-ui.css" />
		<link href="<?php echo base_url('css/navbar.css');?>" rel="stylesheet">
		<link href="<?php echo base_url('css/your_style.css');?>" rel="stylesheet">

		<!-- JS -->
		<script src="<?php echo base_url('js/jquery-1.11.2.min.js');?>"></script>
		<script type="text/javascript" src="https://cdn.datatables.net/s/bs/jqc-1.11.3,dt-1.10.10/datatables.min.js"></script>
		<script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
		<script src="<?php echo base_url('script?file=BluumiDropzone.js');?>"></script>
		<script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
		<script src="<?php echo base_url('/script?file=functions.js');?>"></script>

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
		<div class="container">

			<!-- Static navbar -->
			<nav class="navbar navbar-inverse">
				<div class="container-fluid">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
							<span class="sr-only">Menu</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="<?php echo base_url('welcome');?>">
							<img src="<?php echo base_url('images/logo.png');?>" alt="<?php print $title; ?>"/>
						</a>
					</div>
					<div id="navbar" class="navbar-collapse collapse">
						<ul class="nav navbar-nav">
							<li><a href="<?php echo base_url('welcome');?>"><i class="fa fa-home fa-lg"></i> <?php echo $this->lang->line('home')?></a></li>
							<li><a href="<?php echo base_url('notification');?>"><i class="fa fa-share-alt fa-lg"></i> <?php echo $this->lang->line('notifications')?></a></li>
							<li><a href="<?php echo base_url('userDevice');?>"><i class="fa fa-user fa-lg"></i> <?php echo $this->lang->line('users')?></a></li>
							<li><a href="<?php echo base_url('rent');?>"><i class="fa fa-calendar fa-lg"></i> <?php echo $this->lang->line('requests')?></a></li>
							<li><a href="<?php echo base_url('survey');?>"><i class="fa fa-question fa-lg"></i> <?php echo $this->lang->line('surveys')?></a></li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cog fa-lg"></i> <?php echo $this->lang->line('config')?> <span class="caret"></span></a>
								<ul class="dropdown-menu">
									<li><a href="<?php echo base_url('config');?>"><i class="fa fa-file-text-o fa-fw"></i> <?php echo $this->lang->line('information')?></a></li>
									<li><a href="<?php echo base_url('car');?>"><i class="fa fa-car fa-fw"></i> <?php echo $this->lang->line('cars')?></a></li>
									<li><a href="<?php echo base_url('carClass');?>"><i class="fa fa-list fa-fw"></i> <?php echo $this->lang->line('car_classes')?></a></li>
									<li><a href="<?php echo base_url('carExtra');?>"><i class="fa fa-tag fa-fw"></i> <?php echo $this->lang->line('car_extras')?></a></li>
									<li><a href="<?php echo base_url('discount');?>"><i class="fa fa-percent fa-fw"></i> <?php echo $this->lang->line('discounts')?></a></li>
									<li><a href="<?php echo base_url('location');?>"><i class="fa fa-map-marker fa-fw"></i> <?php echo $this->lang->line('locations')?></a></li>
									<?php if($this->session->userdata('logged_in')->rol_id==ROL_ADMIN){?>
										<li><a href="<?php echo base_url('user');?>"><i class="fa fa-user-secret fa-lg"></i> <?php echo $this->lang->line('managers')?></a></li>
									<?php }?>
									<li><a href="<?php echo base_url('config/email');?>"><i class="fa fa-send fa-fw"></i> <?php echo $this->lang->line('email')?></a></li>
								</ul>
							</li>
						</ul>
						<ul class="nav navbar-nav navbar-right">
							<li><a href="<?php echo base_url('welcome/logout');?>"><?php echo $this->session->userdata('logged_in')->name;?> <i class="fa fa-sign-out fa-lg"></i></a></li>
						</ul>
					</div><!--/.nav-collapse -->
				</div><!--/.container-fluid -->
			</nav>
			
			<?php 
				$success = $this->session->flashdata('success');
				if(!empty($success)){
					echo '<div class="alert alert-success alert-dismissible" role="alert"><i class="fa fa-thumbs-o-up"></i> '.$this->session->flashdata('success').' <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
					$this->session->unset_userdata('success');
				}
				$error = $this->session->flashdata('error');
				if(!empty($error)){
					echo '<div class="alert alert-danger alert-dismissible" role="alert"><i class="fa fa-thumbs-o-down"></i> '.$this->session->flashdata('error').' <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
					$this->session->unset_userdata('error');
				}
			?>
			
			<?php print $content ?>
			
			<!-- MODAL ELIMINAR -->
			<div class="modal" id="deleteModal">
				<div class="modal-dialog">
					<div class="modal-content">
						<form action="" method="post" id="delete_form">
							<input type="hidden" name="id" id="delete_id"/>
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<h4 class="modal-title"><?php echo $this->lang->line("delete")?></h4>
							</div>
							<div class="modal-body">
								<h3><?php echo $this->lang->line("delete_confirm")?></h3>
								<p><?php echo $this->lang->line("delete_note")?></p>
							</div>
							<div class="modal-footer">
								<div class="btn-group">
									<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $this->lang->line("cancel")?></button>
									<button type="submit" class="btn btn-danger"><?php echo $this->lang->line("continue")?></button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
			
		</div> <!-- /container -->
		
		<div style="position:fixed; left:0px; top:0px; width:100%; height:100%; opacity: 0.8; z-index:9999; display:none; background: url(<?php echo base_url('images/loading.gif'); ?>) center center no-repeat #000" id="lightboxSending"></div>

 		<script src="<?php echo base_url('js/bootstrap.min.js');?>"></script>
	</body>
</html>