<html lang="es">
	<head>
    	<meta charset="utf-8">
    	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="icon" href="<?php echo base_url('images/favicon.ico');?>">
    	<title><?php print $title; ?></title>

    	<!-- Bootstrap core CSS -->
    	<link href="<?php echo base_url('css/bootstrap.min.css');?>" rel="stylesheet">

    	<!-- Custom styles for this template -->
    	<link href="<?php echo base_url('css/signin.css');?>" rel="stylesheet">
    	<link rel="stylesheet" href="<?php echo base_url('css/font-awesome.min.css');?>">
    	<link href="<?php echo base_url('css/your_style.css');?>" rel="stylesheet">

    	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    	<!--[if lt IE 9]>
      	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    	<![endif]-->
	</head>
	<body class="login">
    	<div class="container">
    		<?php print $content ?>
		</div>

		<!-- JS -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script src="<?php echo base_url('js/bootstrap.min.js');?>"></script>
	</body>
</html>

