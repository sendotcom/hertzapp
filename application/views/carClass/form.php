<ol class="breadcrumb">
	<li><a href="<?php echo base_url('welcome');?>"><?php echo $this->lang->line('home')?></a></li>
	<li><a href="<?php echo base_url($class_name);?>"><?php echo $this->lang->line('car_classes')?></a></li>
	<li class="active"><?php echo $this->lang->line($mode=='create' ? 'new_f' : 'edit')?></li>
</ol>

<div class="panel panel-success">
	<div class="panel-heading">
		<h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $this->lang->line($mode=='create' ? 'new_f' : 'edit').' '.$this->lang->line('car_class')?></h3>
	</div>
	<div class="panel-body">
		<?php echo form_open_multipart($url);?>
			<fieldset>
			
				<?php 
					$errors = validation_errors();
					if(!empty($errors)){
						echo '<div class="alert alert-danger" role="alert">'.$errors.'</div>';
					}
				?>
				
				<section>
					<label><?php echo $this->lang->line('code')?></label>
					<?php if($mode=='create'){
						echo form_input(array(
							'id' => 'id',
							'name' => 'id',
							'value' => $id,
							'class' => 'form-control',
							'maxlength' => '10'
						));
					}else{
						echo "<i>$id</i>";
					}?>
				</section>
				<br/>
			
				<section>
					<label><?php echo $this->lang->line('name')?></label>
					<?php echo form_input(array(
						'id' => 'name',
						'name' => 'name',
						'value' => $name,
						'class' => 'form-control',
						'maxlength' => '45'
					));?>
				</section>
				<br/>

			</fieldset>
			
			<div class="btn-group">
				<button class="btn btn-blue btn-sm" type="submit" name="save"><i class="fa fa-check"></i> <?php echo $this->lang->line('save')?></button>
				<a href="<?php echo base_url($class_name);?>" class="btn btn-default btn-sm"><?php echo $this->lang->line('back')?></a>
			</div>
		<?php echo form_close();?>
	</div>
</div>