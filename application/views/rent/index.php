<ol class="breadcrumb">
	<li><a href="<?php echo base_url('welcome');?>"><?php echo $this->lang->line('home')?></a></li>
	<li class="active"><?php echo $this->lang->line('cars')?></li>
</ol>

<div class="panel panel-success panel-btn">
	<div class="panel-heading clearfix">
		<h3 class="panel-title"><i class="fa fa-calendar"></i> <?php echo $this->lang->line("list_of").$this->lang->line("requests")?></h3>
	</div>
	<div class="panel-body">
		<div class="table-responsive">
			<table class="table table-bordered table-striped table-hover table-condensed" id="grid">
				<thead>
					<tr>
						<th style="width: 200px !important;"><?php echo $this->lang->line('booking')?></th>
						<th style="width: 200px !important;"><?php echo $this->lang->line('start_at')?></th>
						<th style="width: 200px !important;"><?php echo $this->lang->line('end_at')?></th>
						<th style="width: 150px !important;"><?php echo $this->lang->line('ref')?></th>
						<th style="width: 150px !important;"><?php echo $this->lang->line('car')?></th>
						<th style="width: 100px !important;"><?php echo $this->lang->line('actions')?></th>
					</tr>
				</thead>
			</table>
		</div>
	</div>
</div>

<script type="text/javascript">
$(document).ready(function() {
    $('#grid').DataTable({
    	"processing": true,
        "serverSide": true,
        "ajax":{
            url : '<?php echo base_url("/$class_name/pagination");?>', // json datasource
            type: "post",  // method  , by default get
            error: function(data){  // error handling
            	$(".grid-error").empty();
                $("#grid_processing").css("display","none");
            },
            complete: function(data){
            	//Registra el modal de confirmar
            	registerDeleteModal();
            },
        },
        language: {
            url: "<?php echo base_url($this->lang->line("js_datatable"));?>"
        }
	});
});
</script>