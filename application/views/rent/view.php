<ol class="breadcrumb">
	<li><a href="<?php echo base_url('welcome');?>"><?php echo $this->lang->line('home')?></a></li>
	<li><a href="<?php echo base_url($class_name);?>"><?php echo $this->lang->line('requests')?></a></li>
	<li class="active"><?php echo $rent->ref_thermeon?></li>
</ol>

<?php if($rent->status_id==2){
	echo '<div class="alert alert-danger" role="alert"><i class="fa fa-close"></i> '.$this->lang->line('rent_cancelled').'</div>';;
}?>

<div class="row">
	<div class="col-md-6">
		<div class="panel panel-success">
			<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-angle-right fa-fw"></i> <?php echo $this->lang->line('start_at')?></h3>
			</div>
			<div class="panel-body">
				<address>
					<i><i class="fa fa-calendar"></i> <?php echo $rent->start_at?></i><br/>
					<strong><?php echo $from->name?></strong><br/>
					<?php echo $from->address_1?><br/>
					<?php echo $from->address_2?><br/>
					<?php echo $from->city.', '.$from->zip?><br/>
					<?php echo $this->lang->line('phone').': '.$from->phone?><br/>
					<?php echo $this->lang->line('hours').': '.$from->hours?>
				</address>
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="panel panel-success">
			<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-angle-left fa-fw"></i> <?php echo $this->lang->line('end_at')?></h3>
			</div>
			<div class="panel-body">
				<address>
					<i><i class="fa fa-calendar"></i> <?php echo $rent->end_at?></i><br/>
					<strong><?php echo $to->name?></strong><br/>
					<?php echo $to->address_1?><br/>
					<?php echo $to->address_2?><br/>
					<?php echo $to->city.', '.$to->zip?><br/>
					<?php echo $this->lang->line('phone').': '.$to->phone?><br/>
					<?php echo $this->lang->line('hours').': '.$to->hours?>
				</address>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-success">
			<div class="panel-heading">
				<h3 class="panel-title"><?php echo $rent->ref.(!empty($rent->ref_thermeon) ? " - <b>$rent->ref_thermeon</b>" : '')?></h3>
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-6">
						<?php if(!empty($car)){?>
							<div class="media">
								<div class="media-left">
									<a>
										<img class="media-object" src="<?php echo base_url(!empty($car->image) ? $car->image : 'images/logo_lg.png')?>"/>
									</a>
								</div>
								<div class="media-body">
									<h4 class="media-heading"><?php echo $car->name?></h4>
									<p>
										<b><?php echo $car->class_id?></b> <i><?php echo $car->class_name?></i>
									</p>
								</div>
							</div>
							<br/>
						<?php }?>
					</div>
					<div class="col-md-6">
						<?php if(!empty($user)){?>
							<address>
								<strong><?php echo trim($user->name.' '.$user->surname)?></strong><br/>
								<?php echo $this->lang->line('email').': '.$user->email?><br/>
								<?php echo $this->lang->line('user_code').': '.$user->code?><br/>
							</address>
						<?php }?>	
					</div>
				</div>
				
				<div class="table-responsive">
					<table class="table table-striped">
						<thead>
							<tr>
								<th><?php echo $this->lang->line('name')?></th>
								<th style="text-align: right;"><?php echo $this->lang->line('price')?></th>
								<th style="text-align: right;"><?php echo $this->lang->line('days')?></th>
								<th style="text-align: right;"><?php echo $this->lang->line('subtotal')?></th>
							</tr>
						</thead>
						<tbody>
							<?php if(!empty($car)){
								echo "<tr>
									<td>$car->name</td>
									<td style='text-align: right;'>".number_format(($rent->price / $rent->days), 2, ',', '.')." MXN</td>
									<td style='text-align: right;'>$rent->days</td>
									<td style='text-align: right;'>".number_format($rent->price, 2, ',', '.')." MXN</td>
								</tr>";
							}?>
							<?php foreach ($extras AS $extra){
								echo "<tr>
									<td>$extra->name</td>
									<td style='text-align: right;'>".number_format($extra->price, 2, ',', '.')." MXN</td>
									<td style='text-align: right;'>$rent->days</td>
									<td style='text-align: right;'>".number_format($extra->price * $rent->days, 2, ',', '.')." MXN</td>
								</tr>";
							}?>
						</tbody>
						<tfoot>
							<tr>
								<th></th>
								<th></th>
								<th></th>
								<th class='pull-right'><?php echo number_format($rent->amount, 2, ',', '.')?> MXN</th>
							</tr>
						</tfoot>
					</table>
				</div>

			</div>
		</div>
	</div>
</div>

		
<div class="btn-group pull-right">
	<a href="<?php echo base_url($class_name);?>" class="btn btn-default btn-sm"><?php echo $this->lang->line('back')?></a>
</div>
