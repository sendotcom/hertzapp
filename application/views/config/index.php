<ol class="breadcrumb">
	<li><a href="<?php echo base_url('welcome');?>"><?php echo $this->lang->line('home')?></a></li>
	<li class="active"><?php echo $this->lang->line('information')?></li>
</ol>

<div class="panel panel-success">
	<div class="panel-heading">
		<h3 class="panel-title"><i class="fa fa-file-text-o"></i> <?php echo $this->lang->line('information')?></h3>
	</div>
	<div class="panel-body">
		<?php echo form_open_multipart($url);?>
			<fieldset>
			
				<?php 
					$errors = validation_errors();
					if(!empty($errors)){
						echo '<div class="alert alert-danger" role="alert">'.$errors.'</div>';
					}
				?>
				
				<section>
					<label><?php echo $this->lang->line('privacy')?></label>
					<?php echo form_textarea(array(
						'name' => 'privacy',
						'value' => $privacy,
						'class' => 'form-control ckeditor',
						'rows' => '10',
					));?>
				</section>
				<br/>
				
				<section>
					<label><?php echo $this->lang->line('terms')?></label>
					<?php echo form_textarea(array(
						'name' => 'terms',
						'value' => $terms,
						'class' => 'form-control ckeditor',
						'rows' => '10',
					));?>
				</section>
				<br/>

			</fieldset>
			
			<div class="btn-group">
				<button class="btn btn-blue btn-sm" type="submit" name="save"><i class="fa fa-check"></i> <?php echo $this->lang->line('save')?></button>
				<a href="<?php echo base_url($class_name);?>" class="btn btn-default btn-sm"><?php echo $this->lang->line('back')?></a>
			</div>
		<?php echo form_close();?>
	</div>
</div>