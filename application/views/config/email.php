<ol class="breadcrumb">
	<li><a href="<?php echo base_url('welcome');?>"><?php echo $this->lang->line('home')?></a></li>
	<li class="active"><?php echo $this->lang->line('email')?></li>
</ol>

<div class="panel panel-success">
	<div class="panel-heading">
		<h3 class="panel-title"><i class="fa fa-send"></i> <?php echo $this->lang->line('email')?></h3>
	</div>
	<div class="panel-body">
		<?php echo form_open_multipart($url);?>
			<fieldset>
			
				<?php 
					$errors = validation_errors();
					if(!empty($errors)){
						echo '<div class="alert alert-danger" role="alert">'.$errors.'</div>';
					}
				?>
				
				<section>
					<label><?php echo $this->lang->line('email_polls')?></label>
					<div class="input-group">
						<?php echo form_input(array(
							'id' => 'email_polls',
							'name' => 'email_polls',
							'value' => $email_polls,
							'class' => 'form-control',
							'maxlength' => '45'
						));?>
						<span class="input-group-addon"><i class="fa fa-at"></i></span>
					</div>
				</section>
				<br/>
				
				<section>
					<label><?php echo $this->lang->line('email_sos')?></label>
					<div class="input-group">
						<?php echo form_input(array(
							'id' => 'email_sos',
							'name' => 'email_sos',
							'value' => $email_sos,
							'class' => 'form-control',
							'maxlength' => '45'
						));?>
						<span class="input-group-addon"><i class="fa fa-at"></i></span>
					</div>
				</section>
				<br/>

			</fieldset>
			
			<div class="btn-group">
				<button class="btn btn-blue btn-sm" type="submit" name="save"><i class="fa fa-check"></i> <?php echo $this->lang->line('save')?></button>
			</div>
		<?php echo form_close();?>
	</div>
</div>