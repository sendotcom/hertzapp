<ol class="breadcrumb">
	<li><a href="<?php echo base_url('welcome');?>"><?php echo $this->lang->line('home')?></a></li>
	<li><a href="<?php echo base_url('notification');?>"><?php echo $this->lang->line('notifications')?></a></li>
	<li class="active"><?php echo $this->lang->line($mode=='create' ? 'send' : 'edit')?></li>
</ol>

<div class="panel panel-success">
	<div class="panel-heading">
		<h3 class="panel-title"><i class="fa fa-share-alt"></i> <?php echo $this->lang->line($mode=='create' ? 'new' : 'edit').' '.$this->lang->line('notification')?></h3>
	</div>
	<div class="panel-body">
		<?php echo form_open_multipart($url, array('class' => ''));?>
			<fieldset>
			
				<?php 
					$errors = validation_errors();
					if(!empty($errors)){
						echo '<div class="alert alert-danger" role="alert">'.$errors.'</div>';
					}
				?>
			
				<section>
					<label><?php echo $this->lang->line('title')?></label>
					<?php echo form_input(array(
						'id' => 'title',
						'name' => 'title',
						'value' => $title,
						'class' => 'form-control',
						'maxlength' => '150'
					));?>
				</section>
				<br/>
				
				<section>
					<label><?php echo $this->lang->line('description')?></label>
					<?php echo form_textarea(array(
						'id' => 'description',
						'name' => 'description',
						'value' => $description,
						'class' => 'form-control',
						'maxlength' => '10000',
						'rows' => '4'
					));?>
				</section>
				<br/>
				
				<section>
					<label><?php echo $this->lang->line('category')?></label>
					<?php echo form_dropdown(
						'category_id',
						$categories,
						array($category_id),
						'class="form-control"'
					); ?>
				</section>
				<br/>
				
				<section>
					<label><?php echo $this->lang->line('cdp')?></label>
					<?php echo form_input(array(
						'id' => 'cdp',
						'name' => 'cdp',
						'value' => $cdp,
						'class' => 'form-control',
						'maxlength' => '45'
					));?>
				</section>
				<br/>
				
				<section>
					<label><?php echo $this->lang->line('imagen')?></label>
					<div class="btn btn-default btn-block btn-upload" id="dropzone"></div>
					<div class="note"><?php echo $this->lang->line('validation_image_accepted')?></div>
					<div class="note"><?php echo $this->lang->line('validation_resolution_accepted')?></div>
				</section>
				<br/>
				
			</fieldset>
			
			<div class="btn-group">
				<button class="btn btn-blue btn-sm" type="submit" name="save"><i class="fa fa-send"></i> <?php echo $this->lang->line('send')?></button>
				<a href="<?php echo base_url('notification');?>" class="btn btn-default btn-sm"><?php echo $this->lang->line('back')?></a>
			</div>
		<?php echo form_close();?>
	</div>
</div>

<script>
	$(function() {
		//Subida de imagenes
		$('#dropzone').BluumiDropzone({
			id: 'file',
			image: '<?php echo !empty($image) ? base_url($image) : ''?>',
		});
	});
</script>