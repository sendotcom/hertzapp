<ol class="breadcrumb">
	<li><a href="<?php echo base_url('welcome');?>"><?php echo $this->lang->line('home')?></a></li>
	<li class="active"><?php echo $this->lang->line('notifications')?></li>
</ol>

<div class="panel panel-success panel-btn">
	<div class="panel-heading clearfix">
		<h3 class="panel-title"><i class="fa fa-share-alt"></i> <?php echo $this->lang->line('notifications_manage')?></h3>
		<a href="<?php echo base_url('notification/create');?>" class="btn btn-blue btn-sm pull-right"><i class="fa fa-send"></i> <?php echo $this->lang->line('send')?></a>
	</div>
	<div class="panel-body">
		<div class="table-responsive">
			<table class="table table-bordered table-striped table-hover table-condensed" id="grid">
				<thead>
					<tr>
						<th><?php echo $this->lang->line('title')?></th>
						<th style="width: 150px !important;"><?php echo $this->lang->line('sent_at')?></th>
						<th style="width: 150px !important;"><?php echo $this->lang->line('category')?></th>
						<th style="width: 150px !important;"><?php echo $this->lang->line('actions')?></th>
					</tr>
				</thead>
			</table>
		</div>
	</div>
</div>

<!-- MODAL ELIMINAR -->
<div class="modal bg-default" id="deleteModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<form action="<?php echo base_url('notification/delete');?>" method="post">
				<input type="hidden" name="id" id="notification_id"/>
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title"><?php echo $this->lang->line('delete')?></h4>
				</div>
				<div class="modal-body">
					<h3><?php echo $this->lang->line('notification_delete')?></h3>
					<p><?php echo $this->lang->line('notification_delete_msg')?></p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-success" data-dismiss="modal"><?php echo $this->lang->line('no')?></button>
					<button type="submit" class="btn btn-primary"><?php echo $this->lang->line('yes')?></button>
				</div>
			</form>
		</div>
	</div>
</div>

<script type="text/javascript">
$('#deleteModal').on('show.bs.modal', function (event) {
	var button = $(event.relatedTarget);
	$('#notification_id').val(button.data('id'));
});
$('#deleteModal').on('hide.bs.modal', function (event) {
	$('#notification_id').val('');
});

$(document).ready(function() {
	$('#grid').DataTable({
		"processing": true,
	    "serverSide": true,
	    "ajax":{
			url : '<?php echo base_url('notification/pagination');?>', // json datasource
			type: "post",  // method  , by default get
			error: function(data){  // error handling
				$(".grid-error").html("");
				$("#grid").append('<tbody class="grid-error"><tr><th colspan="3"><?php echo $this->lang->line('empty_results')?></th></tr></tbody>');
				$("#grid_processing").css("display","none");
			},
		},
	    language: {
	        url: "<?php echo base_url($this->lang->line("js_datatable"));?>"
	    }
	});
});
</script>