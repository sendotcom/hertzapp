<div class="jumbotron">
	<h1><?php echo $this->lang->line('welcome')?></h1>
	<p><?php echo $this->lang->line('welcome_message')?></p>
	<p>
		<a class="btn btn-blue" href="<?php echo base_url('notification');?>" role="button"><i class="fa fa-share-alt"></i> <?php echo $this->lang->line('notifications')?></a>
		<a class="btn btn-blue" href="<?php echo base_url('userDevice');?>" role="button"><i class="fa fa-user"></i> <?php echo $this->lang->line('users')?></a>
		<a class="btn btn-blue" href="<?php echo base_url('rent');?>" role="button"><i class="fa fa-calendar fa-lg"></i> <?php echo $this->lang->line('requests')?></a>
		<a class="btn btn-blue" href="<?php echo base_url('survey');?>"><i class="fa fa-question fa-lg"></i> <?php echo $this->lang->line('surveys')?></a>
	</p>
</div>


