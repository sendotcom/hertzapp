<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESCTRUCTIVE') OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code


/** CUSTOM **/
define('LOCAL_ENVIROMENT', FALSE); // TRUE PARA TRABAJAR EN LOCAL
defined('MASTER_PWD')		   OR define('MASTER_PWD', 'CONFIGURAR_MASTER_PWD_MD5_HASH');
defined('ROL_ADMIN')           OR define('ROL_ADMIN', 1);
defined('IP_SERVER')           OR define('IP_SERVER', 'CONFIGURAR_IP_SERVER');// IP SERVIDOR

//SMTP MAILING
defined('SMTP_HOST')	OR define('SMTP_HOST', 'CONFIGURAR_SMTP_HOST');
defined('SMTP_PORT')	OR define('SMTP_PORT', CONFIGURAR_SMTP_PORT);
defined('SMTP_USER')	OR define('SMTP_USER', 'CONFIGURAR_STMP_USER');
defined('SMTP_PWD')		OR define('SMTP_PWD', 'CONFIGURAR_STMP_PASSWORD');

//AMAZON
defined('AMAZON_API')          OR define('AMAZON_API', "CONFIGURAR_AMAZON_API");
defined('AMAZON_SECRET')       OR define('AMAZON_SECRET', "CONFIGURAR_AMAZON_SECRET");
defined('GOOGLE_CREDENTIALS')  OR define('GOOGLE_CREDENTIALS', "CONFIGURAR_GOOGLE_CREDENTIALS");
defined('SNS_ANDROID')         OR define('SNS_ANDROID', "CONFIGURAR_SNS_ANDROID");
defined('SNS_IOS')         	   OR define('SNS_IOS', "CONFIGURAR_SNS_IOS");
//defined('SNS_IOS')         	   OR define('SNS_IOS', "CONFIGURAR_SNS_IOS_SANDBOX");
defined('SNS_IOS_FF')          OR define('SNS_IOS_FF', "CONFIGURAR_SNS_IOS_FF");
//defined('SNS_IOS_FF')          OR define('SNS_IOS_FF', "CONFIGURAR_SNS_IOS_FF_SANDBOX");
defined('SNS_TOPIC')           OR define('SNS_TOPIC', "CONFIGURAR_SNS_TOPIC");

//HERTZ WS
//defined('HERTZ_URL')           OR define('HERTZ_URL', "https://www.thermeon.com/webXG/avasa-test/xml/webxml");
defined('HERTZ_URL')           OR define('HERTZ_URL', "https://webxml.eu.thermeon.io/webXG/avasa/xml/webxml/");
//defined('HERTZ_USER')          OR define('HERTZ_USER', "avaweb");
//defined('HERTZ_PWD')           OR define('HERTZ_PWD', "18FK!@9T");
defined('HERTZ_USER')          OR define('HERTZ_USER', "avaapp");
defined('HERTZ_PWD')           OR define('HERTZ_PWD', "d3AGabra");
defined('HERTZ_CORP_ID')       OR define('HERTZ_CORP_ID', "APP-WEB");
defined('FIREFLY_CORP_ID')     OR define('FIREFLY_CORP_ID', "APP-WEBFF");

//GEOTAB
defined('GEOTAB_URL_LOGIN')    OR define('GEOTAB_URL_LOGIN', "https://my.geotab.com/apiv1");
defined('GEOTAB_URL')		   OR define('GEOTAB_URL', "https://movistar144.geotab.com/apiv1");
defined('GEOTAB_USER')    	   OR define('GEOTAB_USER', "easian@excelia-bpm.com");
defined('GEOTAB_PWD')    	   OR define('GEOTAB_PWD', "geotab_hertz");
defined('GEOTAB_DB')    	   OR define('GEOTAB_DB', "tefmex_hertz");

// IMPUESTOS //
defined('TAX_AIRPORT')     	    OR define('TAX_AIRPORT', 0.19);
defined('TAX_VLF')     			OR define('TAX_VLF', 0.06);
defined('TAX_SERVICE')     		OR define('TAX_SERVICE', 10);
defined('TAX_VAT')     			OR define('TAX_VAT', 0.16);