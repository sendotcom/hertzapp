<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
class RestController extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
    }
    
    /**
     * Genera una cadena de texto aleatoria
     * @param number $length Longitud de la cadena
     * @return string Cadena generada
     */
    public function generateRandomString($length = 10, $specials=true) {
    	$randomString = '';
    	$seed = str_split('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'.($specials ? '.,-_!#$&()/[]{}' : ''));
    	shuffle($seed);
    	foreach (array_rand($seed, $length) as $i) {
    		$randomString .= $seed[$i];
    	}
    	return $randomString;
    }
    
    /**
     * Comprueba si existe un directorio, si no existe lo crea
     * @param string $dir
     */
    public function checkDir($dir){
    	if (!file_exists($dir)) {
    		mkdir($dir, 0777, true);
    		file_put_contents($dir.'index.html', "<!DOCTYPE html><html><head><title>403 Forbidden</title></head><body><p>Directory access is forbidden.</p></body>", FILE_APPEND);
    	}
    }
    
    /**
     * Envia un email
     * @param string $to_email
     * @param string $subject
     * @param string $body
     * @param string $from_name
     */
    public function sendEmail($to_email, $subject, $body, $from_name='Hertz', $bcc=false){
    	if(!LOCAL_ENVIROMENT){
    		$this->load->library('My_PHPMailer');
    		$mail = new PHPMailer();
    		$mail->CharSet = 'UTF-8';
    		$mail->IsSMTP();
    		$mail->Host       = SMTP_HOST;
    		$mail->SMTPAuth   = true;
    		$mail->Port       = SMTP_PORT;
    		$mail->Username   = SMTP_USER;
    		$mail->Password   = SMTP_PWD;
    			
    		$mail->SetFrom('CONFIGURAR_FROM_EMAIL', $from_name);
    		$mail->Subject = $subject;
    		$mail->AddAddress($to_email);
    		if($bcc){
    			$mail->AddBCC('CONFIGURAR_BCC_EMAIL', 'CONFIGURAR_BCC_NAME');
    		}
    		$mail->AltBody = $body;
    		$mail->Body = $body;
    			
    		$mail->IsHTML(true);
    		return $mail->Send();
    	}
    
    	return false;
    }
    
    /**
     * Calcula la distancia entre dos puntos
     * @param unknown $latitudeFrom
     * @param unknown $longitudeFrom
     * @param unknown $latitudeTo
     * @param unknown $longitudeTo
     * @return number Devuelve los metros de distancia
     */
    public function distance($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo){
    	$earthRadius = 6371000;
    	// convert from degrees to radians
    	$latFrom = deg2rad(str_replace(',','.', trim($latitudeFrom)));
    	$lonFrom = deg2rad(str_replace(',','.', trim($longitudeFrom)));
    	$latTo = deg2rad(str_replace(',','.', trim($latitudeTo)));
    	$lonTo = deg2rad(str_replace(',','.', trim($longitudeTo)));
    
    	$lonDelta = $lonTo - $lonFrom;
    	$a = pow(cos($latTo) * sin($lonDelta), 2) +
    	pow(cos($latFrom) * sin($latTo) - sin($latFrom) * cos($latTo) * cos($lonDelta), 2);
    	$b = sin($latFrom) * sin($latTo) + cos($latFrom) * cos($latTo) * cos($lonDelta);
    
    	$angle = atan2(sqrt($a), $b);
    	return $angle * $earthRadius;
    }
    
    /**
     * Llama al API de Hertz
     * @param string $xml
     */
    public function call($session_id, $xml){
    	//Cabeceras de la solicitud XML
    	$xml = '<Request xmlns="http://www.thermeon.com/webXG/xml/webxml/" 
    				referenceNumber="'.$session_id.'" 
    				version="2.3000">
    				'.$xml.'
    			</Request>';

    	//Llamada
    	$ch = curl_init();
    	curl_setopt($ch, CURLOPT_URL, HERTZ_URL);
    	curl_setopt($ch, CURLOPT_VERBOSE, 1);
    	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    	curl_setopt($ch, CURLOPT_POST, 1);
    	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    	curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    	curl_setopt($ch, CURLOPT_USERPWD, HERTZ_USER.":".HERTZ_PWD);
    	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/xml'));
    	curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
    	$response = curl_exec($ch);
    	
    	curl_close($ch);
    	return $response; 
    }
 
    /**
     * Diferencia entre fechas
     * @param string $from
     * @param string $to
     * @return number
     */
    public function dateDiff($from, $to){
    	$date_from = DateTime::createFromFormat('Y-m-d H:i:s', $from);
    	$date_from->setTime(00, 00, 00);
    	$date_to = DateTime::createFromFormat('Y-m-d H:i:s', $to);
    	$date_to->setTime(00, 00, 00);
    	$days = date_diff($date_from, $date_to);
    	return !empty($days) ? $days->format('%a') : 0;
    }
    
}