 <?php
defined('BASEPATH') OR exit('No direct script access allowed');

include('properties.php');

class SystemLog {

    public $file = "";

    public function __construct($file)
    {
        //Extraemos el fichero del cual realizamos la llamada
        $this->file = $file;
    }

    /**
    * Insertamos en modo error
    * 
    * Insertamos en el fichero ERROR definido en las properties.
    * Si tenemos activada la consola se imprime.
    * 
    * @param string $msg
    * 
    * */
    public function error($msg)
    {
        $properties = new System_log_properties();
        $error = $properties->error;
        $try = 0;
        $retry = FALSE;
        $msg = date($error['date_format'])." ERROR ".$this->file." - ".$msg;
        //Reintentamos la escritura si falla hasta 3 veces
        do{
            try{
                file_put_contents($error['path'], $msg."\n", FILE_APPEND);
            }
            catch(Exception $e)
            {
                $retry = TRUE;
                $try++;
            }
        }
        while($retry && $try < 3);

        if($error['console'])
        {
            $msg = str_replace('\'', '\"',$msg);
            echo "<script>console.log('".$msg."');</script>";
        }
    }
    
    /**
    * Insertamos en modo error
    * 
    * Insertamos en el fichero DEBUG definido en las properties.
    * Si tenemos activada la consola se imprime.
    * 
    * @param string $msg
    * 
    * */
    public function debug($msg)
    {
        $properties = new System_log_properties();
        $debug = $properties->debug;
        $try = 0;
        $retry = FALSE;
        $msg = date($debug['date_format'])." DEBUG ".$this->file." - ".$msg;
        //Reintentamos la escritura si falla hasta 3 veces
        do
        {
            try
            {
                file_put_contents($debug['path'], $msg."\n", FILE_APPEND);
            }
            catch(Exception $e)
            {
                $retry = TRUE;
                $try++;
            }
        }
        while($retry && $try < 3);
        
        if($debug['console'])
        {
            $msg = str_replace('\'', '\"',$msg);
            echo "<script>console.log('".$msg."');</script>";
        }
    }

    /**
    * Insertamos en modo error
    * 
    * Insertamos en el fichero INFO definido en las properties.
    * Si tenemos activada la consola se imprime.
    * 
    * @param string $msg
    * 
    * */
    public function info($msg)
    {
        $properties = new System_log_properties();
        $info = $properties->info;
        $try = 0;
        $retry = FALSE;
        $msg = date($info['date_format'])." INFO ".$this->file." - ".$msg;
        //Reintentamos la escritura si falla hasta 3 veces
        do
        {
            try
            {
                file_put_contents($info['path'], $msg."\n", FILE_APPEND);
            }
            catch(Exception $e)
            {
                $retry = TRUE;
                $try++;
            }
        }
        while($retry && $try < 3);
        
        //file_put_contents($info['path'], $msg, FILE_APPEND);
        if($info['console'])
        {
            $msg = str_replace('\'', '\"',$msg);
            echo "<script>console.log('".$msg."');</script>";
        }
    }
    
    /**
     * Insertamos en modo Gateway
     *
     * Insertamos en el fichero GATEWAY definido en las properties.
     * Si tenemos activada la consola se imprime.
     *
     * @param string $msg
     *
     * */
    public function gateway($msg)
    {
    	$properties = new System_log_properties();
    	$info = $properties->gateway;
    	$try = 0;
    	$retry = FALSE;
    	$msg = date($info['date_format'])." ERROR ".$this->file." - ".$msg;
    	//Reintentamos la escritura si falla hasta 3 veces
    	do
    	{
    		try
    		{
    			file_put_contents($info['path'], $msg."\n", FILE_APPEND);
    		}
    		catch(Exception $e)
    		{
    			$retry = TRUE;
    			$try++;
    		}
    	}
    	while($retry && $try < 3);
    
    	if($info['console'])
    	{
    		$msg = str_replace('\'', '\"',$msg);
    		echo "<script>console.log('".$msg."');</script>";
    	}
    }
    
    /**
     * Insertamos en modo Sync
     *
     * Insertamos en el fichero GATEWAY definido en las properties.
     * Si tenemos activada la consola se imprime.
     *
     * @param string $msg
     *
     * */
    public function sync($msg)
    {
    	$properties = new System_log_properties();
    	$info = $properties->sync;
    	$try = 0;
    	$retry = FALSE;
    	$msg = date($info['date_format'])." ERROR ".$this->file." - ".$msg;
    	//Reintentamos la escritura si falla hasta 3 veces
    	do
    	{
    		try
    		{
    			file_put_contents($info['path'], $msg."\n", FILE_APPEND);
    		}
    		catch(Exception $e)
    		{
    			$retry = TRUE;
    			$try++;
    		}
    	}
    	while($retry && $try < 3);
    
    	if($info['console'])
    	{
    		$msg = str_replace('\'', '\"',$msg);
    		echo "<script>console.log('".$msg."');</script>";
    	}
    }
    
}