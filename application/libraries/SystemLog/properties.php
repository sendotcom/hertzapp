<?php
    
/**
* Properties System_log
* 
* Definimos los distintos metodos de insercción.
* 
* Según las constantes insertaremos en una ruta diferente 
* y un formato diferente. Además se puede definir pintar
* o no en la consola.
* 
* */
defined('BASEPATH') OR exit('No direct script access allowed');

class System_log_properties {
	
    public $error = array();
    public $debug = array();
    public $info = array();
    public $gateway = array();
    public $sync = array();

    public function __construct()
    {
    	$date = date('Y_m_d');
        //Extraemos el fichero del cual realizamos la llamada
        $this->info = array(
			'date_format' => 'Y-m-d H:i:s',
			'path' => APPPATH."logs/info_log-$date.txt",
			'console' => TRUE
		);
		//Extraemos el fichero del cual realizamos la llamada
        $this->debug = array(
			'date_format' => 'Y-m-d H:i:s',
			'path' => APPPATH."logs/debug_log-$date.txt",
			'console' => TRUE
		);
		//Extraemos el fichero del cual realizamos la llamada
        $this->error = array(
			'date_format' => 'Y-m-d H:i:s',
			'path' => APPPATH."logs/error_log-$date.txt",
			'console' => TRUE
		);
        //Extraemos el fichero del cual realizamos la llamada
        $this->gateway = array(
        	'date_format' => 'Y-m-d H:i:s',
        	'path' => APPPATH."logs/gateway_log-$date.txt",
        	'console' => FALSE
        );
        //Extraemos el fichero del cual realizamos la llamada
        $this->sync = array(
        	'date_format' => 'Y-m-d H:i:s',
        	'path' => APPPATH."logs/sync_log-$date.txt",
        	'console' => FALSE
        );
    }
    
}