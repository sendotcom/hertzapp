<?php
    class Languageloader{
        function initialize() {
        
            $ci =& get_instance();
            $ci->load->helper('language');
            if (session_status() == PHP_SESSION_NONE) {
                session_start();
            }
            $site_lang = $ci->session->userdata('site_lang');

            //Detecta el idioma del navegador
            $broser_lang = !empty ($_SERVER['HTTP_ACCEPT_LANGUAGE']) ? strtok(strip_tags($_SERVER['HTTP_ACCEPT_LANGUAGE']), ',') : '';
            if ($broser_lang==''){
                $header = getallheaders();//Para los dispositivos moviles Android enviar en el header 'language-device': es, en, etc
                if (array_key_exists('Language-Device', $header)){
                    $broser_lang= $header['Language-Device'];
                    $site_lang=NULL;
                }
            }
            $lang = substr($broser_lang, 0, 2);

            switch ($lang){
                case "fr":
                    $lang = "french";
                    break;
                case "it":
                    $lang = "italian";
                    break;
                case "en":
                    $lang = "english";
                    break; 
                case "es":
                    $lang = "spanish";
                    break;       
                default:
                    $lang = "english";
                    break;
            }
            //Fin detecta

            

            if ($site_lang) {
                $_SESSION['site_lang']=$site_lang;
                $ci->lang->load('string',$ci->session->userdata('site_lang'));
            } else {
                $_SESSION['site_lang']=$lang;
                $ci->lang->load('string',$lang );
                
            }
        }

    }
	
?>
