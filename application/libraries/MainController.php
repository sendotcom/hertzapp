<?php defined('BASEPATH') OR exit('No direct script access allowed');

class MainController extends CI_Controller {
	
	public function __construct(){
		parent::__construct();

		//Seguridad
		if(!$this->session->userdata('logged_in'))
		{
			redirect('login');
		}
	}
	
	/**
	 * Genera una cadena de texto aleatoria
	 * @param number $length Longitud de la cadena
	 * @return string Cadena generada
	 */
	public function generateRandomString($length = 10) {
		$randomString = '';
		$seed = str_split('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789.,-_!#$&()/[]{}');
		shuffle($seed);
		foreach (array_rand($seed, $length) as $i) {
			$randomString .= $seed[$i];
		}
		return $randomString;
	}
	
	/**
	 * Envia un email
	 * @param string $to_email
	 * @param string $subject
	 * @param string $body
	 * @param string $from_name
	 */
	public function sendEmail($to_email, $subject, $body, $from_name='Hertz'){
		if(!LOCAL_ENVIROMENT){
			$this->load->library('My_PHPMailer');
			$mail = new PHPMailer();
			$mail->CharSet = 'UTF-8';
			$mail->IsSMTP();
			$mail->Host       = SMTP_HOST;
			$mail->SMTPAuth   = true;
			$mail->Port       = SMTP_PORT;
			$mail->Username   = SMTP_USER;
			$mail->Password   = SMTP_PWD;
				
			$mail->SetFrom('CONFIGURAR_FROM_EMAIL', $from_name);
			$mail->Subject = $subject;
			$mail->AddAddress($to_email);
			$mail->AltBody = $body;
			$mail->Body = $body;
				
			$mail->IsHTML(true);
			return $mail->Send();
		}
	
		return false;
	}
	
	/**
	 * Obtiene un parametro por GET y lo limpia
	 * @param string $param_name
	 */
	public function get($param_name, $strip_tags=true){
		$data = '';
		if(isset($_GET[$param_name])){
			$data = $this->input->get($param_name);
			if(is_array($data)){
				foreach ($data AS $i => $item){
					$data[$i] = $this->security->xss_clean($data[$i]);
					if($strip_tags){
						$data[$i] = strip_tags(trim($data[$i]));
					}
				}
			}else{
				$data = $this->security->xss_clean($data);
				if($strip_tags){
					$data = strip_tags(trim($data));
				}
			}
		}
		return $data;
	}
	
	/**
	 * Obtiene un parametro por POST y lo limpia
	 * @param string $param_name
	 */
	public function post($param_name, $strip_tags=true){
		$data = '';
		if(isset($_POST[$param_name])){
			$data = $this->input->post($param_name);
			if(is_array($data)){
				foreach ($data AS $i => $item){
					$data[$i] = $this->security->xss_clean($data[$i]);
					if($strip_tags){
						$data[$i] = strip_tags(trim($data[$i]));
					}
				}
			}else{
				$data = $this->security->xss_clean($data);
				if($strip_tags){
					$data = strip_tags(trim($data));
				}
			}
		}
		return $data;
	}
	
	/**
	 * Comprueba si existe un directorio, si no existe lo crea
	 * @param string $dir
	 */
	public function checkDir($dir){
		if (!file_exists($dir)) {
			mkdir($dir, 0777, true);
			file_put_contents($dir.'index.html', "<!DOCTYPE html><html><head><title>403 Forbidden</title></head><body><p>Directory access is forbidden.</p></body>", FILE_APPEND);
		}
	}
	
}