<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
require 'aws.phar';

class SnsUtils
{
	const GCM = 'GCM';
	const APNS = 'APNS';
	const APNS_SANDBOX = 'APNS_SANDBOX';
	const WNS = 'WNS';
	private $sns;
	
	public function __construct() {
		$this->sns = Aws\Sns\SnsClient::factory(array(
			'key'    => AMAZON_API,
			'secret' => AMAZON_SECRET,
			'region'  => 'eu-west-1'
		));
	}
	
	/**
	 * Registra una applicacion para una plataforma
	 * @param string $name
	 * @param string $platform
	 * @param string $platformCredential
	 * @param string $platformPrincipal
	 * @return string|boolean
	 */
	public function createPlatformApplication($name, $platform, $platformCredential, $platformPrincipal){
		try {
			$name = str_replace(' ', '_', $name);
			$response = $this->sns->CreatePlatformApplication(array(
				// Name is required
				'Name' => $name,
				// Platform is required
				'Platform' => $platform,
				//Atributos
				'Attributes' => array(
					'PlatformCredential' => $platformCredential,
					'PlatformPrincipal' => $platformPrincipal,
				),
			));
			
			//file_put_contents(FCPATH.'/application/logs/__sns.txt', "\n [createPlatformApplication] Result: ".print_r($response, true), FILE_APPEND);
		
			if(isset($response['PlatformApplicationArn'])) {
				$app = new stdClass;
				$app->platform = $platform;
				$app->app_arn = $response['PlatformApplicationArn'];
				if($platform==self::GCM){
					$app->topic_arn = $this->createTopic($name);
				}
				return $app;
			}
			
		} catch(Exception $e) {
			//file_put_contents(FCPATH.'/application/logs/__sns.txt', "\n [createPlatformApplication] Error: ".$e->getMessage(), FILE_APPEND);
		}
		return false;
	}
	
	/**
	 * Registra un Dispositivo para una app
	 * @param string $token
	 * @param string $arn
	 */
	public function createPlatformEndpoint($token, $appArn, $topicArn=null) {
		try {
			$response = $this->sns->CreatePlatformEndpoint(array(
				// PlatformApplicationArn is required
				'PlatformApplicationArn' => $appArn,
				// Token is required
				'Token' => $token
			));
			
			//file_put_contents(FCPATH.'/application/logs/__sns.txt', "\n [CreatePlatformEndpoint] Result: ".print_r($response, true), FILE_APPEND);
	
			if(isset($response['EndpointArn'])) {
				//Suscribe en la 
				if(!empty($topicArn)){
					$this->subscribeTopic($response['EndpointArn'], $topicArn);
				}
				
				return $response['EndpointArn'];
			}
		} catch(Exception $e) {
			//file_put_contents(FCPATH.'/application/logs/__sns.txt', "\n [CreatePlatformEndpoint] Error: ".$e->getMessage(), FILE_APPEND);
		}
		return false;
	}
	
	/**
	 * Crea un tema para enviar notificaciones globales
	 * @param unknown $name
	 */
	private function createTopic($name){
		try {
			$name = str_replace(' ', '_', $name);
			$response = $this->sns->CreateTopic(array(
				// Name is required
				'Name' => $name,
			));
				
			//file_put_contents(FCPATH.'/application/logs/__sns.txt', "\n [CreateTopic] Result: ".print_r($response, true), FILE_APPEND);
		
			if(isset($response['TopicArn'])) {
				return $response['TopicArn'];
			}
		} catch(Exception $e) {
			//file_put_contents(FCPATH.'/application/logs/__sns.txt', "\n [CreateTopic] Error: ".$e->getMessage(), FILE_APPEND);
		}
		return false;
	}

	/**
	 * Suscribe al dispositivo en un tema para una app
	 * @param string $token
	 * @param string $topicArn
	 */
	private function subscribeTopic($endPointArn, $topicArn){
		try {
			$response = $this->sns->Subscribe(array(
				// Endpoint a suscribir
				'Endpoint' => $endPointArn,
				//Required
				'Protocol' => 'application',
				//Required
				'TopicArn' => $topicArn,
			));
		
			//file_put_contents(FCPATH.'/application/logs/__sns.txt', "\n [Subscribe] Result: ".print_r($response, true), FILE_APPEND);
		
			if(isset($response['SubscriptionArn'])) {
				return true;
			}
		} catch(Exception $e) {
			//file_put_contents(FCPATH.'/application/logs/__sns.txt', "\n [Subscribe] Error: ".$e->getMessage(), FILE_APPEND);
		}
		return false;
	}
	
	/**
	 * Envia una notificacion
	 * @param string $message
	 * @param string $type
	 * @param string $arn
	 * @return boolean
	 */
	public function publish($message, $type='topic', $arn){
		try {
			$message = json_encode(array(
				'default' => $message,
				'GCM' => "{ \"data\": $message}",
				'APNS' => "{ \"aps\": $message}",
				'APNS_SANDBOX' => "{ \"aps\": $message}",
			));
			
			$response = $this->sns->Publish(array(
				//Destino de la notificacion EndPoint o Topic
				($type=='endpoint' ? 'TargetArn' : 'TopicArn') => $arn,
				//Titulo de la notificacion
				'Subject' => 'SNS',
				// Contenido a enviar (PAYLOAD)
				'Message' => $message,
				'MessageStructure' => 'json',
			));
		
			//file_put_contents(FCPATH.'/application/logs/__sns.txt', "\n [Publish] Result: ".print_r($response, true), FILE_APPEND);
		
			if(isset($response['MessageId'])) {
				return true;
			}
		} catch(Exception $e) {
			//file_put_contents(FCPATH.'/application/logs/__sns.txt', "\n [Publish] Error: ".$e->getMessage(), FILE_APPEND);
		}
		return false;
	}
	
	/**
	 * Elimina un endpoint
	 * @param string $arn
	 */
	public function deleteEndPoint($arn){
		try {
			$response = $this->sns->DeleteEndpoint(array(
				// ARN
				'EndpointArn' => $arn
			));
				
			//file_put_contents(FCPATH.'/application/logs/__sns.txt', "\n [DeleteEndpoint] Result: ".print_r($response, true), FILE_APPEND);
		
			if(isset($response['EndpointArn'])) {
				return true;
			}
		} catch(Exception $e) {
			//file_put_contents(FCPATH.'/application/logs/__sns.txt', "\n [DeleteEndpoint] Error: ".$e->getMessage(), FILE_APPEND);
		}
		return false;
	}
	
	/**
	 * Envia un mensaje PUSH
	 * @param unknown $app
	 * @param unknown $subject
	 * @param unknown $message
	 * @param number $silent
	 * @param array $arns
	 * @param unknown $idAdvertiser
	 * @param string $versionCode
	 * @param string $id
	 */
	public function sendPush($subject, $message, $silent=0, $arns=array(), $id='', $image = ''){
		$payload = array(
			"type" => "notification",
			"subject" => "$subject",
			"message" => "$message",
			"image" => "$image",
			"id" => "$id",
			"content-available" => "1",
		);
		if($silent==0){
			$payload["sound"]="default";
			$payload["alert"]="$subject";
		}
		$payload = json_encode($payload);
		
		if(!empty($arns)){
			foreach($arns AS $arn){
				$res = $this->publish($payload, 'endpoint', $arn);
			}
		}else{
			$res = $this->publish($payload, 'topic', SNS_TOPIC);
		}
	}
	
}
