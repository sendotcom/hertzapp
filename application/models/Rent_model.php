<?php
class Rent_model extends CI_Model {
	
	const TABLE = 'rent';
	
	public function getById($id){
		return $this->db
			->from(self::TABLE.' AS t')
			->where("(t.id LIKE '$id' OR md5(t.id)='$id')")
			->get()
			->row();
	}
	
	public function getAll($id_user){
		return $this->db
			->select("
				t.id, 
				IFNULL(t.ref_thermeon, '') AS ref, 
				t.start_at, t.end_at, t.days, t.amount,
				lf.code AS from_code, lf.name AS from_name, lf.city AS from_city,
				lt.code AS to_code, lt.name AS to_name, lt.city AS to_city,
				c.name AS car_name,
				c.image AS car_image,
				c.class_id AS car_class_id,
				cc.name AS car_class_name,
				c.passengers AS car_passengers,
				c.transmission AS car_transmission,
				CONCAT(IF(t.end_at < NOW(), '1', '0'),UNIX_TIMESTAMP(t.start_at)) AS sort")
			->from(self::TABLE.' AS t')
			->join(Car_model::TABLE.' AS c', "c.id=t.car_id", 'LEFT')
			->join(Car_Class_model::TABLE.' AS cc', "cc.id=c.class_id", 'LEFT')
			->join(Location_model::TABLE.' AS lf', "lf.id=t.from_id", 'LEFT')
			->join(Location_model::TABLE.' AS lt', "lt.id=t.to_id", 'LEFT')
			->where("t.user_id", $id_user)
			->where("t.status_id !=", 2)
			->order_by('sort', 'ASC')
			->get()
			->result();
	}
	
	public function insert($data){
		$this->db->insert(self::TABLE, $data);
		return $this->db->insert_id();
	}
	
	public function update($id, $data){
		$this->db->where('id', $id);
		$this->db->update(self::TABLE, $data);
		return $this->db->affected_rows()>0;
	}
	
	public function updateByRefThermeon($ref, $data){
		$this->db
			->where('ref_thermeon', $ref)
			->update(self::TABLE, $data);
	}
	
	public function delete($id){
		$this->db
			->where("(id LIKE '$id' OR md5(id)='$id')")
			->delete(self::TABLE);
		return $this->db->affected_rows()>0;
	}
	
	public function getByRef($ref, $id_user=null){
		$where = array("t.ref_thermeon" => $ref);
		if(!empty($id_user)){
			$where["t.user_id"]= $id_user;
		}
		return $this->db
			->from(self::TABLE.' AS t')
			->where($where)
			->get()
			->row();
	}
	
	public function haveActiveRentals($id_user){
		$result = $this->db
			->select("COUNT(t.id) AS total")
			->from(self::TABLE.' AS t')
			->where("t.user_id", $id_user)
			->where("t.status_id !=", 2)
			//->where("t.start_at >", date('Y-m-d 00:00:00'))
			->where("('".date('Y-m-d H:i:s')."' BETWEEN t.start_at AND t.end_at)")
			->get()
			->row();
		return !empty($result) ? ($result->total > 0 ? 1 : 0) : 0;
	}
	
	public function getActiveRent($id_user){
		return $this->db
			->from(self::TABLE.' AS t')
			->where("t.user_id", $id_user)
			->where("t.status_id !=", 2)
			->where("('".date('Y-m-d H:i:s')."' BETWEEN t.start_at AND t.end_at)")
			->get()
			->row();
	}

}