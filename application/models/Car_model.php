<?php
class Car_model extends CI_Model {
	
	const TABLE = 'car';
	
	public function getById($id){
		return $this->db
			->select("t.*, c.name AS class_name")
			->from(self::TABLE.' AS t')
			->join(Car_Class_model::TABLE.' AS c', "c.id=t.class_id", 'LEFT')
			->where("(t.id LIKE '$id' OR md5(t.id)='$id')")
			->get()
			->row();
	}
	
	public function getByClass($class){
		return $this->db
			->select("t.*, c.name AS class_name")
			->from(self::TABLE.' AS t')
			->join(Car_Class_model::TABLE.' AS c', "c.id=t.class_id", 'LEFT')
			->where("t.class_id", $class)
			->get()
			->row();
	}
	
	public function getAll($id_class=null){
		$where = array('t.deleted' => 0);
		if(!empty($class)){
			$where['t.class_id']= $id_class;
		}
		return $this->db
			->select("t.*, c.name AS class_name")
			->from(self::TABLE.' AS t')
			->join(Car_Class_model::TABLE.' AS c', "c.id=t.class_id", 'LEFT')
			->where($where)
			->get()
			->result();
	}
	
	public function insert($data){
		$this->db->insert(self::TABLE, $data);
		return $this->db->insert_id();
	}
	
	public function update($id, $data){
		$this->db->where('id', $id);
		$this->db->update(self::TABLE, $data);
		return $this->db->affected_rows()>0;
	}
	
	public function delete($id){
		$this->update($id, array('deleted' => 1));
	}

}