<?php
class Category_model extends CI_Model {
	
	const TABLE = 'category';
	
	public function __construct()
	{
		// Call the CI_Model constructor
		parent::__construct();
	}
	
	/**
	 * Obtiene la categoria por el ID
	 * @param integer $id
	 */
	public function getCategory($id){
		$result = $this->db->get_where(self::TABLE, "id=$id", 1);
		$result = $result->first_row();
		return isset($result) && !empty($result) ? $result : array();
	}
	
	/**
	 * Obtiene todas las categorias
	 * @return array
	 */
	function getCategories($asObject=true){
		$result = $this->db
			->select('*')
			->get(self::TABLE)
			->result();
		
		if(!$asObject){
			$items = array();
			foreach($result AS $r){
				$items[$r->id]=$r->title;
			}
			return $items;
		}
		
		return $result;
	}

	
}