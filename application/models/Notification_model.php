<?php
class Notification_model extends CI_Model {
	
	const TABLE = 'notification';
	const TABLE_IMAGE = 'notification_image';
	
	public function __construct(){
		// Call the CI_Model constructor
		parent::__construct();
	}
	
	/**
	 * Listado de notificaciones
	 * @param integer $idUser
	 */
	public function getNotifications($versionCode=null){
		$this->db->select("id, 
				title, 
				description AS desc, 
				creation_date AS date, 
				version_code, 
				category_id,
				IFNULL(cdp, '') AS cdp");
		$this->db->where('deleted', 0);
		if(!empty($versionCode)){
			$this->db->where('version_code >', $versionCode);
		}
		$this->db->order_by("creation_date", "ASC");
		$result = $this->db->get(self::TABLE);
		$result = $result->result();
		return isset($result) && !empty($result) ? $result : array();
	}

	/**
	 * Obtener notificacion
	 * @param integer $id
	 */
	public function getNotification($id){
		$result = $this->db
			->where('id', $id)
			->get(self::TABLE)
			->row();
		return isset($result) && !empty($result) ? $result : array();
	}
	
	/**
	 * Inserta una Notificacion
	 * @param array $data
	 */
	public function insert($data){
		$this->db->insert(self::TABLE, $data);
		return $this->db->insert_id();
	}
	
	/**
	 * Actualiza una notificacion
	 * @param integer $id
	 * @param array $data
	 */
	public function update($id, $data){
		$this->db->where('id', $id);
		$this->db->update(self::TABLE, $data);
		return $this->db->affected_rows()>0;
	}
	
	/**
	 * Eliminar una notificacion
	 * @param integer $id
	 */
	public function delete($id){
		$this->db->where('id', $id);
		$this->db->delete(self::TABLE);
		return $this->db->affected_rows()>0;
	}
	
	// IMAGES
	
	/**
	 * Inserta una Imagen para la Notificacion
	 * @param array $data
	 */
	public function insertImage($data){
		$this->db->insert(self::TABLE_IMAGE, $data);
		return $this->db->insert_id();
	}
	
	/**
	 * Obtiene las imagenes de una notificacion
	 * @param integer $id
	 */
	public function getNotificationImages($id){
		$this->db->where('notification_id', $id);
		$result = $this->db->get(self::TABLE_IMAGE);
		$result = $result->result();
		return isset($result) && !empty($result) ? $result : array();
	}
	
	/**
	 * Elimina una imagen de notificacion
	 * @param integer $id
	 */
	public function deleteNotificationImage($id){
		$this->db
			->where('notification_id', $id)
			->delete(self::TABLE_IMAGE);
	}

	/**
	 * Obtiene las notification_deleted
	 * @param integer $id
	 */
	public function getListDeleted(){
		$result = $this->db
			->select('id')
			->where('deleted', 1)
			->get(self::TABLE)
			->result();
		return isset($result) && !empty($result) ? $result : array();
	}

}