<?php
class Discount_model extends CI_Model {
	
	const TABLE = 'discount';
	
	/**
	 * Obtiene un registro por id
	 */
	public function getById($id){
		return $this->db
			->where('id', $id)
			->get(self::TABLE)
			->row();
	}
	
	/**
	 * Inserta un registro
	 * @param integer $id
	 * @param array $data
	 */
	public function insert($data){
		$this->db->insert(self::TABLE, $data);
		return $this->db->insert_id();
	}

	/**
	 * Actualiza la información de un registor
	 * @param integer $id
	 * @param array $data
	 */
	public function update($id, $data){
		$this->db
			->where('id', $id)
			->update(self::TABLE, $data);
	}

	/**
	 * Eliminar un registro
	 * @param integer $id
	 */
	public function delete($id){
		$this->update($id, array('deleted' => 1));
	}
	
	/**
	 * Obtiene un registro por id
	 */
	public function getByCDP($cdp, $validate=true){
		if($validate){
			return $this->db
				->where('cdp', $cdp)
				->where('validity >=', date('Y-m-d'))
				->get(self::TABLE)
				->row();
		}else{
			return $this->db
				->where('cdp', $cdp)
				->get(self::TABLE)
				->row();
		}
	}
	
}
