<?php
class Survey_model extends CI_Model {
	
	const TABLE = 'survey';
	
	public function getById($id){
		return $this->db
			->from(self::TABLE.' AS t')
			->where("(t.id LIKE '$id' OR md5(t.id)='$id')")
			->get()
			->row();
	}
	
	public function insert($data){
		$this->db->insert(self::TABLE, $data);
		return $this->db->insert_id();
	}
	
	public function update($id, $data){
		$this->db->where('id', $id);
		$this->db->update(self::TABLE, $data);
		return $this->db->affected_rows()>0;
	}
	
	public function delete($id){
		$this->db
			->where("(id LIKE '$id' OR md5(id)='$id')")
			->delete(self::TABLE);
		return $this->db->affected_rows()>0;
	}
	
	public function getAll($id_user, $id_corp='H'){
		if($id_corp==FIREFLY_CORP_ID){
			$id_corp = 'F';
		}else{
			$id_corp = 'H';
		}
		
		$results = $this->db
			->select("t.*,
					IFNULL((SELECT 1 FROM ".Survey_Answer_model::TABLE." AS a WHERE a.id_survey=t.id AND a.id_user='$id_user'), 0) AS answered")
			->from(self::TABLE.' AS t')
			->where('t.business', $id_corp)
			->order_by('t.created_at', 'DESC')
			->get()
			->result();
		if(!empty($results) && is_array($results)){
			foreach($results AS $result){
				$result->questions = $this->Survey_Question_model->getAllById($result->id);
			}
		}
		return $results;
	}

}