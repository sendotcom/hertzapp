<?php
class Survey_Answer_model extends CI_Model {
	
	const TABLE = 'survey_answer';
	
	public function get($id_user, $id_survey){
		return $this->db
			->from(self::TABLE.' AS t')
			->where("id_user", $id_user)
			->where("id_survey", $id_survey)
			->get()
			->row();
	}
	
	public function insert($data){
		$this->db->insert(self::TABLE, $data);
		return $this->db->affected_rows()>0;
	}
	
	public function delete($id_user, $id_survey){
		$this->db
			->where("id_user", $id_user)
			->where("id_survey", $id_survey)
			->delete(self::TABLE);
		return $this->db->affected_rows()>0;
	}

}