<?php
class Survey_Question_model extends CI_Model {
	
	const TABLE = 'survey_question';
	const TYPE_INPUT = 'input';
	const TYPE_RADIO = 'radio';
	const TYPE_STAR = 'star';
	
	public function getById($id){
		return $this->db
			->from(self::TABLE.' AS t')
			->where("(t.id LIKE '$id' OR md5(t.id)='$id')")
			->get()
			->row();
	}
	
	public function insert($data){
		$this->db->insert(self::TABLE, $data);
		return $this->db->insert_id();
	}
	
	public function update($id, $data){
		$this->db->where('id', $id);
		$this->db->update(self::TABLE, $data);
		return $this->db->affected_rows()>0;
	}
	
	public function delete($id){
		$this->db
			->where("(id LIKE '$id' OR md5(id)='$id')")
			->delete(self::TABLE);
		return $this->db->affected_rows()>0;
	}
	
	public function deleteNotIn($id, $not_in_array=array()){
		$this->db
			->where("survey_id", $id)
			->where_not_in("id", $not_in_array)
			->delete(self::TABLE);
		return $this->db->affected_rows()>0;
	}
	
	public function getAllById($id){
		return $this->db
			->select("t.id,
					t.title,
					t.type,
					IFNULL(t.opt_1,'') AS opt_1,
					IFNULL(t.opt_2,'') AS opt_2,
					IFNULL(t.opt_3,'') AS opt_3,
					IFNULL(t.opt_4,'') AS opt_4")
			->from(self::TABLE.' AS t')
			->where("survey_id", $id)
			->get()
			->result();
	}

}