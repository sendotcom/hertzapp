<?php
class Car_Class_model extends CI_Model {
	
	const TABLE = 'car_class';
	
	public function getById($id){
		return $this->db
			->from(self::TABLE.' AS t')
			->where("t.id", $id)
			->get()
			->row();
	}
	
	function getAll($asObject=true){
		$result = $this->db
			->select('*')
			->order_by('id', 'ASC')
			->get(self::TABLE)
			->result();
		
		if(!$asObject){
			$items = array();
			foreach($result AS $r){
				$items[$r->id]=$r->name;
			}
			return $items;
		}
		
		return $result;
	}
	
	public function insert($data){
		$this->db->insert(self::TABLE, $data);
		return $this->db->affected_rows()>0;
	}
	
	public function update($id, $data){
		$this->db->where('id', $id);
		$this->db->update(self::TABLE, $data);
		return $this->db->affected_rows()>0;
	}
	
	public function delete($id){
		$this->db
			->where("id", $id)
			->delete(self::TABLE);
		return $this->db->affected_rows()>0;
	}

	
}