<?php
class Car_Extra_model extends CI_Model {
	
	const TABLE = 'car_extra';

	/**
	 * Obtiene un registro por id
	 */
	public function getById($id){
		return $this->db
			->where('id', $id)
			->get(self::TABLE)
			->row();
	}

	/**
	 * Inserta un registro
	 * @param integer $id
	 * @param array $data
	 */
	public function insert($data){
		$this->db->insert(self::TABLE, $data);
		return $this->db->insert_id();
	}

	/**
	 * Actualiza la información de un registor
	 * @param integer $id
	 * @param array $data
	 */
	public function update($id, $data){
		$this->db
			->where('id', $id)
			->update(self::TABLE, $data);
	}

	/**
	 * Eliminar un registro
	 * @param integer $id
	 */
	public function delete($id){
		$this->update($id, array('deleted' => 1));
	}

	/**
	 * Extrae todo los tipo de Vehículos
	 * @param bool|true $asObject
	 * @return array
	 */
	function getAll($asObject=true){
		$result = $this->db
			->select('*')
			->where('deleted', 0)
			->order_by('type', 'ASC')
			->order_by('code', 'ASC')
			->get(self::TABLE)
			->result();
		
		if(!$asObject){
			$items = array();
			foreach($result AS $r){
				$items[$r->id]=$r->name;
			}
			return $items;
		}
		
		return $result;
	}
	
	public function getByCode($code){
		return $this->db
			->where('code', $code)
			->get(self::TABLE)
			->row();
	}
	
}