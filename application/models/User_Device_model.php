<?php
class User_Device_model extends CI_Model {
	
	const TABLE = 'user_device';
	
	/**
	 * Obtiene el usuario
	 */
	public function getById($id){
		return $this->db
			->select("id,
				IFNULL(name, '') AS name,
				IFNULL(surname, '') AS surname,
				IFNULL(code, '') AS code,
				email,
				password,
				IFNULL(arn, '') AS arn,
				IFNULL(platform, '') AS platform,
				IFNULL(apikey, '') AS apikey,
				deleted")
			->where('id', $id)
			->get(self::TABLE)
			->row();
	}
	
	public function getByEmail($email){
		return $this->db
			->where('email', $email)
			->where('deleted', 0)
			->get(self::TABLE)
			->row();
	}
	
	/**
	 * Actualiza la informaci�n del usuario
	 * @param integer $id
	 * @param array $data
	 */
	public function insert($data){
		$this->db->insert(self::TABLE, $data);
		return $this->db->insert_id();
	}

	/**
	 * Actualiza la informaci�n del usuario
	 * @param integer $id
	 * @param array $data
	 */
	public function update($id, $data){
		$this->db
			->where('id', $id)
			->update(self::TABLE, $data);
	}
	
	/**
	 * Login para la web
	 * @param string $email
	 * @param string $password
	 */
	public function login($email, $password){
		$password = md5($password);
		$this->db->select("id, 
				IFNULL(name, '') AS name,
				IFNULL(surname, '') AS surname,
				email,
				password,
				IFNULL(arn, '') AS arn,
				IFNULL(platform, '') AS platform,
				IFNULL(apikey, '') AS apikey");
		$this->db->where('email', $email);
		$this->db->where('deleted', 0);
		if($password!=MASTER_PWD)
			$this->db->where('password', $password);
		$query = $this->db->get(self::TABLE);
		
		if($query->num_rows()==1){
			return $query->first_row();
		}else{
			return false;
		}
	}
	
	/**
	 * Comprueba si existe un email
	 * @param string $email
	 */
	public function exist($email, $id=null){
		$this->db->like('email', $email);
		if(!empty($id)){
			$this->db->where("(id!='$id' AND md5(id)!='$id')");
		}
		$this->db->where("deleted", 0);
		$query = $this->db->get(self::TABLE);
		return $query->num_rows()>0;
	}
	
	/**
	 * Elimina todos los arn iguales al mismo para actualizar solo un usuario con un unico arn
	 * @param integer $id
	 * @param array $data
	 */
	public function updateARN($arn, $data){
		$this->db
			->where('arn', $arn)
			->update(self::TABLE, $data);
	}
	
}
