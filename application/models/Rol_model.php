<?php
class Rol_model extends CI_Model {
	
	const TABLE = 'rol';
	
	public function __construct()
	{
		// Call the CI_Model constructor
		parent::__construct();
	}
	
	/**
	 * Obtiene todas las categorias
	 * @return array
	 */
	function getList($asObject=true){
		$result = $this->db
			->select('*')
			->get(self::TABLE)
			->result();
		
		if(!$asObject){
			$items = array();
			foreach($result AS $r){
				$items[$r->id]=$r->name;
			}
			return $items;
		}
		
		return $result;
	}

	
}