<?php
class Rent_Extra_model extends CI_Model {
	
	const TABLE = 'rent_extra';
	
	public function getAll($id_rent, $forApi=false){
		return $this->db
			->select($forApi ? "t.extra_id AS id, e.name" : "t.*, e.name")
			->from(self::TABLE.' AS t')
			->join(Car_Extra_model::TABLE.' AS e', "e.id=t.extra_id", 'LEFT')
			->where("t.rent_id", $id_rent)
			->get()
			->result();
	}
	
	public function insert($data){
		$this->db->insert(self::TABLE, $data);
		return $this->db->insert_id();
	}
	
	public function update($id, $data){
		$this->db->where('id', $id);
		$this->db->update(self::TABLE, $data);
		return $this->db->affected_rows()>0;
	}
	
	public function delete($id){
		$this->db
			->where("(id LIKE '$id' OR md5(id)='$id')")
			->delete(self::TABLE);
		return $this->db->affected_rows()>0;
	}

}