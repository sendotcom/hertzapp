<?php
class Location_model extends CI_Model {
	
	const TABLE = 'location';
	
	public function getById($id){
		return $this->db
			->from(self::TABLE.' AS t')
			->where("(t.id LIKE '$id' OR md5(t.id)='$id')")
			->get()
			->row();
	}
	
	/**
	 * Inserta un registro
	 * @param integer $id
	 * @param array $data
	 */
	public function insert($data){
		$this->db->insert(self::TABLE, $data);
		return $this->db->insert_id();
	}
	
	/**
	 * Actualiza la información de un registor
	 * @param integer $id
	 * @param array $data
	 */
	public function update($id, $data){
		$this->db
		->where('id', $id)
		->update(self::TABLE, $data);
	}
	
	/**
	 * Eliminar un registro
	 * @param integer $id
	 */
	public function delete($id){
		$this->update($id, array('deleted' => 1));
	}
	
	public function getByCode($code){
		return $this->db
			->from(self::TABLE.' AS t')
			->where("t.code", $code)
			->get()
			->row();
	}
	
	function getAll($text='', $corp_id='H'){
		if($corp_id==FIREFLY_CORP_ID){
			$corp_id = 'F';
		}else{
			$corp_id = 'H';
		}
		
		if(!empty($text)){
			return $this->db
				->where('business', $corp_id)
				->where('deleted', 0)
				->or_like('name', $text)
				->or_like('address_1', $text)
				->or_like('address_2', $text)
				->order_by('name', 'ASC')
				->get(self::TABLE)
				->result();
		}else{
			return $this->db
				->where('business', $corp_id)
				->where('deleted', 0)
				->order_by('name', 'ASC')
				->get(self::TABLE)
				->result();
		}
	}
	
	function getNearly($lat, $lon, $corp_id='H'){
		if($corp_id==FIREFLY_CORP_ID){
			$corp_id = 'F';
		}else{
			$corp_id = 'H';
		}
		
		return $this->db
			->select("*, 
					( 6371 * acos( cos( radians($lat) ) * cos( radians( lat ) ) * cos( radians( lon ) - radians($lon) ) + sin( radians($lat) ) * sin( radians( lat ) ) ) ) AS distance")
			->where("(lat IS NOT NULL AND lat!='')")
			->where("(lon IS NOT NULL AND lon!='')")
			->where('business', $corp_id)
			->where('deleted', 0)
			->order_by('distance', 'ASC')
			->get(self::TABLE)
			->row();
	}
	
}