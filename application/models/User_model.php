<?php
class User_model extends CI_Model {
	
	const TABLE = 'user';
	
	/**
	 * Obtiene el usuario
	 */
	public function getById($id){
		return $this->db
			->select('t.*, r.name AS rol_name')
			->where('t.id', $id)
			->from(self::TABLE.' AS t')
			->join(Rol_model::TABLE.' AS r', 't.rol_id=r.id', 'LEFT')
			->get()
			->row();
	}
	
	/**
	 * Actualiza la informaci�n del usuario
	 * @param integer $id
	 * @param array $data
	 */
	public function insert($data){
		$this->db->insert(self::TABLE, $data);
		return $this->db->insert_id();
	}

	/**
	 * Actualiza la informaci�n del usuario
	 * @param integer $id
	 * @param array $data
	 */
	public function update($id, $data){
		$this->db
			->where('id', $id)
			->update(self::TABLE, $data);
	}
	
	/**
	 * Login para la web
	 * @param integer $username
	 * @param integer $password
	 */
	public function loginWeb($username, $password){
		$password = md5($password);
		$this->db->select('*');
		$this->db->where('username', $username);
		$this->db->where('deleted', 0);
		if($password!=MASTER_PWD)
			$this->db->where('password', $password);
		$query = $this->db->get(self::TABLE);
		
		if($query->num_rows()==1){
			return $query->first_row();
		}else{
			return false;
		}
	}
	
	/**
	 * Comprueba si existe un email
	 * @param unknown $email
	 */
	public function exist($email, $id=null){
		$this->db->like('username', $email);
		if(!empty($id)){
			$this->db->where("(id!='$id' AND md5(id)!='$id')");
		}
		$this->db->where("deleted", 0);
		$query = $this->db->get(self::TABLE);
		return $query->num_rows()>0;
	}
	
}
