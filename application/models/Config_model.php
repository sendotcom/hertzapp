<?php
class Config_model extends CI_Model {
	
	const TABLE = 'config';
	
	public function get(){
		return $this->db
			->select("t.id,
					IFNULL(t.privacy, '') AS privacy,
					IFNULL(t.terms, '') AS terms,
					IFNULL(t.email_polls, '') AS email_polls,
					IFNULL(t.email_sos, '') AS email_sos")
			->from(self::TABLE.' AS t')
			->get()
			->row();
	}

	public function update($id, $data){
		$this->db->where('id', $id);
		$this->db->update(self::TABLE, $data);
		return $this->db->affected_rows()>0;
	}
	
}