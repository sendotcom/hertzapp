<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH.'/libraries/MainController.php';
class Rent extends MainController {
	
	private $class_name = 'rent';
	
	/**
	 * Lista
	 */
	public function index()
	{
		$this->data['class_name']= $this->class_name;
		
		$this->template->write_view('content', "$this->class_name/index", $this->data, TRUE);
		$this->template->render();
	}
	
	/**
	 * Ver
	 */
	public function view(){
		$rent = $this->Rent_model->getById($this->get('id'));
		if(empty($rent)){
			redirect("/$this->class_name");
		}
		
		//PARAMETROS
		$this->data['class_name']= $this->class_name;
		$this->data['rent']= $rent;
		$this->data['extras']= $this->Rent_Extra_model->getAll($rent->id);
		$this->data['car']= $this->Car_model->getById($rent->car_id);
		$this->data['user']= $this->User_Device_model->getById($rent->user_id);
		$this->data['from']= $this->Location_model->getById($rent->from_id);
		$this->data['to']= $this->Location_model->getById($rent->to_id);
		
		$this->template->write_view('content', "/$this->class_name/view", $this->data, TRUE);
		$this->template->render();
	}
	
	/**
	 * Paginacion listado
	 */
	public function pagination(){
		//PARAMETROS
		$query_filter = $this->input->post('search');
		$query_filter = strtolower($query_filter['value']);
		$query_sort = $this->input->post('order');
		$query_sort_direction = strtolower($query_sort[0]['dir']);
		$query_sort = strtolower($query_sort[0]['column']);
		$query_offset = $this->input->post('start');
		$query_limit = $this->input->post('length');
		$query_draw = $this->input->post('draw');
	
		//Resultado TOTAL
		$result_total = $this->db
			->select('COUNT(*) AS total')
			->get(Rent_model::TABLE)
			->row();
		$result_total_filtered = $result_total = $result_total->total;
	
		//FILTROS y SELECT
		if(!empty($query_filter)){
			$this->db->where("(t.ref like '%$query_filter%' 
					OR t.ref_thermeon like '%$query_filter%'
					OR t.start_at like '%$query_filter%'
					OR t.end_at like '%$query_filter%'
					OR lf.name like '%$query_filter%'
					OR lt.name like '%$query_filter%')");
		}
		$select = array('t.rented_at', 't.start_at', 't.end_at', 't.ref', 't.car_id', 't.id');
		$this->db->select(implode(',', $select).', 
				t.ref_thermeon, 
				lf.name AS from_name, 
				lt.name AS to_name,
				c.name AS car_name, c.class_id AS car_class');
	
		//Resultado PAGINADO
		$this->db->from(Rent_model::TABLE.' AS t');
		$this->db->join(Location_model::TABLE.' AS lf', 't.from_id=lf.id', 'LEFT');
		$this->db->join(Location_model::TABLE.' AS lt', 't.to_id=lt.id', 'LEFT');
		$this->db->join(Car_model::TABLE.' AS c', 't.car_id=c.id', 'LEFT');
		if(isset($select[$query_sort])){
			$this->db->order_by($select[$query_sort], $query_sort_direction);
		}else{
			$this->db->order_by('t.start_at', 'ASC');
		}
		$this->db->limit($query_limit, $query_offset);
		$query = $this->db->get();
	
		$result_data = array();
		foreach($query->result() AS $row){
			//Botones de Accion
			$toolbar = '<div class="btn-group">';
			$toolbar.= '<a href="'.base_url("$this->class_name/view").'?id='.md5($row->id).'" class="btn btn-info btn-sm" title="'.$this->lang->line("edit").'"><i class="fa fa-info fa-lg"></i> '.$this->lang->line('see').'</a>';
			$toolbar.= '</div>';
	
			//Fila
			$result_data[]=array(
				$row->rented_at,
				"<b>$row->start_at</b><br/><i>$row->from_name</i>",
				"<b>$row->end_at</b><br/><i>$row->to_name</i>",
				$row->ref.(!empty($row->ref_thermeon) ? " ($row->ref_thermeon)" : ''),
				"$row->car_name ($row->car_class)",
				$toolbar,
			);
		}
		$result_total_filtered = !empty($query_filter) ? count($result_data) : $result_total;
	
		//Respuesta
		echo json_encode(array(
			'draw' => intval($query_draw),
			'recordsTotal' => intval($result_total),
			'recordsFiltered' => intval($result_total_filtered),
			'data' => $result_data,
		));
	}
	
}