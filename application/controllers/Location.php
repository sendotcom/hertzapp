<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/MainController.php';

class Location extends MainController {
	
	private $class_name = 'location';
	
	function __construct()
	{
		// Construct the parent class
		parent::__construct();
	}

	/**
	 * Vista para listar
	 */
	public function index(){
		$this->data['class_name']= $this->class_name;

		$this->template->write_view('content', "$this->class_name/index", $this->data, TRUE);
		$this->template->render();
	}

	/**
	 * Vista para crear
	 */
	public function create(){
		//REGLAS VALIDACION
		$this->load->library('form_validation');
		$this->form_validation->set_rules ('code', $this->lang->line("code"), 'trim|required|max_length[20]|is_unique['.Location_model::TABLE.'.code]');
		$this->form_validation->set_rules ('name', $this->lang->line("name"), 'trim|required|max_length[65]');
		$this->form_validation->set_rules ('address_1', $this->lang->line("address_1"), 'trim|max_length[128]');
		$this->form_validation->set_rules ('address_2', $this->lang->line("address_2"), 'trim|max_length[65]');
		$this->form_validation->set_rules ('zip', $this->lang->line("zip"), 'trim|max_length[20]');
		$this->form_validation->set_rules ('phone', $this->lang->line("phone"), 'trim|max_length[20]');
		$this->form_validation->set_rules ('city', $this->lang->line("city"), 'trim|max_length[45]');
		$this->form_validation->set_rules ('hours', $this->lang->line("hours"), 'trim|max_length[128]');
		$this->form_validation->set_rules ('lat', $this->lang->line("lat"), 'trim|required|max_length[20]');
		$this->form_validation->set_rules ('lon', $this->lang->line("lon"), 'trim|required|max_length[20]');
		$this->form_validation->set_rules ('business', $this->lang->line("business"), 'trim|required');
		$this->form_validation->set_message ('required', $this->lang->line('validation_required'));
		$this->form_validation->set_message ('max_length', $this->lang->line('validation_max_length'));

		//PARAMETROS
		$this->data['mode']= 'create';
		$this->data['url']= base_url("$this->class_name/create");
		$this->data['code']= $this->post('code');
		$this->data['name']= $this->post('name');
		$this->data['address_1']= $this->post('address_1');
		$this->data['address_2']= $this->post('address_2');
		$this->data['zip']= $this->post('zip');
		$this->data['phone']= $this->post('phone');
		$this->data['city']= $this->post('city');
		$this->data['hours']= $this->post('hours');
		$this->data['lat']= $this->post('lat');
		$this->data['lon']= $this->post('lon');
		$this->data['business']= $this->post('business');
		$this->data['class_name']= $this->class_name;

		//GUARDAR
		if(!empty($_POST)){
			if($this->form_validation->run () == TRUE){
				//Insertar
				$id = $this->Location_model->insert(array(
					'code' => $this->data['code'],
					'name' => $this->data['name'],
					'address_1' => $this->data['address_1'],
					'address_2' => $this->data['address_2'],
					'zip' => $this->data['zip'],
					'phone' => $this->data['phone'],
					'city' => $this->data['city'],
					'hours' => $this->data['hours'],
					'lat' => $this->data['lat'],
					'lon' => $this->data['lon'],
					'business' => $this->data['business'],
				));
	
				if(!empty($id)){
					$this->session->set_flashdata('success', $this->lang->line('location_saved'));
					redirect("/$this->class_name");
				}
			}
		}else{
			$this->data['business']= "H";
		}
		
		$this->template->write_view('content', "$this->class_name/form", $this->data, TRUE);
		$this->template->render();
	}

	/**
	 * Vista para actualizar una clase de vehiculo
	 */
	public function update(){
		$location = $this->Location_model->getById($this->get('id'));
		if(empty($location)){
			redirect("/$this->class_name");
		}
		
		//REGLAS VALIDACION
		$this->load->library('form_validation');
		$this->form_validation->set_rules ('name', $this->lang->line("name"), 'trim|required|max_length[65]');
		$this->form_validation->set_rules ('address_1', $this->lang->line("address_1"), 'trim|max_length[128]');
		$this->form_validation->set_rules ('address_2', $this->lang->line("address_2"), 'trim|max_length[65]');
		$this->form_validation->set_rules ('zip', $this->lang->line("zip"), 'trim|max_length[20]');
		$this->form_validation->set_rules ('phone', $this->lang->line("phone"), 'trim|max_length[20]');
		$this->form_validation->set_rules ('city', $this->lang->line("city"), 'trim|max_length[45]');
		$this->form_validation->set_rules ('hours', $this->lang->line("hours"), 'trim|max_length[128]');
		$this->form_validation->set_rules ('lat', $this->lang->line("lat"), 'trim|required|max_length[20]');
		$this->form_validation->set_rules ('lon', $this->lang->line("lon"), 'trim|required|max_length[20]');
		$this->form_validation->set_rules ('business', $this->lang->line("business"), 'trim|required');
		$this->form_validation->set_message ('required', $this->lang->line('validation_required'));
		$this->form_validation->set_message ('max_length', $this->lang->line('validation_max_length'));
		
		//PARAMETROS
		$this->data['mode']= 'update';
		$this->data['url']= base_url("$this->class_name/update=id=$location->id");
		$this->data['name']= $this->post('name');
		$this->data['address_1']= $this->post('address_1');
		$this->data['address_2']= $this->post('address_2');
		$this->data['zip']= $this->post('zip');
		$this->data['phone']= $this->post('phone');
		$this->data['city']= $this->post('city');
		$this->data['hours']= $this->post('hours');
		$this->data['lat']= $this->post('lat');
		$this->data['lon']= $this->post('lon');
		$this->data['business']= $this->post('business');
		$this->data['class_name']= $this->class_name;
		
		//GUARDAR
		if(!empty($_POST)){
			if($this->form_validation->run () == TRUE){
				//Actualizar
				$this->Location_model->update($location->id, array(
					'code' => $this->data['code'],
					'name' => $this->data['name'],
					'address_1' => $this->data['address_1'],
					'address_2' => $this->data['address_2'],
					'zip' => $this->data['zip'],
					'phone' => $this->data['phone'],
					'city' => $this->data['city'],
					'hours' => $this->data['hours'],
					'lat' => $this->data['lat'],
					'lon' => $this->data['lon'],
					'business' => $this->data['business'],
				));
		
				$this->session->set_flashdata('success', $this->lang->line('location_saved'));
				redirect("/$this->class_name");
			}
		}else{
			//CARGAR DE BD
			if(!empty($location)){
				$this->data['code']= $location->code;
				$this->data['name']= $location->name;
				$this->data['address_1']= $location->address_1;
				$this->data['address_2']= $location->address_2;
				$this->data['zip']= $location->zip;
				$this->data['phone']= $location->phone;
				$this->data['city']= $location->city;
				$this->data['hours']= $location->hours;
				$this->data['lat']= $location->lat;
				$this->data['lon']= $location->lon;
				$this->data['business']= $location->business;
			}
		}
		
		$this->template->write_view('content', "$this->class_name/form", $this->data, TRUE);
		$this->template->render();
	}

	/**
	 * Borrar usuario
	 */
	public function delete(){
		if($this->Location_model->delete($this->get('id')))
			$this->session->set_flashdata('success', $this->lang->line('location_deleted'));
		else
			$this->session->set_flashdata('error', $this->lang->line('location_no_deleted'));
		redirect("/$this->class_name");
	}
	
	/**
	 * Importar
	 */
	public function import(){
		//PARAMETROS
		$data = array(
			'url' => base_url("$this->class_name/import"),
			'class_name' => $this->class_name,
		);
	
		if(!empty($_FILES['file'])){
			$file = $_FILES['file']['name'];
			if(!empty($file) && $this->security->xss_clean($file, TRUE)){
				$imported = 0;
	
				//Apertura de fichero
				if (($handle = fopen($_FILES['file']['tmp_name'], "r")) !== FALSE) {
					$i = 0;
					while (($row = fgetcsv($handle, 1000, ";")) !== FALSE) {
						if($i>=1 && $i<=1000){
							$code = trim(utf8_encode($row[0]));
							
							$attr = array(
								'code' => $code,
								'name' => trim(utf8_encode($row[1])),
								'address_1' => trim(utf8_encode($row[2])),
								'address_2' => trim(utf8_encode($row[3])),
								'city' => trim(utf8_encode($row[4])),
								'zip' => trim(utf8_encode($row[5])),
								'lat' => str_replace(',', '.', trim(utf8_encode($row[6]))),
								'lon' => str_replace(',', '.', trim(utf8_encode($row[7]))),
								'phone' => trim(utf8_encode($row[8])),
								'hours' => trim(utf8_encode($row[9])),
								'business' => trim(utf8_encode($row[10])),
								'deleted' => intval(trim(utf8_encode($row[11]))),
							);
	
							$item = $this->Location_model->getByCode($code);
							if(!empty($item)){
								$this->Location_model->update($item->id, $attr);
								$imported++;
							}else{
								$id = $this->Location_model->insert($attr);
								if(!empty($id)){
									$imported++;
								}
							}
						}
						$i++;
					}
					
					$this->session->set_flashdata('success', sprintf($this->lang->line("locations_imported"), $imported, $i-1));
					redirect("$this->class_name/import");
				}
			}
		}
	
		$this->template->write_view('content', "$this->class_name/import", $data, TRUE);
		$this->template->render();
	}
	
	/**
	 * Formato para importar
	 */
	public function import_format(){
		$public_dir = "/uploads/tmp/";
		$real_path = FCPATH.$public_dir;
		$this->checkDir($real_path);
		$file_name = 'importador_oficinas_'.time().'.csv';
	
		$fp = fopen($real_path.$file_name, 'w');
	
		fputcsv($fp, [
			utf8_decode('CÓDIGO'),
			utf8_decode('NOMBRE'),
			utf8_decode('DIRECCIÓN 1'),
			utf8_decode('DIRECCIÓN 2'),
			utf8_decode('CIUDAD'),
			utf8_decode('CÓDIGO POSTAL'),
			utf8_decode('LAT'),
			utf8_decode('LON'),
			utf8_decode('PHONE'),
			utf8_decode('HORARIO'),
			utf8_decode('EMPRESA'),
			utf8_decode('ELIMINAR'),
		], ';');
	
		fclose($fp);
		redirect(base_url($public_dir.$file_name));
	}
	
	/**
	 * Pagina el listado
	 */
	public function pagination(){
		//PARAMETROS
		$query_filter = $this->input->post('search');
		$query_filter = strtolower($query_filter['value']);
		$query_sort = $this->input->post('order');
		$query_sort_direction = strtolower($query_sort[0]['dir']);
		$query_sort = strtolower($query_sort[0]['column']);
		$query_offset = $this->input->post('start');
		$query_limit = $this->input->post('length');
		$query_draw = $this->input->post('draw');
		
		//Resultado TOTAL
		$this->db->select('COUNT(*) AS total');
		$query = $this->db
			->where('deleted', 0)
			->get(Location_model::TABLE)
			->row();
		$result_total_filtered = $result_total =  $query->total;
		
		//FILTROS y SELECT
		if(!empty($query_filter)){
			$this->db->where("(t.code='$query_filter' OR t.name like '%$query_filter%' OR t.city like '%$query_filter%' OR IF(t.business='H', 'Hertz', 'Firefly') like '%$query_filter%')");
		}
		$select = array('t.code', 't.name', 't.city', "IF(t.business='H', 'Hertz', 'Firefly') AS business",'t.id');
		$this->db->select(implode(',', $select));

		//Resultado PAGINADO
		$this->db->from(Location_model::TABLE.' AS t');
		$this->db->where('deleted', 0);

		if(isset($select[$query_sort])){
			$this->db->order_by($select[$query_sort], $query_sort_direction);
		}else{
			$this->db->order_by('name', 'ASC');
		}
		$this->db->limit($query_limit, $query_offset);
		
		$query = $this->db->get();
		$result_data = array();
		foreach($query->result() AS $row){
			//Botones de Accion
			$toolbar = '<div class="btn-group">';
			$toolbar.= '<a href="'.base_url("$this->class_name/update").'?id='.$row->id.'" class="btn btn-blue btn-sm"><i class="fa fa-edit fa-lg"></i> '.$this->lang->line('edit').'</a>';
			$toolbar.= '<a class="btn btn-danger btn-sm btn-confirm-delete" data-toggle="modal" data-target="#deleteModal" data-action="'.base_url("$this->class_name/delete")."?id=".$row->id.'" data-backdrop="true" title="'.$this->lang->line("delete").'"><i class="fa fa-trash fa-lg"></i> '.$this->lang->line('delete').'</a>';
			$toolbar.= '</div>';
			
			//Fila
			$result_data[]=array(
				$row->code,
				$row->name,
				$row->city,
				$row->business,
				$toolbar,
			);
		}
		$result_total_filtered = !empty($query_filter) ? count($result_data) : $result_total;
	
		//Respuesta
		echo json_encode(array(
			'draw' => intval($query_draw),
			'recordsTotal' => intval($result_total),
			'recordsFiltered' => intval($result_total_filtered),
			'data' => $result_data,
		));
	}

}