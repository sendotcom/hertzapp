<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	
	function __construct()
	{
		// Construct the parent class
		parent::__construct();
	}

	/**
	 * Vista de login
	 */
	public function index()
	{
		$this->template->set_template('login');
		
		$this->load->library('form_validation');
		$this->form_validation->set_rules ('username', $this->lang->line('user'), 'trim|required');
		$this->form_validation->set_rules ('password', $this->lang->line('password'), 'trim|required|callback_validate');
		$this->form_validation->set_message ('required', $this->lang->line('validation_required'));
		
		$data = array(
			'username' => $this->input->post('username'),
			'password' => $this->input->post('password')
		);
		
		if($this->form_validation->run () == TRUE){
			$this->session->set_flashdata('success', $this->lang->line('welcome')." ".$this->session->userdata('logged_in')->name."!");
			redirect ('welcome');
		}
		
		$this->template->write_view('content', 'login/login', $data, TRUE);
		$this->template->render();
	}
	
	/**
	 * Validacion de login
	 * @param string $password
	 */
	public function validate($password){
		$username = $this->input->post('username');
		
		$user = $this->User_model->loginWeb($username, $password);
		if($user){
			$this->session->set_userdata('logged_in', $user);
			return true;
		}else{
			$this->form_validation->set_message('validate', $this->lang->line('validation_login'));
			return false;
		}
	} 
	
}
