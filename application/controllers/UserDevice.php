<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/MainController.php';

class UserDevice extends MainController {
	
	function __construct()
	{
		// Construct the parent class
		parent::__construct();
	}

	/**
	 * Vista para listar notificaciones
	 */
	public function index(){
		$data = array();

		$this->template->write_view('content', 'userDevice/index', $data, TRUE);
		$this->template->render();
	}

	/**
	 * Vista para actualizar un usuario
	 */
	public function update(){
		$id = $this->input->get('id');
		$user = $this->User_Device_model->getById($id);
		if(empty($user)){
			redirect(base_url('userDevice'));
		}
		
		//REGLAS VALIDACION
		$this->load->library('form_validation');
		$this->form_validation->set_rules ('name', $this->lang->line("name"), 'trim|required|max_length[45]');
		$this->form_validation->set_rules ('surname', $this->lang->line("name"), 'trim|max_length[45]');
		$this->form_validation->set_rules ('email', $this->lang->line("email"), 'trim|required|max_length[45]');
		$this->form_validation->set_rules ('password', $this->lang->line("password"), 'trim|max_length[45]');
		$this->form_validation->set_message ('required', $this->lang->line('validation_required'));
		$this->form_validation->set_message ('max_length', $this->lang->line('validation_max_length'));

		//PARAMETROS
		$data = array(
			'mode' => 'update',
			'url' => base_url('userDevice/update?id='.$id),
			'name' => $this->post('name'),
			'surname' => $this->post('surname'),
			'email' => $this->post('email'),
			'password' => $this->post('password'),
			'user' => $user,
		);

		//GUARDAR
		if($this->form_validation->run () == TRUE){
			$attr = array(
				'name' => $data['name'],
				'surname' => $data['surname'],
				'email' => $data['email'],
			);
			if(!empty($data['password'])){
				$attr['password']=md5($data['password']);
			}
			
			//Actualizar
			$this->User_Device_model->update($id, $attr);

			$this->session->set_flashdata('success', $this->lang->line('user_saved'));
			redirect(base_url('userDevice'));
		}else{
			//CARGAR DE BD
			$data['name']=$user->name;
			$data['surname']=$user->surname;
			$data['email']=$user->email;
		}

		$this->template->write_view('content', 'userDevice/form', $data, TRUE);
		$this->template->render();
	}

	/**
	 * Borrar usuario
	 */
	public function delete(){
		$id = $this->post('id');
		$this->User_Device_model->update($id, array(
			'deleted' => 1,
		));
		
		$this->session->set_flashdata('success', $this->lang->line('user_deleted'));
		redirect('userDevice');
	}
	
	/**
	 * Pagina el listado de Notificaciones
	 */
	public function pagination(){
		//PARAMETROS
		$query_filter = $this->input->post('search');
		$query_filter = strtolower($query_filter['value']);
		$query_sort = $this->input->post('order');
		$query_sort_direction = strtolower($query_sort[0]['dir']);
		$query_sort = strtolower($query_sort[0]['column']);
		$query_offset = $this->input->post('start');
		$query_limit = $this->input->post('length');
		$query_draw = $this->input->post('draw');
		
		//Resultado TOTAL
		$this->db->select('COUNT(*) AS total');
		$this->db->where('deleted', 0);
		$query = $this->db->get(User_Device_model::TABLE)->row();
		$result_total_filtered = $result_total =  $query->total;
		
		//FILTROS y SELECT
		if(!empty($query_filter)){
			$this->db->where("(t.id='$query_filter' OR t.email like '%$query_filter%' OR t.name like '%$query_filter%' OR t.surname like '%$query_filter%')");
		}
		$select = array('t.name', 't.surname', 't.email', 't.platform', 't.id');
		$this->db->select(implode(',', $select));
		$this->db->where('deleted', 0);
	
		//Resultado PAGINADO
		$this->db->from(User_Device_model::TABLE.' AS t');
		
		if(isset($select[$query_sort])){
			$this->db->order_by($select[$query_sort], $query_sort_direction);
		}else{
			$this->db->order_by('name,surname', 'ASC');
		}
		$this->db->limit($query_limit, $query_offset);
		
		$query = $this->db->get();
		$result_data = array();
		foreach($query->result() AS $row){
			//Botones de Accion
			$toolbar = '<div class="btn-group">';
			$toolbar.= '<a href="'.base_url('userDevice/update').'?id='.$row->id.'" class="btn btn-blue btn-sm"><i class="fa fa-edit fa-lg"></i> '.$this->lang->line('edit').'</a>';
			$toolbar.= '<a class="btn btn-danger btn-sm" data-toggle="modal" data-target="#deleteModal" data-id="'.$row->id.'" data-backdrop="true"><i class="fa fa-trash fa-lg"></i> '.$this->lang->line('delete').'</a>';
			$toolbar.= '</div>';
			
			//Fila
			$result_data[]=array(
				$row->name,
				$row->surname,
				$row->email,
				($row->platform=='ios' ? '<i class="fa fa-apple fa-lg"></i>' : '<i class="fa fa-android fa-lg text-success"></i>'),
				$toolbar,
			);
		}
		$result_total_filtered = !empty($query_filter) ? count($result_data) : $result_total;
	
		//Respuesta
		$response = array(
			'draw' => intval($query_draw),
			'recordsTotal' => intval($result_total),
			'recordsFiltered' => intval($result_total_filtered),
			'data' => $result_data,
		);
		echo json_encode($response);
	}
	
	/**
	 * Validacion Email unico
	 * @param string $username
	 */
	public function is_unique($email){
		$id = isset($_GET['id']) ? $_GET['id'] : null;
	
		$user = $this->User_Device_model->exist($email, $id);
		if($user){
			$this->form_validation->set_message('is_unique', str_replace('%1', $this->lang->line("email"), $this->lang->line('validation_exist')));
			return false;
		}else
			return true;
	}

}