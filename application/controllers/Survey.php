<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/MainController.php';

class Survey extends MainController {
	
	private $class_name = 'survey';
	
	function __construct()
	{
		// Construct the parent class
		parent::__construct();
	}

	/**
	 * Listado
	 */
	public function index(){
		$this->data['class_name']= $this->class_name;

		$this->template->write_view('content', "$this->class_name/index", $this->data, TRUE);
		$this->template->render();
	}

	/**
	 * Nuevo
	 */
	public function create(){
		//REGLAS VALIDACION
		$this->load->library('form_validation');
		$this->form_validation->set_rules ('title', $this->lang->line("title"), 'trim|required|max_length[45]');
		$this->form_validation->set_rules ('business', $this->lang->line("business"), 'trim|required');
		$this->form_validation->set_message ('required', $this->lang->line('validation_required'));
		$this->form_validation->set_message ('max_length', $this->lang->line('validation_max_length'));

		//PARAMETROS
		$this->data['mode']= 'create';
		$this->data['url']= base_url("$this->class_name/create");
		$this->data['title']= $this->post('title');
		$this->data['business']= $this->post('business');
		$this->data['questions']= array();
		$this->data['class_name']= $this->class_name;
		
		//GUARDAR
		if(!empty($_POST)){
			//Preguntas
			$questions_id = $this->post('questions_id');
			$questions_type = $this->post('questions_type');
			$questions_title = $this->post('questions_title');
			$answer_op1 = $this->post('answer_op1');
			$answer_op2 = $this->post('answer_op2');
			$answer_op3 = $this->post('answer_op3');
			$answer_op4 = $this->post('answer_op4');
			if(!empty($questions_id) && is_array($questions_id)){
				foreach($questions_id AS $i => $question_id){
					$question = array(
						'id' => $question_id,
						'type' => isset($questions_type[$i]) ? $questions_type[$i] : Survey_Question_model::TYPE_INPUT,
						'title' => isset($questions_title[$i]) ? $questions_title[$i] : '',
						'opt_1' => isset($answer_op1[$i]) ? $answer_op1[$i] : '',
						'opt_2' => isset($answer_op2[$i]) ? $answer_op2[$i] : '',
						'opt_3' => isset($answer_op3[$i]) ? $answer_op3[$i] : '',
						'opt_4' => isset($answer_op4[$i]) ? $answer_op4[$i] : '',
					);
			
					$this->data['questions'][]= $question;
				}
			}
			
			if($this->form_validation->run () == TRUE){
				//Insertar
				$id = $this->Survey_model->insert(array(
					'title' => $this->data['title'],
					'business' => $this->data['business'],
					'created_at' => date('Y-m-d'),
				));
	
				if(!empty($id)){
					//Preguntas
					foreach ($this->data['questions'] AS $question){
						unset($question['id']);
						$question['survey_id']= $id;
						$this->Survey_Question_model->insert($question);
					}
					
					$this->session->set_flashdata('success', $this->lang->line('survey_saved'));
					redirect("/$this->class_name");
				}
			}
		}
		
		$this->template->write_view('content', "$this->class_name/form", $this->data, TRUE);
		$this->template->render();
	}

	/**
	 * Vista para actualizar una clase de vehiculo
	 */
	public function update(){
		$survey = $this->Survey_model->getById($this->get('id'));
		if(empty($survey)){
			redirect("/$this->class_name");
		}
		
		//REGLAS VALIDACION
		$this->load->library('form_validation');
		$this->form_validation->set_rules ('title', $this->lang->line("title"), 'trim|required|max_length[45]');
		$this->form_validation->set_rules ('business', $this->lang->line("business"), 'trim|required');
		$this->form_validation->set_message ('required', $this->lang->line('validation_required'));
		$this->form_validation->set_message ('max_length', $this->lang->line('validation_max_length'));
		
		//PARAMETROS
		$this->data['mode']= 'update';
		$this->data['url']= base_url("$this->class_name/update?id=$survey->id");
		$this->data['title']= $this->post('title');
		$this->data['business']= $this->post('business');
		$this->data['questions']= array();
		$this->data['class_name']= $this->class_name;
		
		//GUARDAR
		if(!empty($_POST)){
			//Preguntas
			$questions_id = $this->post('questions_id');
			$questions_type = $this->post('questions_type');
			$questions_title = $this->post('questions_title');
			$answer_op1 = $this->post('answer_op1');
			$answer_op2 = $this->post('answer_op2');
			$answer_op3 = $this->post('answer_op3');
			$answer_op4 = $this->post('answer_op4');
			if(!empty($questions_id) && is_array($questions_id)){
				foreach($questions_id AS $i => $question_id){
					$question = array(
						'id' => $question_id,
						'type' => isset($questions_type[$i]) ? $questions_type[$i] : Survey_Question_model::TYPE_INPUT,
						'title' => isset($questions_title[$i]) ? $questions_title[$i] : '',
						'opt_1' => isset($answer_op1[$i]) ? $answer_op1[$i] : '',
						'opt_2' => isset($answer_op2[$i]) ? $answer_op2[$i] : '',
						'opt_3' => isset($answer_op3[$i]) ? $answer_op3[$i] : '',
						'opt_4' => isset($answer_op4[$i]) ? $answer_op4[$i] : '',
					);
			
					$this->data['questions'][]= $question;
				}
			}
			
			if($this->form_validation->run () == TRUE){
				//Actualizar
				$this->Survey_model->update($survey->id, array(
					'title' => $this->data['title'],
					'business' => $this->data['business'],
				));
				
				//Preguntas
				$questions_saved = array();
				foreach ($this->data['questions'] AS $question){
					$id_question = $question['id'];
					unset($question['id']);
					
					if(!empty($id_question)){
						$this->Survey_Question_model->update($id_question, $question);
					}else{
						$question['survey_id']= $survey->id;
						$id_question = $this->Survey_Question_model->insert($question);
					}
					$questions_saved[]= $id_question;
				}
				//Eliminar preguntas eliminadas
				$this->Survey_Question_model->deleteNotIn($survey->id, $questions_saved);
		
				$this->session->set_flashdata('success', $this->lang->line('survey_saved'));
				redirect("/$this->class_name");
			}
		}else{
			//CARGAR DE BD
			if(!empty($survey)){
				$this->data['title']= $survey->title;
				$this->data['business']= $survey->business;
				$this->data['questions']= $this->Survey_Question_model->getAllById($survey->id);
			}
		}
		
		$this->template->write_view('content', "$this->class_name/form", $this->data, TRUE);
		$this->template->render();
	}

	/**
	 * Eliminar
	 */
	public function delete(){
		$this->Survey_model->delete($this->get('id'));
		$this->session->set_flashdata('success', $this->lang->line('survey_deleted'));
		redirect("/$this->class_name");
	}
	
	/**
	 * Pagina el listado
	 */
	public function pagination(){
		//PARAMETROS
		$query_filter = $this->input->post('search');
		$query_filter = strtolower($query_filter['value']);
		$query_sort = $this->input->post('order');
		$query_sort_direction = strtolower($query_sort[0]['dir']);
		$query_sort = strtolower($query_sort[0]['column']);
		$query_offset = $this->input->post('start');
		$query_limit = $this->input->post('length');
		$query_draw = $this->input->post('draw');
		
		//Resultado TOTAL
		$this->db->select('COUNT(*) AS total');
		$query = $this->db->get(Survey_model::TABLE)->row();
		$result_total_filtered = $result_total =  $query->total;
		
		//FILTROS y SELECT
		if(!empty($query_filter)){
			$this->db->where("(t.title like '%$query_filter%' OR IF(t.business='H', 'Hertz', 'FireFly') LIKE '%$query_filter%')");
		}
		$select = array('t.created_at', 't.title', 't.business', 't.id');
		$this->db->select("t.created_at, t.title, IF(t.business='H', 'Hertz', 'FireFly') AS business, t.id");

		//Resultado PAGINADO
		$this->db->from(Survey_model::TABLE.' AS t');
		if(isset($select[$query_sort])){
			$this->db->order_by($select[$query_sort], $query_sort_direction);
		}else{
			$this->db->order_by('title', 'ASC');
		}
		$this->db->limit($query_limit, $query_offset);
		
		$query = $this->db->get();
		$result_data = array();
		foreach($query->result() AS $row){
			//Botones de Accion
			$toolbar = '<div class="btn-group">';
			$toolbar.= '<a href="'.base_url("$this->class_name/update").'?id='.$row->id.'" class="btn btn-blue btn-sm"><i class="fa fa-edit fa-lg"></i> '.$this->lang->line('edit').'</a>';
			$toolbar.= '<a class="btn btn-danger btn-sm btn-confirm-delete" data-toggle="modal" data-target="#deleteModal" data-action="'.base_url("$this->class_name/delete")."?id=".$row->id.'" data-backdrop="true" title="'.$this->lang->line("delete").'"><i class="fa fa-trash fa-lg"></i> '.$this->lang->line('delete').'</a>';
			$toolbar.= '</div>';
			
			//Fila
			$result_data[]=array(
				$row->created_at,
				$row->title,
				$row->business,
				$toolbar,
			);
		}
		$result_total_filtered = !empty($query_filter) ? count($result_data) : $result_total;
	
		//Respuesta
		echo json_encode(array(
			'draw' => intval($query_draw),
			'recordsTotal' => intval($result_total),
			'recordsFiltered' => intval($result_total_filtered),
			'data' => $result_data,
		));
	}

}