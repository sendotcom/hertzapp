<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/MainController.php';

class Discount extends MainController {
	
	function __construct()
	{
		// Construct the parent class
		parent::__construct();
	}

	/**
	 * Vista para listar descuentos
	 */
	public function index(){
		$data = array();

		$this->template->write_view('content', 'discount/index', $data, TRUE);
		$this->template->render();
	}

	/**
	 * Vista para crear un descuento
	 */
	public function create(){
		//REGLAS VALIDACION
		$this->load->library('form_validation');
		$this->form_validation->set_rules ('cdp', $this->lang->line("cdp"), 'trim|required|max_length[255]');
		$this->form_validation->set_rules ('description', $this->lang->line("description"), 'trim|required|max_length[255]');
		$this->form_validation->set_rules ('validity', $this->lang->line("validity"), 'trim|required|max_length[255]');
		$this->form_validation->set_message ('required', $this->lang->line('validation_required'));
		$this->form_validation->set_message ('max_length', $this->lang->line('validation_max_length'));

		//PARAMETROS
		$data = array(
			'mode' => 'create',
			'url' => base_url('discount/create'),
			'cdp' => $this->post('cdp'),
			'description' => $this->post('description'),
			'validity' => $this->post('validity'),
		);

		//GUARDAR
		if($this->form_validation->run () == TRUE){
			//Insertar
			$id = $this->Discount_model->insert(array(
				'cdp' => $this->post('cdp'),
				'description' => $this->post('description'),
				'validity' => $this->post('validity'),
			));

			if(!empty($id)){
				$this->session->set_flashdata('success', $this->lang->line('discount_saved'));
				redirect(base_url('discount'));
			}
		}
		
		$this->template->write_view('content', 'discount/form', $data, TRUE);
		$this->template->render();
	}

	/**
	 * Vista para actualizar un descuento
	 */
	public function update(){
		$id = $this->input->get('id');

		//REGLAS VALIDACION
		$this->load->library('form_validation');
		$this->form_validation->set_rules ('cdp', $this->lang->line("cdp"), 'trim|required|max_length[255]');
		$this->form_validation->set_rules ('description', $this->lang->line("description"), 'trim|required|max_length[255]');
		$this->form_validation->set_rules ('validity', $this->lang->line("validity"), 'trim|required|max_length[255]');
		$this->form_validation->set_message ('required', $this->lang->line('validation_required'));
		$this->form_validation->set_message ('max_length', $this->lang->line('validation_max_length'));

		//PARAMETROS
		$data = array(
			'mode' => 'update',
			'url' => base_url('discount/update?id='.$id),
			'cdp' => $this->post('cdp'),
			'description' => $this->post('description'),
			'validity' => $this->post('validity'),
		);

		//GUARDAR
		if($this->form_validation->run () == TRUE){
			$attr = array(
				'cdp' => $this->post('cdp'),
				'description' => $this->post('description'),
				'validity' => $this->post('validity'),
			);

			//Actualizar
			$this->Discount_model->update($id, $attr);

			$this->session->set_flashdata('success', $this->lang->line('discount_saved'));
			redirect(base_url('discount'));
		}else{
			//CARGAR DE BD
			$discount = $this->Discount_model->getById($id);
			if(!empty($discount)){
				$data['cdp']=$discount->cdp;
				$data['description']=$discount->description;
				$data['validity']=$discount->validity;
			}
		}
		
		$this->template->write_view('content', 'discount/form', $data, TRUE);
		$this->template->render();
	}

	/**
	 * Borrar usuario
	 */
	public function delete(){
		$id = $this->post('id');
		$this->Discount_model->delete($id);
		
		$this->session->set_flashdata('success', $this->lang->line('discount_deleted'));
		redirect('discount');
	}
	
	/**
	 * Importar
	 */
	public function import(){
		//PARAMETROS
		$data = array(
			'url' => base_url('discount/import'),
		);
		
		if(!empty($_FILES['file'])){
			$file = $_FILES['file']['name'];
			if(!empty($file) && $this->security->xss_clean($file, TRUE)){
				
				$imported = 0;
				
				//Apertura de fichero
				if (($handle = fopen($_FILES['file']['tmp_name'], "r")) !== FALSE) {
					$i = 0;
					while (($row = fgetcsv($handle, 1000, ";")) !== FALSE) {
						if($i>=1 && $i<=1000){
							$date = DateTime::createFromFormat('d/m/Y', trim(utf8_encode($row[2])));
							
							$attr = array(
								'cdp' => trim(utf8_encode($row[0])),
								'description' => trim(utf8_encode($row[1])),
								'validity' => $date->format('Y-m-d'),
								'deleted' => intval(trim(utf8_encode($row[3]))),
							);
							
							$item = $this->Discount_model->getByCDP($attr['cdp'], false);
							if(!empty($item)){
								$this->Discount_model->update($item->id, $attr);
								$imported++;
							}else{
								$id = $this->Discount_model->insert($attr);
								if(!empty($id)){
									$imported++;
								}
							}
						}
						$i++;
					}
					
					$this->session->set_flashdata('success', sprintf($this->lang->line("discounts_imported"), $imported, $i-1));
					redirect("discount/import");
				}
			}
		}	
		
		$this->template->write_view('content', 'discount/import', $data, TRUE);
		$this->template->render();
	}
	
	/**
	 * Formato para importar
	 */
	public function import_format(){
		$public_dir = "/uploads/tmp/";
		$real_path = FCPATH.$public_dir;
		$this->checkDir($real_path);
		$file_name = 'importador_cdp_'.time().'.csv';
	
		$fp = fopen($real_path.$file_name, 'w');
	
		fputcsv($fp, [
			utf8_decode('CDP'),
			utf8_decode('Descripción'),
			utf8_decode('Validez'),
			utf8_decode('Eliminar'),
		], ';');
	
		fclose($fp);
		redirect(base_url($public_dir.$file_name));
	}
	
	/**
	 * Pagina el listado de Descuentos
	 */
	public function pagination(){
		//PARAMETROS
		$query_filter = $this->input->post('search');
		$query_filter = strtolower($query_filter['value']);
		$query_sort = $this->input->post('order');
		$query_sort_direction = strtolower($query_sort[0]['dir']);
		$query_sort = strtolower($query_sort[0]['column']);
		$query_offset = $this->input->post('start');
		$query_limit = $this->input->post('length');
		$query_draw = $this->input->post('draw');
		
		//Resultado TOTAL
		$this->db->select('COUNT(*) AS total');
		$query = $this->db
			->where('deleted', 0)
			->get(Discount_model::TABLE)
			->row();
		$result_total_filtered = $result_total =  $query->total;
		
		//FILTROS y SELECT
		if(!empty($query_filter)){
			$this->db->where("(t.id='$query_filter' OR t.cdp like '%$query_filter%' OR t.description like '%$query_filter%' OR t.validity like '%$query_filter%')");
		}
		$select = array('t.id', 't.cdp', 't.description', 't.validity');
		$this->db->select(implode(',', $select));

		//Resultado PAGINADO
		$this->db->from(Discount_model::TABLE.' AS t');
		$this->db->where('t.deleted', 0);

		if(isset($select[$query_sort])){
			$this->db->order_by($select[$query_sort], $query_sort_direction);
		}else{
			$this->db->order_by('validity', 'ASC');
		}
		$this->db->limit($query_limit, $query_offset);
		
		$query = $this->db->get();
		$result_data = array();
		foreach($query->result() AS $row){
			//Botones de Accion
			$toolbar = '<div class="btn-group">';
			$toolbar.= '<a href="'.base_url('discount/update').'?id='.$row->id.'" class="btn btn-blue btn-sm"><i class="fa fa-edit fa-lg"></i> '.$this->lang->line('edit').'</a>';
			$toolbar.= '<a class="btn btn-danger btn-sm" data-toggle="modal" data-target="#deleteModal" data-id="'.$row->id.'" data-backdrop="true"><i class="fa fa-trash fa-lg"></i> '.$this->lang->line('delete').'</a>';
			$toolbar.= '</div>';
			
			//Fila
			$result_data[]=array(
				$row->cdp,
				$row->description,
				$row->validity,
				$toolbar,
			);
		}
		$result_total_filtered = !empty($query_filter) ? count($result_data) : $result_total;
	
		//Respuesta
		$response = array(
			'draw' => intval($query_draw),
			'recordsTotal' => intval($result_total),
			'recordsFiltered' => intval($result_total_filtered),
			'data' => $result_data,
		);
		echo json_encode($response);
	}
	
}