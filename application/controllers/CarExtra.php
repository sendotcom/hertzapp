<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/MainController.php';

class CarExtra extends MainController {
	
	private $class_name = 'carExtra';
	
	function __construct()
	{
		// Construct the parent class
		parent::__construct();
	}

	/**
	 * Vista para listar clases de vehiculo
	 */
	public function index(){
		$this->data['class_name']= $this->class_name;

		$this->template->write_view('content', "$this->class_name/index", $this->data, TRUE);
		$this->template->render();
	}

	/**
	 * Vista para crear una clase de vehiculo
	 */
	public function create(){
		//REGLAS VALIDACION
		$this->load->library('form_validation');
		$this->form_validation->set_rules ('code', $this->lang->line("code"), 'trim|required|max_length[10]|is_unique['.Car_Extra_model::TABLE.'.code]');
		$this->form_validation->set_rules ('name', $this->lang->line("name"), 'trim|required|max_length[128]');
		$this->form_validation->set_rules ('name_short', $this->lang->line("name_short"), 'trim|required|max_length[10]');
		$this->form_validation->set_rules ('description', $this->lang->line("description"), 'trim|max_length[512]');
		$this->form_validation->set_rules ('price', $this->lang->line("price"), 'trim|required|max_length[15]');
		$this->form_validation->set_rules ('type', $this->lang->line("type"), 'trim|required');
		$this->form_validation->set_message ('required', $this->lang->line('validation_required'));
		$this->form_validation->set_message ('max_length', $this->lang->line('validation_max_length'));

		//PARAMETROS
		$this->data['mode']= 'create';
		$this->data['url']= base_url("$this->class_name/create");
		$this->data['code']= $this->post('code');
		$this->data['name']= $this->post('name');
		$this->data['name_short']= $this->post('name_short');
		$this->data['description']= $this->post('description');
		$this->data['price']= $this->post('price');
		$this->data['type']= $this->post('type');
		$this->data['class_name']= $this->class_name;

		//GUARDAR
		if(!empty($_POST)){
			if($this->form_validation->run () == TRUE){
				//Insertar
				$id = $this->Car_Extra_model->insert(array(
					'code' => $this->data['code'],
					'name' => $this->data['name'],
					'name_short' => $this->data['name_short'],
					'description' => $this->data['description'],
					'price' => $this->data['price'],
					'type' => $this->data['type'],
				));
	
				if(!empty($id)){
					//Imagen
					if (!empty($_FILES)) {
						//Ruta Destino
						$public_dir = "/uploads/extras/".md5($id)."/";
						$real_path = FCPATH.$public_dir;
						$this->checkDir($real_path);
							
						$file = $_FILES['file']['name'];
						if(!empty($file) && $this->security->xss_clean($file, TRUE)){
							//Mover imagen
							$file_name = md5(date('YmdHis').$this->generateRandomString(3)).'.'.pathinfo($file, PATHINFO_EXTENSION);
							$r = move_uploaded_file($_FILES['file']['tmp_name'], $real_path.$file_name);
							if($r==1){
								$this->Car_Extra_model->update($id, array('image' => $public_dir.$file_name));
							}
						}
					}
					
					$this->session->set_flashdata('success', $this->lang->line('car_extra_saved'));
					redirect("/$this->class_name");
				}
			}
		}else{
			$this->data['type']= "product";
			$this->data['price']= number_format(0, 2);
		}
		
		$this->template->write_view('content', "$this->class_name/form", $this->data, TRUE);
		$this->template->render();
	}

	/**
	 * Vista para actualizar una clase de vehiculo
	 */
	public function update(){
		$extra = $this->Car_Extra_model->getById($this->get('id'));
		if(empty($extra)){
			redirect("/$this->class_name");
		}
		
		//REGLAS VALIDACION
		$this->load->library('form_validation');
		$this->form_validation->set_rules ('code', $this->lang->line("code"), 'trim|required|max_length[10]|is_unique['.Car_Extra_model::TABLE.'.code]');
		$this->form_validation->set_rules ('name', $this->lang->line("name"), 'trim|required|max_length[128]');
		$this->form_validation->set_rules ('name_short', $this->lang->line("name_short"), 'trim|required|max_length[10]');
		$this->form_validation->set_rules ('description', $this->lang->line("description"), 'trim|max_length[512]');
		$this->form_validation->set_rules ('price', $this->lang->line("price"), 'trim|required|max_length[15]');
		$this->form_validation->set_rules ('type', $this->lang->line("type"), 'trim|required');
		$this->form_validation->set_message ('required', $this->lang->line('validation_required'));
		$this->form_validation->set_message ('max_length', $this->lang->line('validation_max_length'));
		
		//PARAMETROS
		$this->data['mode']= 'update';
		$this->data['url']= base_url("$this->class_name/update=id=$extra->id");
		$this->data['code']= $this->post('code');
		$this->data['name']= $this->post('name');
		$this->data['name_short']= $this->post('name_short');
		$this->data['description']= $this->post('description');
		$this->data['price']= $this->post('price');
		$this->data['type']= $this->post('type');
		$this->data['class_name']= $this->class_name;
		
		//GUARDAR
		if(!empty($_POST)){
			if($this->form_validation->run () == TRUE){
				$attr = array(
					'code' => $this->data['code'],
					'name' => $this->data['name'],
					'name_short' => $this->data['name_short'],
					'description' => $this->data['description'],
					'price' => $this->data['price'],
					'type' => $this->data['type'],
				);
				
				//Imagen
				if (!empty($_FILES)) {
					//Ruta Destino
					$public_dir = "/uploads/extras/".md5($extra->id)."/";
					$real_path = FCPATH.$public_dir;
					$this->checkDir($real_path);
				
					$file = $_FILES['file']['name'];
					if(!empty($file) && $this->security->xss_clean($file, TRUE)){
						//Mover imagen
						$file_name = md5(date('YmdHis').$this->generateRandomString(3)).'.'.pathinfo($file, PATHINFO_EXTENSION);
						$r = move_uploaded_file($_FILES['file']['tmp_name'], $real_path.$file_name);
						if($r==1){
							$attr['image']= $public_dir.$file_name;
						}
					}
				}
				
				//Insertar
				$this->Car_Extra_model->update($extra->id, $attr);
		
				$this->session->set_flashdata('success', $this->lang->line('car_extra_saved'));
				redirect("/$this->class_name");
			}
		}else{
			//CARGAR DE BD
			if(!empty($extra)){
				$this->data['code']= $extra->code;
				$this->data['name']= $extra->name;
				$this->data['name_short']= $extra->name_short;
				$this->data['description']= $extra->description;
				$this->data['price']= $extra->price;
				$this->data['type']= $extra->type;
				$this->data['image']= $extra->image;
			}
		}
		
		$this->template->write_view('content', "$this->class_name/form", $this->data, TRUE);
		$this->template->render();
	}

	/**
	 * Borrar usuario
	 */
	public function delete(){
		if($this->Car_Extra_model->delete($this->get('id')))
			$this->session->set_flashdata('success', $this->lang->line('car_extra_deleted'));
		else
			$this->session->set_flashdata('error', $this->lang->line('car_extra_no_deleted'));
		redirect("/$this->class_name");
	}
	
	/**
	 * Importar
	 */
	public function import(){
		//PARAMETROS
		$data = array(
			'url' => base_url("$this->class_name/import"),
			'class_name' => $this->class_name,
		);
	
		if(!empty($_FILES['file'])){
			$file = $_FILES['file']['name'];
			if(!empty($file) && $this->security->xss_clean($file, TRUE)){
				$imported = 0;
	
				//Apertura de fichero
				if (($handle = fopen($_FILES['file']['tmp_name'], "r")) !== FALSE) {
					$i = 0;
					while (($row = fgetcsv($handle, 1000, ";")) !== FALSE) {
						if($i>=1 && $i<=1000){
							$code = trim(utf8_encode($row[0]));
							$price = str_replace(',','.',trim(utf8_encode($row[4])));
							
							$attr = array(
								'code' => $code,
								'name' => trim(utf8_encode($row[1])),
								'name_short' => trim(utf8_encode($row[2])),
								'description' => trim(utf8_encode($row[3])),
								'price' => floatval($price),
								'type' => trim(utf8_encode($row[5])),
								'deleted' => intval(trim(utf8_encode($row[7]))),
							);
	
							$item = $this->Car_Extra_model->getByCode($code);
							$id = null;
							if(!empty($item)){
								$this->Car_Extra_model->update($item->id, $attr);
								$id = $item->id;
							}else{
								$id = $this->Car_Extra_model->insert($attr);
							}
								
							if(!empty($id)){
								$imported++;
								$image = trim(utf8_encode($row[6]));
								if(!empty($image)){
									$image = file_get_contents($image);
									if(!empty($image)){
										//Ruta Destino
										$public_dir = "/uploads/extras/".md5($id)."/";
										$real_path = FCPATH.$public_dir;
										$this->checkDir($real_path);
	
										$filename = md5(date('YmdHis').$this->generateRandomString(3)).'.png';
										file_put_contents($real_path.$filename, $image, FILE_APPEND);
										$this->Car_Extra_model->update($id, array(
											'image' => $public_dir.$filename
										));
									}
								}
							}
						}
						$i++;
					}
					
					$this->session->set_flashdata('success', sprintf($this->lang->line("car_extras_imported"), $imported, $i-1));
					redirect("$this->class_name/import");
				}
			}
		}
	
		$this->template->write_view('content', "$this->class_name/import", $data, TRUE);
		$this->template->render();
	}
	
	/**
	 * Formato para importar
	 */
	public function import_format(){
		$public_dir = "/uploads/tmp/";
		$real_path = FCPATH.$public_dir;
		$this->checkDir($real_path);
		$file_name = 'importador_extras_'.time().'.csv';
	
		$fp = fopen($real_path.$file_name, 'w');
	
		fputcsv($fp, [
			utf8_decode('CÓDIGO'),
			utf8_decode('NOMBRE'),
			utf8_decode('NOMBRE CORTO'),
			utf8_decode('DESCRIPCIÓN'),
			utf8_decode('PRECIO'),
			utf8_decode('TIPO'),
			utf8_decode('IMAGEN'),
			utf8_decode('ELIMINAR'),
		], ';');
	
		fclose($fp);
		redirect(base_url($public_dir.$file_name));
	}
	
	/**
	 * Pagina el listado de Descuentos
	 */
	public function pagination(){
		//PARAMETROS
		$query_filter = $this->input->post('search');
		$query_filter = strtolower($query_filter['value']);
		$query_sort = $this->input->post('order');
		$query_sort_direction = strtolower($query_sort[0]['dir']);
		$query_sort = strtolower($query_sort[0]['column']);
		$query_offset = $this->input->post('start');
		$query_limit = $this->input->post('length');
		$query_draw = $this->input->post('draw');
		
		//Resultado TOTAL
		$this->db->select('COUNT(*) AS total');
		$query = $this->db
			->where('deleted', 0)
			->get(Car_Extra_model::TABLE)
			->row();
		$result_total_filtered = $result_total =  $query->total;
		
		//FILTROS y SELECT
		if(!empty($query_filter)){
			$this->db->where("(t.code='$query_filter' OR t.name like '%$query_filter%')");
		}
		$select = array('t.code', 't.name', 't.type','t.id');
		$this->db->select(implode(',', $select));

		//Resultado PAGINADO
		$this->db->from(Car_Extra_model::TABLE.' AS t');
		$this->db->where('deleted', 0);

		if(isset($select[$query_sort])){
			$this->db->order_by($select[$query_sort], $query_sort_direction);
		}else{
			$this->db->order_by('name', 'ASC');
		}
		$this->db->limit($query_limit, $query_offset);
		
		$query = $this->db->get();
		$result_data = array();
		foreach($query->result() AS $row){
			//Botones de Accion
			$toolbar = '<div class="btn-group">';
			$toolbar.= '<a href="'.base_url("$this->class_name/update").'?id='.$row->id.'" class="btn btn-blue btn-sm"><i class="fa fa-edit fa-lg"></i> '.$this->lang->line('edit').'</a>';
			$toolbar.= '<a class="btn btn-danger btn-sm btn-confirm-delete" data-toggle="modal" data-target="#deleteModal" data-action="'.base_url("$this->class_name/delete")."?id=".$row->id.'" data-backdrop="true" title="'.$this->lang->line("delete").'"><i class="fa fa-trash fa-lg"></i> '.$this->lang->line('delete').'</a>';
			$toolbar.= '</div>';
			
			//Fila
			$result_data[]=array(
				$row->code,
				$row->name,
				$this->lang->line($row->type),
				$toolbar,
			);
		}
		$result_total_filtered = !empty($query_filter) ? count($result_data) : $result_total;
	
		//Respuesta
		echo json_encode(array(
			'draw' => intval($query_draw),
			'recordsTotal' => intval($result_total),
			'recordsFiltered' => intval($result_total_filtered),
			'data' => $result_data,
		));
	}

}