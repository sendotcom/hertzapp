<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/MainController.php';

class Script extends MainController {
	
	public function index()
	{
		//Traducciones
		function _translate($x) {
			$CI=&get_instance();
			return $CI->lang->line($x[1]); 
		}
		
		//Rutas
		function _path($x) {
			return base_url($x[1]);
		}
		
		$js = file_get_contents(base_url('js/'.$_GET['file']));
		$js = preg_replace_callback('~_t\("(.+?)"\)~', '_translate', $js);
		$js = preg_replace_callback('~_p\("(.+?)"\)~', '_path', $js);
		echo $js;
	}
	
}
