<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/MainController.php';

class CarClass extends MainController {
	
	private $class_name = 'carClass';
	
	function __construct()
	{
		// Construct the parent class
		parent::__construct();
	}

	/**
	 * Vista para listar clases de vehiculo
	 */
	public function index(){
		$this->data['class_name']= $this->class_name;

		$this->template->write_view('content', "$this->class_name/index", $this->data, TRUE);
		$this->template->render();
	}

	/**
	 * Vista para crear una clase de vehiculo
	 */
	public function create(){
		//REGLAS VALIDACION
		$this->load->library('form_validation');
		$this->form_validation->set_rules ('id', $this->lang->line("code"), 'trim|required|max_length[10]|is_unique['.Car_Class_model::TABLE.'.id]');
		$this->form_validation->set_rules ('name', $this->lang->line("name"), 'trim|required|max_length[45]');
		$this->form_validation->set_message ('required', $this->lang->line('validation_required'));
		$this->form_validation->set_message ('max_length', $this->lang->line('validation_max_length'));

		//PARAMETROS
		$this->data['mode']= 'create';
		$this->data['url']= base_url("$this->class_name/create");
		$this->data['id']= $this->post('id');
		$this->data['name']= $this->post('name');
		$this->data['class_name']= $this->class_name;

		//GUARDAR
		if(!empty($_POST)){
			if($this->form_validation->run () == TRUE){
				//Insertar
				$id = $this->Car_Class_model->insert(array(
					'id' => $this->data['id'],
					'name' => $this->data['name'],
				));
	
				if(!empty($id)){
					$this->session->set_flashdata('success', $this->lang->line('car_class_saved'));
					redirect("/$this->class_name");
				}
			}
		}
		
		$this->template->write_view('content', "$this->class_name/form", $this->data, TRUE);
		$this->template->render();
	}

	/**
	 * Vista para actualizar una clase de vehiculo
	 */
	public function update(){
		$class = $this->Car_Class_model->getById($this->get('id'));
		if(empty($class)){
			redirect("/$this->class_name");
		}
		
		//REGLAS VALIDACION
		$this->load->library('form_validation');
		$this->form_validation->set_rules ('name', $this->lang->line("name"), 'trim|required|max_length[45]');
		$this->form_validation->set_message ('required', $this->lang->line('validation_required'));
		$this->form_validation->set_message ('max_length', $this->lang->line('validation_max_length'));
		
		//PARAMETROS
		$this->data['mode']= 'update';
		$this->data['url']= base_url("$this->class_name/update=id=$class->id");
		$this->data['id']= $this->post('id');
		$this->data['name']= $this->post('name');
		$this->data['class_name']= $this->class_name;
		
		//GUARDAR
		if(!empty($_POST)){
			if($this->form_validation->run () == TRUE){
				//Insertar
				$this->Car_Class_model->update($class->id, array(
					'name' => $this->data['name'],
				));
		
				$this->session->set_flashdata('success', $this->lang->line('car_class_saved'));
				redirect("/$this->class_name");
			}
		}else{
			//CARGAR DE BD
			if(!empty($class)){
				$this->data['id']= $class->id;
				$this->data['name']= $class->name;
			}
		}
		
		$this->template->write_view('content', "$this->class_name/form", $this->data, TRUE);
		$this->template->render();
	}

	/**
	 * Borrar usuario
	 */
	public function delete(){
		if($this->Car_Class_model->delete($this->get('id')))
			$this->session->set_flashdata('success', $this->lang->line('car_class_deleted'));
		else
			$this->session->set_flashdata('error', $this->lang->line('car_class_no_deleted'));
		redirect("/$this->class_name");
	}
	
	/**
	 * Pagina el listado de Descuentos
	 */
	public function pagination(){
		//PARAMETROS
		$query_filter = $this->input->post('search');
		$query_filter = strtolower($query_filter['value']);
		$query_sort = $this->input->post('order');
		$query_sort_direction = strtolower($query_sort[0]['dir']);
		$query_sort = strtolower($query_sort[0]['column']);
		$query_offset = $this->input->post('start');
		$query_limit = $this->input->post('length');
		$query_draw = $this->input->post('draw');
		
		//Resultado TOTAL
		$this->db->select('COUNT(*) AS total');
		$query = $this->db->get(Car_Class_model::TABLE)->row();
		$result_total_filtered = $result_total =  $query->total;
		
		//FILTROS y SELECT
		if(!empty($query_filter)){
			$this->db->where("(t.id='$query_filter' OR t.name like '%$query_filter%')");
		}
		$select = array('t.id', 't.name');
		$this->db->select(implode(',', $select));

		//Resultado PAGINADO
		$this->db->from(Car_Class_model::TABLE.' AS t');

		if(isset($select[$query_sort])){
			$this->db->order_by($select[$query_sort], $query_sort_direction);
		}else{
			$this->db->order_by('id', 'ASC');
		}
		$this->db->limit($query_limit, $query_offset);
		
		$query = $this->db->get();
		$result_data = array();
		foreach($query->result() AS $row){
			//Botones de Accion
			$toolbar = '<div class="btn-group">';
			$toolbar.= '<a href="'.base_url("$this->class_name/update").'?id='.$row->id.'" class="btn btn-blue btn-sm"><i class="fa fa-edit fa-lg"></i> '.$this->lang->line('edit').'</a>';
			$toolbar.= '<a class="btn btn-danger btn-sm btn-confirm-delete" data-toggle="modal" data-target="#deleteModal" data-action="'.base_url("$this->class_name/delete")."?id=".$row->id.'" data-backdrop="true" title="'.$this->lang->line("delete").'"><i class="fa fa-trash fa-lg"></i> '.$this->lang->line('delete').'</a>';
			$toolbar.= '</div>';
			
			//Fila
			$result_data[]=array(
				$row->id,
				$row->name,
				$toolbar,
			);
		}
		$result_total_filtered = !empty($query_filter) ? count($result_data) : $result_total;
	
		//Respuesta
		echo json_encode(array(
			'draw' => intval($query_draw),
			'recordsTotal' => intval($result_total),
			'recordsFiltered' => intval($result_total_filtered),
			'data' => $result_data,
		));
	}

}