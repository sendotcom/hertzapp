<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/MainController.php';
require APPPATH . '/libraries/SnsUtils.php';
class Notification extends MainController {
	
	private $sns;

	function __construct()
	{
		// Construct the parent class
		parent::__construct();
		
		//SNS
		$this->load->library('SnsUtils');
		$this->sns = new SnsUtils();
	}

	/**
	 * Vista para listar notificaciones
	 */
	public function index(){
		$data = array();

		$this->template->write_view('content', 'notification/index', $data, TRUE);
		$this->template->render();
	}

	/**
	 * Vista para crear una notificacion
	 */
	public function create(){
		//REGLAS VALIDACION
		$this->load->library('form_validation');
		$this->form_validation->set_rules ('title', $this->lang->line("title"), 'trim|required|max_length[150]');
		$this->form_validation->set_rules ('description', $this->lang->line("description"), 'trim|required|max_length[10000]');
		$this->form_validation->set_rules ('category_id', $this->lang->line("category"), 'trim|required');
		$this->form_validation->set_rules ('cdp', $this->lang->line("cdp"), 'trim|required|max_length[45]');
		$this->form_validation->set_message ('required', $this->lang->line('validation_required'));
		$this->form_validation->set_message ('max_length', $this->lang->line('validation_max_length'));

		//PARAMETROS
		$data = array(
			'mode' => 'create',
			'url' => base_url('notification/create'),
			'title' => $this->post('title'),
			'description' => $this->post('description'),
			'category_id' => $this->post('category_id'),
			'cdp' => $this->post('cdp'),
			'version_code' => strtotime(date('Y-m-d H:i:s')),
			'creation_date' => date('Y-m-d H:i:s'),
			'image' => $this->post('image'),
		);

		//GUARDAR
		if($this->form_validation->run () == TRUE){
			//Insertar
			$notification_id = $this->Notification_model->insert(array(
				'title' => $data['title'],
				'description' => $data['description'],
				'creation_date' => $data['creation_date'],
				'version_code' => $data['version_code'],
				'category_id' => $data['category_id'],
				'cdp' => $data['cdp'],
				'user_id' => $this->session->userdata('logged_in')->id,
			));

			if(!empty($notification_id)){
				
				//Imagen
				$image = '';
				if (!empty($_FILES)) {
					//Ruta Destino
					$public_dir = "/uploads/push/".md5($notification_id)."/";
					$real_path = FCPATH.$public_dir;
					$this->checkDir($real_path);
				
					$file = $_FILES['file']['name'];
					if(!empty($file) && $this->security->xss_clean($file, TRUE)){
						//Mover imagen
						$file_name = md5(date('YmdHis').$this->generateRandomString(3)).'.'.pathinfo($file, PATHINFO_EXTENSION);
						$r = move_uploaded_file($_FILES['file']['tmp_name'], $real_path.$file_name);
						if($r==1){
							$image = $public_dir.$file_name;
							$this->Notification_model->insertImage(array(
								'notification_id' => $notification_id,
								'path' => $image,
							));
						}
					}
				}
				
				//ENVIAR NOTIFICACION
				$this->sns->sendPush($data['title'], $data['description'], 0, null, $notification_id, $image);

				$this->session->set_flashdata('success', $this->lang->line('notification_saved'));
				redirect(base_url('notification'));
			}
		}

		//Categorias
		$data['categories'] = $this->Category_model->getCategories(false);

		$this->template->write_view('content', 'notification/form', $data, TRUE);
		$this->template->render();
	}

	/**
	 * Vista para actualizar una notificacion
	 */
	public function update(){
		$id = $this->input->get('id');
		
		//REGLAS VALIDACION
		$this->load->library('form_validation');
		$this->form_validation->set_rules ('title', $this->lang->line("title"), 'trim|required|max_length[150]');
		$this->form_validation->set_rules ('description', $this->lang->line("description"), 'trim|required|max_length[10000]');
		$this->form_validation->set_rules ('category_id', $this->lang->line("category"), 'trim|required');
		$this->form_validation->set_rules ('cdp', $this->lang->line("cdp"), 'trim|required|max_length[45]');
		$this->form_validation->set_message ('required', $this->lang->line('validation_required'));
		$this->form_validation->set_message ('max_length', $this->lang->line('validation_max_length'));
		
		//PARAMETROS
		$data = array(
			'mode' => 'update',
			'url' => base_url('notification/update').'?id='.$id,
			'title' => $this->post('title'),
			'description' => $this->post('description'),
			'category_id' => $this->post('category_id'),
			'cdp' => $this->post('cdp'),
			'version_code' => strtotime(date('Y-m-d H:i:s')),
			'creation_date' => date('Y-m-d H:i:s'),
			'image' => $this->post('image'),
		);

		//PARAMETROS
		if(!empty($_POST)){
			//VALIDACION
			if($this->form_validation->run () == TRUE){
				
				//Actualizar
				$this->Notification_model->update($id, array(
					'title' => $data['title'],
					'description' => $data['description'],
					'creation_date' => $data['creation_date'],
					'version_code' => $data['version_code'],
					'category_id' => $data['category_id'],
					'cdp' => $data['cdp'],
				));
				
				//Imagen
				if (!empty($_FILES)) {
					//Ruta Destino
					$public_dir = "/uploads/push/".md5($id)."/";
					$real_path = FCPATH.$public_dir;
					$this->checkDir($real_path);
			
					$file = $_FILES['file']['name'];
					if(!empty($file) && $this->security->xss_clean($file, TRUE)){
						//Eliminar imagen anterior
						$this->Notification_model->deleteNotificationImage($id);
						
						//Mover imagen
						$file_name = md5(date('YmdHis').$this->generateRandomString(3)).'.'.pathinfo($file, PATHINFO_EXTENSION);
						$r = move_uploaded_file($_FILES['file']['tmp_name'], $real_path.$file_name);
						if($r==1){
							$this->Notification_model->insertImage(array(
								'notification_id' => $id,
								'path' => $public_dir.$file_name,
							));
						}
					}
				}
		
				$this->session->set_flashdata('success', $this->lang->line('notification_saved'));
				redirect(base_url('notification'));
			}
		}else{
			//CARGAR DE BD
			$push = $this->Notification_model->getNotification($id);
			if(!empty($push)){
				$data['title']=$push->title;
				$data['description']=$push->description;
				$data['category_id']=$push->category_id;
				$data['cdp']=$push->cdp;
				$images = $this->Notification_model->getNotificationImages($id);
				if(!empty($images)){
					$data['image']=$images[0]->path;
				}
			}
		}

		//Categorias
		$data['categories'] = $this->Category_model->getCategories(false);

		$this->template->write_view('content', 'notification/form', $data, TRUE);
		$this->template->render();
	}

	/**
	 * Borrar notificación
	 */
	public function delete(){
		$id = $this->post('id');
		$this->Notification_model->update($id, array(
			'deleted' => 1,
		));
		
		$this->session->set_flashdata('success', $this->lang->line('notification_deleted'));
		redirect('notification');
	}
	
	/**
	 * Pagina el listado de Notificaciones
	 */
	public function pagination(){
		//PARAMETROS
		$query_filter = $this->input->post('search');
		$query_filter = strtolower($query_filter['value']);
		$query_sort = $this->input->post('order');
		$query_sort_direction = strtolower($query_sort[0]['dir']);
		$query_sort = strtolower($query_sort[0]['column']);
		$query_offset = $this->input->post('start');
		$query_limit = $this->input->post('length');
		$query_draw = $this->input->post('draw');
		
		//Resultado TOTAL
		$this->db->select('COUNT(*) AS total');
		$this->db->where('deleted', 0);
		$query = $this->db->get(Notification_model::TABLE)->row();
		$result_total_filtered = $result_total =  $query->total;
		
		//FILTROS y SELECT
		if(!empty($query_filter)){
			$this->db->where("(t.id='$query_filter' OR t.title like '%$query_filter%' OR c.title like '%$query_filter%')");
		}
		$select = array('t.title', 't.creation_date', 't.category_id', 't.id');
		$this->db->select(implode(',', $select).', c.title AS category_name');
		$this->db->where('deleted', 0);
	
		//Resultado PAGINADO
		$this->db->from(Notification_model::TABLE.' AS t');
		$this->db->join(Category_model::TABLE.' AS c', 't.category_id=c.id', 'LEFT');
		
		if(isset($select[$query_sort])){
			$this->db->order_by($select[$query_sort], $query_sort_direction);
		}else{
			$this->db->order_by('creation_date', 'DESC');
		}
		$this->db->limit($query_limit, $query_offset);
		
		$query = $this->db->get();
		$result_data = array();
		foreach($query->result() AS $row){
			//Botones de Accion
			$toolbar = '<div class="btn-group">';
			$toolbar.= '<a href="'.base_url('notification/update').'?id='.$row->id.'" class="btn btn-blue btn-sm"><i class="fa fa-edit fa-lg"></i> '.$this->lang->line('edit').'</a>';
			$toolbar.= '<a class="btn btn-danger btn-sm" data-toggle="modal" data-target="#deleteModal" data-id="'.$row->id.'" data-backdrop="true"><i class="fa fa-trash fa-lg"></i> '.$this->lang->line('delete').'</a>';
			$toolbar.= '</div>';
			
			//Fila
			$result_data[]=array(
				$row->title,
				$row->creation_date,
				$row->category_name,
				$toolbar,
			);
		}
		$result_total_filtered = !empty($query_filter) ? count($result_data) : $result_total;
	
		//Respuesta
		$response = array(
			'draw' => intval($query_draw),
			'recordsTotal' => intval($result_total),
			'recordsFiltered' => intval($result_total_filtered),
			'data' => $result_data,
		);
		echo json_encode($response);
	}

}