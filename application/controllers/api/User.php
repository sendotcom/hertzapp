<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/RestController.php';
require APPPATH . '/libraries/SnsUtils.php';
class User extends RestController {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
    }
    
    /**
     * Iniciar sesion
     */
    public function login_post(){
    	//Email
    	$email = $this->post('email');
    	if(empty($email)){
    		$this->response(array(
    			'status' => 404,
    			'message' => sprintf($this->lang->line("api_validation_required"), "email"),
    		));
    	}
    	//Password
    	$password = $this->post('password');
    	if(empty($password)){
    		$this->response(array(
    			'status' => 404,
    			'message' => sprintf($this->lang->line("api_validation_required"), "password"),
    		));
    	}
    	$arn = $this->post('arn');
    	$platform = $this->post('platform');
    	 
    	$user = $this->User_Device_model->login($email, $password);
    	if(!empty($user)){
    		//Si no tiene apikey: se genera
    		if(empty($user->apikey)){
    			$user->apikey = $this->generateRandomString(40);
    			$this->User_Device_model->update($user->id, array('apikey' => $user->apikey));
    		}
    		//Actualiza ARN/Platform
    		if (!empty($arn)) {
    			$this->User_Device_model->updateARN($arn, array(
    				'arn' => ''
    			));
    			$this->User_Device_model->update($user->id, array(
    				'arn' => $arn,
    				'platform' => $platform,
    			));
    		}
    		
    		$this->response(array(
    			'status' => 200,
    			'data' => $user,
    		));
    	}else{
    		$this->response(array(
    			'status' => 400,
    			'message' => $this->lang->line("api_login_error"),
    		));
    	}
    }
    
    /**
     * Registrar
     */
    public function register_post(){
    	//Email
    	$email = $this->post('email');
    	if(empty($email)){
    		$this->response(array(
    			'status' => 404,
    			'message' => sprintf($this->lang->line("api_validation_required"), "email"),
    		));
    	}
    	//Password
    	$password = $this->post('password');
    	if(empty($password)){
    		$this->response(array(
    			'status' => 404,
    			'message' => sprintf($this->lang->line("api_validation_required"), "password"),
    		));
    	}
    	//Nombre
    	$name = $this->post('name');
    	/*if(empty($name)){
    		$this->response(array(
    			'status' => 404,
    			'message' => "name_required",
    		));
    	}*/
    	//Apellidos
    	$surname = $this->post('surname');
    	//ARN
    	$arn = $this->post('arn');
    	//Plataforma
    	$platform = $this->post('platform');
    	if(empty($platform)){
    		$platform = 'android';
    	}
    	 
    	$user = $this->User_Device_model->exist($email);
    	if(empty($user)){
    		//Actualiza ARN/Platform
    		if (!empty($arn)) {
    			$this->User_Device_model->updateARN($arn, array(
    				'arn' => ''
    			));
    		}
    		
    		$id = $this->User_Device_model->insert(array(
    			'name' => $name,
    			'surname' => $surname,
    			'email' => $email,
    			'password' => md5($password),
    			'arn' => $arn,
    			'platform' => $platform,
    			'apikey' => $this->generateRandomString(40),
    		));
    		$this->response(array(
    			'status' => 200,
    			'message' => $this->lang->line("api_register_success"),
    			'data' => $this->User_Device_model->getById($id),
    		), 200);
    	}else{
    		$this->response(array(
    			'status' => 400,
    			'message' => $this->lang->line("api_register_email_exist"),
    		));
    	}
    }
 
    /**
     * Cerrar sesion
     */
    public function session_close_post(){
    	if (!empty($this->_apiuser->arn)){
    		//Quita el token a los usuarios
    		$this->User_Device_model->updateARN($this->_apiuser->arn, array('arn' => ''));
    		
    		//Elimina el token de SNS
    		$sns = new SnsUtils();
    		$sns->deleteEndPoint($this->_apiuser->arn);
    	}
    
    	$this->response(array(
    		'status' => 200,
    	));
    }
    
    /**
     * Recordar contraseña
     */
    public function recover_post()
    {
    	$email = $this->post('email');
    	if (empty($email)) {
    		$this->response(array(
    			'status' => 404,
    			'message' => sprintf($this->lang->line("api_validation_required"), "email"),
    		));
    	}
    
    	$user = $this->User_Device_model->getByEmail($email);
    	if (!empty($user)) {
    		$password = $this->generateRandomString(10);
    		$attr = array();
    		$attr['password'] = md5($password);
    
    		// Actualizar Usuario
    		$this->User_Device_model->update($user->id, $attr);
    
    		//Enviar Email
    		$subject = $this->lang->line("email_recover_subject");
    		$body = $this->lang->line("email_recover");
    		$body = str_replace('#logo#', base_url('images/logo.png'), $body);
    		$body = str_replace('#name#', $user->name, $body);
    		$body = str_replace('#email#', $email, $body);
    		$body = str_replace('#password#', $password, $body);
    		$this->sendEmail($email, $subject, $body);
    		
    		$this->response(array(
    			'status' => 200,
    			'message' => $this->lang->line("api_recover_success"),
    		));
    	}else{
    		$this->response(array(
    			'status' => 400,
    			'message' => $this->lang->line("api_recover_error"),
    		));
    	}
    }
    
}
