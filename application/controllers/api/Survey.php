<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/RestController.php';
class Survey extends RestController {

	/**
	 * Listado
	 */
	public function index_post(){
		$this->response(array(
			'status' => 200,
			'data' => $this->Survey_model->getAll($this->_apiuser->id, $this->_corp_id),
		));
	}
	
	/**
	 * Responder
	 */
	public function answer_post(){
		//Encuesta
		$id_survey = $this->post('id_survey');
		if(empty($id_survey)){
			$this->response(array(
				'status' => 404,
				'message' => sprintf($this->lang->line("api_validation_required"), "id_survey"),
			));
		}
		//Respuestas
		$answers = str_replace("'","\"",$this->post('answers'));
		$answers = json_decode($answers);
		
		$survey = $this->Survey_model->getById($id_survey);
		$exist = $this->Survey_Answer_model->get($this->_apiuser->id, $id_survey);
		if(empty($exist) && !empty($survey)){
			$this->Survey_Answer_model->insert(array(
				'id_survey' => $id_survey,
				'id_user' => $this->_apiuser->id,
				'data' => json_encode($answers), 
			));
			
			//Config
			$config = $this->Config_model->get();
			
			//Email
			$user_name = trim($this->_apiuser->name.' '.$this->_apiuser->surname);
			$html = $this->lang->line("email_survey");
			$html = str_replace('#logo#', base_url('images/logo.png'), $html);
			$html = str_replace('#survey_name#', $survey->title, $html);
			$html = str_replace('#user_name#', $user_name, $html);
			$html_answers = '';
			if(!empty($answers) && is_array($answers)){
				foreach($answers AS $answer){
					$html_answers.="<p><b>$answer->question</b>: $answer->answer</p>";
				}
			}
			$html = str_replace('#answers#', $html_answers, $html);
			$subject = sprintf($this->lang->line("email_survey_subject"), $user_name, $survey->title);
			$this->sendEmail($config->email_polls, $subject, $html);
		}
		
		$this->response(array(
			'status' => 200,
			'message' => $this->lang->line("api_survey_answered"),
		));
	}
	
	/**
	 * Envio de sugerencias
	 */
	public function suggestions_post(){
		//Encuesta
		$text = $this->post('text');
		if(empty($text)){
			$this->response(array(
				'status' => 404,
				'message' => sprintf($this->lang->line("api_validation_required"), "text"),
			));
		}
		
		//Config
		$config = $this->Config_model->get();
		
		//Email
		$user_name = trim($this->_apiuser->name.' '.$this->_apiuser->surname);
		$html = $this->lang->line("email_suggestions");
		$html = str_replace('#logo#', base_url('images/logo.png'), $html);
		$html = str_replace('#user_name#', $user_name, $html);
		$html = str_replace('#suggestions#', $text, $html);
		$subject = sprintf($this->lang->line("email_suggestions_subject"), $user_name);
		$this->sendEmail($config->email_polls, $subject, $html);
	
		$this->response(array(
			'status' => 200,
			'message' => $this->lang->line("api_suggestion_sent"),
		));
	}
	
}
