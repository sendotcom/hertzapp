<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/RestController.php';
class Rent extends RestController {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
    }
    
    /**
     * Busqueda de vehiculos
     */
    public function search_post(){
    	$result = array();
    	$errors = array();
    	$applyCdp = 0;
    	
    	//Session ID
    	$session_id = $this->post('session_id');
    	if(empty($session_id)){
    		$session_id = time().$this->generateRandomString(3, false);
    	}
    	
    	//Desde
    	$location_from = $this->post('location_from');
    	if(empty($location_from)){
    		$this->response(array(
    			'status' => 404,
    			'message' => sprintf($this->lang->line("api_validation_required"), $this->lang->line("start_at")),
    		));
    	}
    	$date_from = $this->post('date_from');
    	if(empty($date_from)){
    		$this->response(array(
    			'status' => 404,
    			'message' => sprintf($this->lang->line("api_validation_required"), $this->lang->line("start_at")),
    		));
    	}
    	
    	//Hasta
    	$location_to = $this->post('location_to');
    	if(empty($location_to)){
    		$this->response(array(
    			'status' => 404,
    			'message' => sprintf($this->lang->line("api_validation_required"), $this->lang->line("end_at")),
    		));
    	}
    	$date_to = $this->post('date_to');
    	if(empty($date_to)){
    		$this->response(array(
    			'status' => 404,
    			'message' => sprintf($this->lang->line("api_validation_required"), $this->lang->line("end_at")),
    		));
    	}
    	
    	//Dias
    	$days = $this->dateDiff($date_from, $date_to);
    	if($days < 1){
    		$this->response(array(
    			'status' => 400,
    			'message' => $this->lang->line("api_rent_dates_error"),
    		));
    	}
    	
    	//Class
    	$class = $this->post('class');
    	
    	//Discount
    	$cdp = $this->post('cdp');
    	if(!empty($cdp)){
    		$cdp = $this->Discount_model->getByCDP($cdp);
    		//Error si no aplica
    		if(empty($cdp)){
    			$errors[]= $this->lang->line("api_rates_error_cdp");
    			$this->response(array(
    				'status' => 400,
    				'session_id' => $session_id,
    				'message' => implode("\n", $errors),
    			));
    		}
    	}else{
    		$cdp = '';
    	}
    	
    	//Solicitud
    	$xmlRequest = '<ResRates>
		    <Pickup locationCode="'.$location_from.'" dateTime="'.str_replace(" ", "T", $date_from).'"/>
		    <Return locationCode="'.$location_to.'" dateTime="'.str_replace(" ", "T", $date_to).'"/>
			';
    	if(!empty($class)){
    		$xmlRequest.= "<Class>$class</Class>";
    	}
		$xmlRequestCdp = $xmlRequest;
		$xmlRequestCdp.= '
			<CorpRateID>'.(!empty($cdp) ? $cdp->cdp : '').'</CorpRateID>
		    <EstimateType>3</EstimateType>
		</ResRates>';
		$xmlRequest.= '
			<CorpRateID>'.$this->_corp_id.'</CorpRateID>
		    <EstimateType>3</EstimateType>
		</ResRates>';
    	
    	//Respuesta y Parseo
    	$response = $this->call($session_id, $xmlRequest);
    	if(!empty($response)){
	    	$xmlResponse = new SimpleXMLElement($response);
	    	if(!empty($xmlResponse)){
	    		$status = $xmlResponse->ResRates['success']=="false" ? false : true;
	    		if($status && isset($xmlResponse->ResRates->Rate)){
	    			//Obtenemos vehiculos de BD
	    			$aux_cars = $this->Car_model->getAll($class);
	    			$cars = array();
	    			foreach($aux_cars AS $car){
	    				$cars[$car->class_id]= $car;
	    			}
	    			
	    			foreach($xmlResponse->ResRates->Rate AS $rate){
	    				//Si esta disponible la tarifa
	    				if(strval($rate->Availability)=='Available'){
		    				$item = new stdClass;
		    				//Detalles de tarifa
		    				$item->class = strval($rate->Class);
		    				$item->rate_id = trim(strval($rate->RateID));
		    				$item->amount = number_format(floatval($rate->Estimate), 2, '.', '');
		    				$item->amount_origin = 0;
		    				$item->price = floatval($rate->RateOnlyEstimate);
		    				$item->drop_charge = floatval($rate->DropCharge);
		    				$item->amount_airport = ($item->price + $item->drop_charge) * TAX_AIRPORT;
		    				$item->amount_vlf = ($item->price + $item->drop_charge) * TAX_VLF;
		    				$item->amount_service = TAX_SERVICE * $days;
		    				$item->amount_vat = number_format(($item->price + $item->drop_charge + $item->amount_airport + $item->amount_vlf + $item->amount_service) * TAX_VAT, 2);
		    				
		    				$item->price = number_format($item->price, 2, '.', '');
		    				$item->drop_charge = number_format($item->drop_charge, 2, '.', '');
		    				$item->amount_airport = number_format($item->amount_airport, 2, '.', '');
		    				$item->amount_vlf = number_format($item->amount_vlf, 2, '.', '');
		    				$item->amount_service = number_format($item->amount_service, 2, '.', '');
		    				
		    				$item->currency = trim(strval($rate->CurrencyCode));
		    				//Detalles del vehiculo
		    				if(isset($cars[$item->class])){
		    					$item->name = $cars[$item->class]->name;
		    					$item->transmission = $cars[$item->class]->transmission;
		    					$item->passengers = $cars[$item->class]->passengers;
		    					$item->doors = $cars[$item->class]->doors;
		    					$item->bags = $cars[$item->class]->bags;
		    					$item->image = !empty($cars[$item->class]->image) ? $cars[$item->class]->image : '';
		    				}else{
		    					$item->name = '';
		    					$item->transmission = 'A';
		    					$item->passengers = '2';
		    					$item->doors = '3';
		    					$item->bags = '2';
		    					$item->image = '';
		    				}
		    				$item->cdp = '';
		    				
		    				//Añadimos a resultados
		    				$result[$item->class]= $item;
	    				}
	    			}
	    			
	    			//Codigo promocional
	    			if(!empty($cdp)){
	    				$response = $this->call($session_id, $xmlRequestCdp);
	    				if(!empty($response)){
	    					$xmlResponse = new SimpleXMLElement($response);
	    					if(!empty($xmlResponse)){
	    						$status = $xmlResponse->ResRates['success']=="false" ? false : true;
	    						if($status && isset($xmlResponse->ResRates->Rate)){
	    							foreach($xmlResponse->ResRates->Rate AS $rate){
	    								$class = strval($rate->Class);
	    								if(strval($rate->Availability)=='Available' 
	    										&& array_key_exists($class, $result)){
	    									$applyCdp = 1;
	    									$item = $result[$class];
	    									
	    									$item->amount_origin = $item->amount;
	    									$item->rate_id = trim(strval($rate->RateID));
	    									$item->amount = number_format(floatval($rate->Estimate), 2, '.', '');
	    									$item->price = floatval($rate->RateOnlyEstimate);
	    									$item->drop_charge = floatval($rate->DropCharge);
	    									$item->amount_airport = ($item->price + $item->drop_charge) * TAX_AIRPORT;
	    									$item->amount_vlf = ($item->price + $item->drop_charge) * TAX_VLF;
	    									$item->amount_service = TAX_SERVICE * $days;
	    									$item->amount_vat = number_format(($item->price + $item->drop_charge + $item->amount_airport + $item->amount_vlf + $item->amount_service) * TAX_VAT, 2);
	    									 
	    									$item->price = number_format($item->price, 2, '.', '');
	    									$item->drop_charge = number_format($item->drop_charge, 2, '.', '');
	    									$item->amount_airport = number_format($item->amount_airport, 2, '.', '');
	    									$item->amount_vlf = number_format($item->amount_vlf, 2, '.', '');
	    									$item->amount_service = number_format($item->amount_service, 2, '.', '');
	    									
	    									$item->cdp = $cdp->description;
	    									
	    									$result[$class]= $item;
	    								}
	    							}
	    						}
	    					}
	    				}
	    				
	    				//Error si no aplica
	    				if(empty($applyCdp)){
	    					$result = array();
	    					$errors[]= $this->lang->line("api_rates_error_cdp");
	    				}
	    			}

	    			//Eliminamos keys
	    			$result = array_values($result);
	    		}else if(!$status){
	    			if(isset($xmlResponse->ResRates->Messages->Message)){
	    				foreach($xmlResponse->ResRates->Messages->Message AS $error){
	    					$error_message = strval($error->Text);
	    					//Error fecha de recogida (apertura oficina)
	    					if($error['number']=='1657'){
	    						$from = substr($error_message, strpos($error_message, 'from')+5, 4);
	    						$to = substr($error_message, strpos($error_message, 'to')+3, 4);
	    						$errors[]= sprintf($this->lang->line("api_rates_error_".$error['number']), $from, $to);
	    					}
	    					
	    					//Error fecha de recogida (cierre oficina)
	    					else if($error['number']=='1658'){
	    						$from = substr($error_message, strpos($error_message, 'open')+5, 4);
	    						$to = substr($error_message, strpos($error_message, 'to')+3, 4);
	    						$errors[]= sprintf($this->lang->line("api_rates_error_".$error['number']), $from, $to);
	    					}
	    					
	    					//Errores traceados traducidos
	    					else if(in_array($error['number'], array('1341','1344','1343', '1346', '9999', '1487'))){
	    						$errors[]= $this->lang->line("api_rates_error_".$error['number']);
	    					}
	    					
	    					//Errores que no se deben mostrar
	    					else if($error['number']=='1358'){
	    						continue;
	    					}
	    					
	    					//Otros errores
	    					else{
	    						$errors[]= $error_message;
	    					}
	    				}
	    			}
	    		}else{
	    			$errors[]= $this->lang->line("api_rent_no_results");
	    		}
	    	}
    	}
    	
    	if(!empty($result)){
    		$this->response(array(
    			'status' => 200,
    			'session_id' => $session_id,
    			'days' => $days,
    			'apply_cdp' => $applyCdp,
    			'data' => $result,
    		));
    	}else{
    		if(empty($errors)){
    			$errors[]= $this->lang->line("api_rent_no_results");
    		}
    		
    		$this->response(array(
    			'status' => 400,
    			'session_id' => $session_id,
    			'message' => implode("\n", $errors),
    		));
    	}
    }
    
    /**
     * Nuevo Alquiler
     */
    public function create_post(){
    	$attr = array(
    		'rented_at' => date('Y-m-d H:i:s'),
    	);
    	$attr_user = array();
    	
    	//Session ID
    	$session_id = $this->post('session_id');
    	if(empty($session_id)){
    		$session_id = time().$this->generateRandomString(3, false);
    	}
    	//Desde
    	$location_from = $this->post('location_from');
    	if(empty($location_from)){
    		$this->response(array(
    			'status' => 404,
    			'message' => sprintf($this->lang->line("api_validation_required"), $this->lang->line("start_at")),
    		));
    	}
    	$date_from = $this->post('date_from');
    	if(empty($date_from)){
    		$this->response(array(
    			'status' => 404,
    			'message' => sprintf($this->lang->line("api_validation_required"), $this->lang->line("start_at")),
    		));
    	}
    	//Hasta
    	$location_to = $this->post('location_to');
    	if(empty($location_to)){
    		$this->response(array(
    			'status' => 404,
    			'message' => sprintf($this->lang->line("api_validation_required"), $this->lang->line("end_at")),
    		));
    	}
    	$date_to = $this->post('date_to');
    	if(empty($date_to)){
    		$this->response(array(
    			'status' => 404,
    			'message' => sprintf($this->lang->line("api_validation_required"), $this->lang->line("end_at")),
    		));
    	}
    	//Clase de vehiculo
    	$class = $this->post('class');
    	if(empty($class)){
    		$this->response(array(
    			'status' => 404,
    			'message' => sprintf($this->lang->line("api_validation_required"), $this->lang->line("car_class")),
    		));
    	}
    	//RateID
    	$rate_id = $this->post('rate_id');
    	if(empty($rate_id)){
    		$this->response(array(
    			'status' => 404,
    			'message' => sprintf($this->lang->line("api_validation_required"), $this->lang->line("price")),
    		));
    	}
    	//Importe
    	$amount = $this->post('amount');
    	if(empty($amount)){
    		$this->response(array(
    			'status' => 404,
    			'message' => sprintf($this->lang->line("api_validation_required"), $this->lang->line("amount")),
    		));
    	}
    	
    	//Importe sin tasas
    	$price = floatval(str_replace(',','',$this->post('price')));
    	if(empty($price)){
    		$this->response(array(
    			'status' => 404,
    			'message' => sprintf($this->lang->line("api_validation_required"), $this->lang->line("price")),
    		));
    	}
    	
    	//Drop Charges
    	$drop_charge = floatval($this->post('drop_charge'));
    	if(empty($drop_charge)){
    		$drop_charge = 0;
    	}
    	
    	//CDP
    	$cdp = floatval($this->post('cdp'));
    	if(empty($cdp)){
    		$cdp = '';
    	}
    	
    	//Dias
    	$days = $this->dateDiff($date_from, $date_to);
    	if($days < 1){
    		$this->response(array(
    			'status' => 400,
    			'message' => $this->lang->line("api_rent_dates_error"),
    		));
    	}
    	
    	//Usuario
    	if(empty($this->_apiuser)){
    		//Email
    		$attr_user['email'] = $this->post('email');
    		if(empty($attr_user['email'])){
    			$this->response(array(
    				'status' => 404,
    				'message' => sprintf($this->lang->line("api_validation_required"), $this->lang->line("email")),
    			));
    		}else{
    			$user = $this->User_Device_model->getByEmail($attr_user['email']);
    			if(!empty($user)){
    				$attr['user_id']= $user->id;
    			}
    		}
    		//Nombre
    		$attr_user['name'] = $this->post('name');
    		if(empty($attr_user['name'])){
    			$this->response(array(
    				'status' => 404,
    				'message' => sprintf($this->lang->line("api_validation_required"), $this->lang->line("name")),
    			));
    		}
    		//Apellidos
    		$attr_user['surname'] = $this->post('surname');
    		if(empty($attr_user['surname'])){
    			$this->response(array(
    				'status' => 404,
    				'message' => sprintf($this->lang->line("api_validation_required"), $this->lang->line("surname")),
    			));
    		}
    	}else{
    		$attr['user_id']= $this->_apiuser->id;
    		$attr_user['email'] = $this->_apiuser->email;
    		$attr_user['name'] = $this->_apiuser->name;
    		$attr_user['surname'] = $this->_apiuser->surname;
    	}
    	
    	//Extras
    	$extras = $this->post("extras");
    	$extras = !empty($extras) ? json_decode($extras) : array();
    	$rent_extras = array();
    	$xml_extras = '';
    	if(!empty($extras) && is_array($extras)){
    		foreach($extras AS $extra_id){
    			$extra = $this->Car_Extra_model->getById($extra_id);
    			if(!empty($extra)){
    				$xml_extras.= '<Option includedInVoucher="false">
						<Code>'.$extra->code.'</Code>
						<Qty>1</Qty>
					</Option>';
    				$rent_extras[]= $extra;
    			}
    		}
    	}
    	
    	//Solicitud
    	$xmlRequest = '
    		<NewReservationRequest confirmAvailability="true">
	    		<Pickup locationCode="'.$location_from.'" dateTime="'.str_replace(" ", "T", $date_from).'"/>
	    		<Return locationCode="'.$location_to.'" dateTime="'.str_replace(" ", "T", $date_to).'"/>
		    	<Source confirmationNumber="'.$session_id.'" countryCode="MX"/>
		    	<Vehicle classCode="'.$class.'"/>
				<Renter>
					<RenterName firstName="'.$attr_user['name'].'" lastName="'.$attr_user['surname'].'"/>
					<Address>
						<Email>'.$attr_user['email'].'</Email>
					</Address>
				</Renter>
				<QuotedRate rateID="'.$rate_id.'" classCode="'.$class.'" corporateRateID="'.$this->_corp_id.'"/>
				'.$xml_extras.'
			</NewReservationRequest>';
    	
    	//Respuesta y Parseo
    	$response = $this->call($session_id, $xmlRequest);
    	if(!empty($response)){
    		$xmlResponse = new SimpleXMLElement($response);
    		if(!empty($xmlResponse) && $xmlResponse->NewReservationResponse['success']=="true"){
    			//Referencia Externa
    			$attr['ref_thermeon']= strval($xmlResponse->NewReservationResponse['reservationNumber']);
    			if(!empty($attr['ref_thermeon'])){
    				//Referencia Interna
    				$attr['ref']= $session_id;
    				//Desde
    				$loc_from = $this->Location_model->getByCode($location_from); 
    				$attr['from_id']= !empty($loc_from) ? $loc_from->id : null;
    				$attr['start_at']= $date_from;
    				//Hasta
    				$loc_to = $this->Location_model->getByCode($location_to);
    				$attr['to_id']= !empty($loc_to) ? $loc_to->id : null;
    				$attr['end_at']= $date_to;
    				//Dias
					$attr['days']= $days;
    				//Vehiculo
    				$car = $this->Car_model->getByClass($class);
    				$attr['car_id']= !empty($car) ? $car->id : null;
    				//Importe
    				$attr['amount']= $amount;
    				$attr['price']= $amount;
    				$attr['discount']= $cdp;    				
    				//Insertar
    				$id = $this->Rent_model->insert($attr);
    				if(!empty($id)){
    					//Eliminar reserva anterior
    					$ref = $this->post('ref');
    					if(!empty($ref)){
    						$rent = $this->Rent_model->getByRef($this->post('ref'), !empty($this->_apiuser) ? $this->_apiuser->id : null);
    						if(!empty($rent)){
    							$this->cancelRent($rent);
    						}
    					}    					
    					
    					//Usuario
    					if(empty($attr['user_id'])){
    						$password = $this->generateRandomString(10);
    						$attr_user['password']= md5($password);
    						$id_user = $this->User_Device_model->insert($attr_user);
    						if(!empty($id_user)){
    							$attr['user_id']= $id_user;
    						}
    						$attr_user['is_new']= true;
    						$attr_user['password']= $password;
    					}else{
    						$attr_user['is_new']= false;
    					}
    				
    					//Extras
    					//$html_extras = "";
						foreach($rent_extras AS $extra){
							$this->Rent_Extra_model->insert(array(
								'rent_id' => $id,
								'extra_id' => $extra->id,
								'price' => $extra->price,
							));
							//Acumulado
							$attr['amount']+= floatval($extra->price * $attr['days']);   								
							//$html_extras.= (!empty($html_extras) ? ', ' : '').$extra->name;
						}
    					
    					//Tasas
    					$attr['amount_price']= floatval($price);
    					$attr['amount_drop_charge']= floatval($drop_charge);
    					$attr['amount_airport']= ($price + $drop_charge) * TAX_AIRPORT;
    					$attr['amount_vlf']= ($price + $drop_charge) * TAX_VLF;
    					$attr['amount_service']= TAX_SERVICE * $days;
    					$attr['amount_vat']= number_format(($attr['amount_price'] + $attr['amount_drop_charge'] + $attr['amount_airport'] + $attr['amount_vlf'] + $attr['amount_service']) * TAX_VAT, 2);
    					
    					$attr['amount_price']= number_format($attr['amount_price'], 2);
    					$attr['amount_drop_charge']= number_format($attr['amount_drop_charge'], 2);
    					$attr['amount_airport']= number_format($attr['amount_airport'], 2);
    					$attr['amount_vlf']= number_format($attr['amount_vlf'], 2);
    					$attr['amount_service']= number_format($attr['amount_service'], 2);
    				
    					//Actualizar
    					$this->Rent_model->update($id, $attr);
    					
    					//Nuevo Email
    					$user = (object) $attr_user;
    					$html = $this->load->view('template/email_rent', array(
    						'user' => $user,
    						'rent' => (object) $attr,
    						'car' => $car,
    						'location_from' => $loc_from,
    						'location_to' => $loc_to,
    					), true);
    					$this->sendEmail($user->email, $this->lang->line("email_confirmation_subject"), $html);
    				
    					//Resultado
    					$this->response(array(
    						'status' => 200,
    						'ref' => $attr['ref_thermeon'],
    					));
    				}
    			}
    		}
    	}
    	
    	//Error
    	$this->response(array(
    		'status' => 400,
    		'message' => $this->lang->line("api_rent_error"),
    	));
    }
    
    /**
     * Mis Alquileres
     */
    public function index_post(){
    	$results = $this->Rent_model->getAll($this->_apiuser->id);
    	foreach($results AS $result){
    		$result->extras = $this->Rent_Extra_model->getAll($result->id, true);
    	}
    	
    	$this->response(array(
    		'status' => 200,
    		'data' => $results,
    	));
    }
    
    /**
     * Cancelar Alquiler
     */
    public function cancel_post(){
    	$rent = $this->Rent_model->getByRef($this->post('ref'), $this->_apiuser->id);
    	if(!empty($rent)){
    		if($this->cancelRent($rent)){
	    		$this->response(array(
	    			'status' => 200,
	    			'message' => $this->lang->line("api_cancel_success"),
	    		));
    		}
    	}
    	
    	//Error
    	$this->response(array(
    		'status' => 400,
    		'message' => $this->lang->line("api_cancel_error"),
    	));
    }
    
    /*
     * Comprueba si el usuario tiene alguna reserva en curso
     */
    public function have_actives_post(){
    	$this->response(array(
    		'status' => 200,
    		'result' => $this->Rent_model->haveActiveRentals($this->_apiuser->id),
    	));
    }
    
    /**
     * Sincroniza las reservas efectivas
     */
    public function sync_get(){
        if ($_SERVER['REMOTE_ADDR'] == IP_SERVER) {
    		$folder = FCPATH."/uploads/import/";
    		if(!LOCAL_ENVIROMENT){
    			$folder = "/home/hertz/home/hertz/";
    		}
    		
    		if(file_exists($folder) && is_dir($folder)){
    			$files = scandir($folder, SCANDIR_SORT_DESCENDING);
                if(!empty($files)){
                    foreach($files AS $file){
    					if(strpos($file, 'aperturas_diarias_') !== false) {
    						file_put_contents(FCPATH.'/application/logs/__sync.txt', "\n".date('Y-m-d H:i:s').": $file", FILE_APPEND);
    						$file = $folder.$file;
    						
                            //Apertura de fichero
    						if (($handle = fopen($file, "r")) !== FALSE) {
    							//$i = 0;
                                while (($row = fgetcsv($handle, 1000, ",")) !== FALSE) {
    								//if($i>=1 && !empty($row[7])){
                                    if($row && !empty($row[5])){
                                        
                                        //Actualiza
    									//$this->Rent_model->updateByRefThermeon(trim(utf8_encode($row[7])), array(
                                        $this->Rent_model->updateByRefThermeon(trim(utf8_encode($row[5])), array(
    										//'contract_number' => trim(utf8_encode($row[6])),
                                            'contract_number' => trim(utf8_encode($row[4])),
    										//'car_number' => trim(utf8_encode($row[8])),
                                            'car_number' => trim(utf8_encode($row[12])),
    									));
    								}
    								//$i++;
    							}
    						}
    						
    						//Elimina fichero
    						unlink($file);
    					}
    				}
    			}
    		}
    	}
    	$this->response(array(
    		'status' => 200,
    	));
    }
    
    /**
     * Cancela una reserva
     * @param object $rent
     * @return boolean
     */
    private function cancelRent($rent){
    	//Solicitud
    	$session_id = time().$this->generateRandomString(3, false);
    	$xmlRequest = '<CancelReservationRequest reservationNumber="'.$rent->ref_thermeon.'"/>';
    	 
    	//Respuesta y Parseo
    	$response = $this->call($session_id, $xmlRequest);
    	if(!empty($response)){
    		$xmlResponse = new SimpleXMLElement($response);
    		if(!empty($xmlResponse)){
    			$status = $xmlResponse->CancelReservationResponse['success']=="false" ? false : true;
    			if($status){
    				$this->Rent_model->update($rent->id, array('status_id' => 2));
    				return true;
    			}
    		}
    	}
    	
    	return false;
    }
    
}
