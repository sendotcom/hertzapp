<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/RestController.php';
class Util extends RestController {

	/**
	 * Informacion de la app
	 */
	public function info_post(){
		$this->response(array(
			'status' => 200,
			'data' => $this->Config_model->get(),
		));
	}
	
	/**
	 * Solicitud de Socorro
	 */
	public function sosrequest_post(){
		$rent = $this->Rent_model->getActiveRent($this->_apiuser->id);
		if(!empty($rent) && !empty($rent->contract_number) && !empty($rent->car_number)){
			//Geotab
			$location = $this->call_geotab($rent->car_number);
			
			//Config
			$config = $this->Config_model->get();
			
			//Email
			$user_name = trim($this->_apiuser->name.' '.$this->_apiuser->surname);
			$html = $this->lang->line("email_sosrequest");
			$html = str_replace('#logo#', base_url('images/logo.png'), $html);
			$html = str_replace('#user_name#', $this->_apiuser->name, $html);
			$html = str_replace('#contract_number#', $rent->contract_number, $html);
			$html = str_replace('#full_name#', $user_name, $html);
			if(!empty($location)){
				$html = str_replace('#lat#', $location->lat, $html);
				$html = str_replace('#lon#', $location->lon, $html);
			}else{
				$html = str_replace('#lat#,#lon#', $this->lang->line('data_not_available'), $html);
			}
			$subject = sprintf($this->lang->line("email_sosrequest_subject"), $user_name);
			$this->sendEmail($config->email_sos, $subject, $html);
			
			$this->response(array(
				'status' => 200,
				'message' => $this->lang->line("api_sosrequest_sent"),
			));
		}
	
		$this->response(array(
			'status' => 405,
			'message' => $this->lang->line("api_sosrequest_error"),
		), 405);
	}
	
	private function call_geotab($vehicle_name){
		if(!empty($vehicle_name)){
			//Login
			$login = json_decode($this->curl_call(GEOTAB_URL_LOGIN, array(
					'method' => 'Authenticate',
					'params' => array(
						'database' => GEOTAB_DB,
						'userName' => GEOTAB_USER,
						'password' => GEOTAB_PWD,
					),
				)
			));
			if(!empty($login)){
				//Obtenemos vehiculo
				$vehicle = json_decode($this->curl_call(GEOTAB_URL, array(
					'method' => 'Get',
					'params' => array(
						'typeName' => "Device",
						'search' => array(
							'name' => "$vehicle_name",
						),
						'credentials' => $login->result->credentials,
					),
				)));
				if(!empty($vehicle) && !empty($vehicle->result) && !empty($vehicle->result[0]->id)){
					//Obtenemos localizacion
					$location = json_decode($this->curl_call(GEOTAB_URL, array(
						'method' => 'Get',
						'params' => array(
							'typeName' => "DeviceStatusInfo",
							'search' => array(
								'deviceSearch' => array(
									'id' => $vehicle->result[0]->id,
								),
							),
							'credentials' => $login->result->credentials,
						),
					)));
					if(!empty($location) && !empty($location->result) && !empty($location->result[0])){
						$info = new stdClass;
						$info->lat = $location->result[0]->latitude;
						$info->lon = $location->result[0]->longitude;
						return $info;
					}
				}
			}
		}
		return false;
	}
	
	private function curl_call($url, $params){
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query(array('JSON-RPC' => json_encode($params))));
		curl_setopt($curl, CURLOPT_HTTPHEADER, array(
			'Accept: application/json',
			'Content-Type: application/x-www-form-urlencoded',
			'Cache-Control: no-cache',
		));
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		$result = curl_exec($curl);
		
		curl_close ($curl);
		return $result;
	}
	
}
