<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/RestController.php';
class Location extends RestController {

	/**
	 * Busqueda de localizaciones
	 */
	public function list_post() {
		//Cadena de busqueda (Opcional)
		$text = $this->post('text');
		
		//Resultado
		$this->response(array(
			'status' => 200,
			'data' => $this->Location_model->getAll($text, $this->_corp_id),
		));
	}
	
	/**
	 * Localizaciones cercanas
	 */
	public function nearly_post(){
		$lat = $this->post('lat');
		if(empty($lat)){
			$this->response(array(
				'status' => 404,
				'message' => "lat_required", 
			));
		}
		$lon = $this->post('lon');
		if(empty($lon)){
			$this->response(array(
				'status' => 404,
				'message' => "lon_required",
			));
		}
		
		$locations = $this->Location_model->getNearly($lat, $lon, $this->_corp_id);
		
		//Resultado
		$this->response(array(
			'status' => 200,
			'data' => $locations,
		));
	}
	
}
