<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/RestController.php';
class Vehicle extends RestController {

	/**
	 * Clases
	 */
	public function classes_post(){
		$this->response(array(
			'status' => 200,
			'data' => $this->Car_Class_model->getAll(),
		));
	}
	
	/**
	 * Extras
	 */
	public function extras_post(){
		$this->response(array(
			'status' => 200,
			'data' => $this->Car_Extra_model->getAll(),
		));
	}
	
}
