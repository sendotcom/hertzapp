<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH . '/libraries/RestController.php';
require APPPATH . '/libraries/SnsUtils.php';
class Sns extends RestController {
	
	private $sns;
	
	public function __construct(){
		parent::__construct();
		$this->sns = new SnsUtils();
	}
	
	/**
	 * Registra un dispositivo para una plataforma y en el topic general
	 */
	public function createEndPoint_post(){
		$token = $this->post('token');
		if(empty($token)){
			$this->response(array(
				'status' => 404,
				'message' => sprintf($this->lang->line("api_validation_required"), "token"),
			));
		}
		$platform = $this->post('platform');
		if(empty($platform)){
			$this->response(array(
				'status' => 404,
				'message' => sprintf($this->lang->line("api_validation_required"), "platform"),
			));
		}
		
		$appArn = '';
		if($platform==SnsUtils::GCM){
			$appArn = SNS_ANDROID;
		}else if($platform=='FF'){
			$appArn = SNS_IOS_FF;
		}else{
			$appArn = SNS_IOS;
		}
		if(!empty($appArn)){
			$response = $this->sns->createPlatformEndpoint($token, $appArn, SNS_TOPIC);
			if($response != false){
				//Si tenemos ARN del Endpoint
				$this->response(array(
					'platform' => $platform,
					'arn' => $response
				), 200);
			}
		}
		
		//Error
		$this->response(array(
			'status' => 200,
			'message' => "No se pudo registrar el dispositivo",
		));
	}
	
	/**
	 * Eliminar endpoint
	 */
	public function deleteEndPoint_post(){
		$arn = $this->post('arn');
		if(empty($arn)){
			$this->response(array(
				'status' => 404,
				'message' => sprintf($this->lang->line("api_validation_required"), "arn"),
			));
		}
		
		$this->sns->deleteEndPoint($arn);
	
		$this->response(array(
			'status' => 200,
		));
	}
	
}