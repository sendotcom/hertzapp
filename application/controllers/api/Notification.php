<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/RestController.php';
class Notification extends RestController {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
    }

    /**
     * Lisado
     */
	public function list_post() {
        $data = array(
        	'notifications' => array(),
        	'deleted' => array(),
        );
		$version_code = $this->post('version_code');

		//Notificaciones
		$notifications = $this->Notification_model->getNotifications($version_code);
		foreach($notifications as $not){
			$not->image = '';
			$imgs = $this->Notification_model->getNotificationImages($not->id);
			foreach($imgs as $img)                    {
				$not->image = $img->path;
			}
			$data['notifications'][]= $not;
		}

		//Notificaciones Eliminadas
		$notifications = $this->Notification_model->getListDeleted();
		foreach($notifications as $d) {
			$data['deleted'][]= $d->id;
		}
		
		$this->response(array(
			'status' => 200,
			'data' => $data,
		));
	}
	
	public function categories_post(){
		$this->response(array(
			'status' => 200,
			'data' => $this->Category_model->getCategories(true),
		));
	}
	
}
