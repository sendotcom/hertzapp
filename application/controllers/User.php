<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/MainController.php';

class User extends MainController {
	
	function __construct()
	{
		// Construct the parent class
		parent::__construct();
		
		//Filtro de usuarios
		if($this->session->userdata('logged_in')->rol_id!=ROL_ADMIN){
			redirect();
		}
	}

	/**
	 * Vista para listar notificaciones
	 */
	public function index(){
		$data = array();

		$this->template->write_view('content', 'user/index', $data, TRUE);
		$this->template->render();
	}

	/**
	 * Vista para crear un usuario
	 */
	public function create(){
		//REGLAS VALIDACION
		$this->load->library('form_validation');
		$this->form_validation->set_rules ('name', $this->lang->line("name"), 'trim|required|max_length[45]');
		$this->form_validation->set_rules ('username', $this->lang->line("email"), 'trim|required|max_length[45]|callback_is_unique');
		$this->form_validation->set_rules ('password', $this->lang->line("password"), 'trim|required|max_length[45]');
		$this->form_validation->set_rules ('rol_id', $this->lang->line("rol"), 'trim|required');
		$this->form_validation->set_message ('required', $this->lang->line('validation_required'));
		$this->form_validation->set_message ('max_length', $this->lang->line('validation_max_length'));

		//PARAMETROS
		$data = array(
			'mode' => 'create',
			'url' => base_url('user/create'),
			'name' => $this->post('name'),
			'username' => $this->post('username'),
			'password' => $this->post('password'),
			'rol_id' => $this->post('rol_id'),
		);

		//GUARDAR
		if($this->form_validation->run () == TRUE){
			//Insertar
			$id = $this->User_model->insert(array(
				'name' => $data['name'],
				'username' => $data['username'],
				'password' => md5($data['password']),
				'rol_id' => $data['rol_id'],
			));

			if(!empty($id)){
				$this->session->set_flashdata('success', $this->lang->line('user_saved'));
				redirect(base_url('user'));
			}
		}
		
		//Roles
		$data['rols'] = $this->Rol_model->getList(false);

		$this->template->write_view('content', 'user/form', $data, TRUE);
		$this->template->render();
	}

	/**
	 * Vista para actualizar un usuario
	 */
	public function update(){
		$id = $this->input->get('id');
		
		//REGLAS VALIDACION
		$this->load->library('form_validation');
		$this->form_validation->set_rules ('name', $this->lang->line("name"), 'trim|required|max_length[45]');
		$this->form_validation->set_rules ('username', $this->lang->line("email"), 'trim|required|max_length[45]');
		$this->form_validation->set_rules ('password', $this->lang->line("password"), 'trim|max_length[45]');
		$this->form_validation->set_rules ('rol_id', $this->lang->line("rol"), 'trim|required');
		$this->form_validation->set_message ('required', $this->lang->line('validation_required'));
		$this->form_validation->set_message ('max_length', $this->lang->line('validation_max_length'));

		//PARAMETROS
		$data = array(
			'mode' => 'update',
			'url' => base_url('user/update?id='.$id),
			'name' => $this->post('name'),
			'username' => $this->post('username'),
			'password' => $this->post('password'),
			'rol_id' => $this->post('rol_id'),
		);

		//GUARDAR
		if($this->form_validation->run () == TRUE){
			$attr = array(
				'name' => $data['name'],
				'username' => $data['username'],
				'rol_id' => $data['rol_id'],
			);
			if(!empty($data['password'])){
				$attr['password']=md5($data['password']);
			}
			
			//Actualizar
			$this->User_model->update($id, $attr);

			$this->session->set_flashdata('success', $this->lang->line('user_saved'));
			redirect(base_url('user'));
		}else{
			//CARGAR DE BD
			$user = $this->User_model->getById($id);
			if(!empty($user)){
				$data['name']=$user->name;
				$data['username']=$user->username;
				$data['rol_id']=$user->rol_id;
			}
		}
		
		//Roles
		$data['rols'] = $this->Rol_model->getList(false);

		$this->template->write_view('content', 'user/form', $data, TRUE);
		$this->template->render();
	}

	/**
	 * Borrar usuario
	 */
	public function delete(){
		$id = $this->post('id');
		$this->User_model->update($id, array(
			'deleted' => 1,
		));
		
		$this->session->set_flashdata('success', $this->lang->line('user_deleted'));
		redirect('user');
	}
	
	/**
	 * Pagina el listado de Notificaciones
	 */
	public function pagination(){
		//PARAMETROS
		$query_filter = $this->input->post('search');
		$query_filter = strtolower($query_filter['value']);
		$query_sort = $this->input->post('order');
		$query_sort_direction = strtolower($query_sort[0]['dir']);
		$query_sort = strtolower($query_sort[0]['column']);
		$query_offset = $this->input->post('start');
		$query_limit = $this->input->post('length');
		$query_draw = $this->input->post('draw');
		
		//Resultado TOTAL
		$this->db->select('COUNT(*) AS total');
		$this->db->where('deleted', 0);
		$query = $this->db->get(User_model::TABLE)->row();
		$result_total_filtered = $result_total =  $query->total;
		
		//FILTROS y SELECT
		if(!empty($query_filter)){
			$this->db->where("(t.id='$query_filter' OR t.username like '%$query_filter%' OR t.name like '%$query_filter%' OR r.name like '%$query_filter%')");
		}
		$select = array('t.name', 't.username', 't.rol_id', 't.id');
		$this->db->select(implode(',', $select).', r.name AS rol_name');
		$this->db->where('deleted', 0);
	
		//Resultado PAGINADO
		$this->db->from(User_model::TABLE.' AS t');
		$this->db->join(Rol_model::TABLE.' AS r', 't.rol_id=r.id', 'LEFT');
		
		if(isset($select[$query_sort])){
			$this->db->order_by($select[$query_sort], $query_sort_direction);
		}else{
			$this->db->order_by('name', 'ASC');
		}
		$this->db->limit($query_limit, $query_offset);
		
		$query = $this->db->get();
		$result_data = array();
		foreach($query->result() AS $row){
			//Botones de Accion
			$toolbar = '<div class="btn-group">';
			$toolbar.= '<a href="'.base_url('user/update').'?id='.$row->id.'" class="btn btn-blue btn-sm"><i class="fa fa-edit fa-lg"></i> '.$this->lang->line('edit').'</a>';
			$toolbar.= '<a class="btn btn-danger btn-sm" data-toggle="modal" data-target="#deleteModal" data-id="'.$row->id.'" data-backdrop="true"><i class="fa fa-trash fa-lg"></i> '.$this->lang->line('delete').'</a>';
			$toolbar.= '</div>';
			
			//Fila
			$result_data[]=array(
				$row->name,
				$row->username,
				$row->rol_name,
				$toolbar,
			);
		}
		$result_total_filtered = !empty($query_filter) ? count($result_data) : $result_total;
	
		//Respuesta
		$response = array(
			'draw' => intval($query_draw),
			'recordsTotal' => intval($result_total),
			'recordsFiltered' => intval($result_total_filtered),
			'data' => $result_data,
		);
		echo json_encode($response);
	}
	
	/**
	 * Validacion Email unico
	 * @param string $username
	 */
	public function is_unique($email){
		$id = isset($_GET['id']) ? $_GET['id'] : null;
	
		$user = $this->User_model->exist($email, $id);
		if($user){
			$this->form_validation->set_message('is_unique', str_replace('%1', $this->lang->line("email"), $this->lang->line('validation_exist')));
			return false;
		}else
			return true;
	}

}