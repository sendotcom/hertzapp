<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH.'/libraries/MainController.php';
class Car extends MainController {
	
	private $class_name = 'car';
	
	/**
	 * Lista
	 */
	public function index()
	{
		$this->data['class_name']= $this->class_name;
		
		$this->template->write_view('content', "$this->class_name/index", $this->data, TRUE);
		$this->template->render();
	}
	
	/**
	 * Nuevo
	 */
	public function create(){
		//REGLAS VALIDACION
		$this->load->library('form_validation');
		$this->form_validation->set_rules ('name', $this->lang->line("name"), 'trim|required|max_length[65]');
		$this->form_validation->set_rules ('transmission', $this->lang->line("transmission"), 'trim|required');
		$this->form_validation->set_rules ('class_id', $this->lang->line("class"), 'trim');
		$this->form_validation->set_rules ('sipcode', $this->lang->line("code"), 'trim|max_length[20]');
		$this->form_validation->set_rules ('doors', $this->lang->line("doors"), 'trim|integer|required');
		$this->form_validation->set_rules ('passengers', $this->lang->line("passengers"), 'trim|integer|required');
		$this->form_validation->set_rules ('bags', $this->lang->line("bags"), 'trim|integer|required');
		$this->form_validation->set_message ('required', $this->lang->line("validation_required"));
		$this->form_validation->set_message ('max_length', $this->lang->line("validation_max_length"));
	
		//PARAMETROS
		$this->data['mode']= 'create';
		$this->data['url']= base_url("/$this->class_name/create");
		$this->data['name']= $this->post('name');
		$this->data['transmission']= $this->post('transmission');
		$this->data['class_id']= $this->post('class_id');
		$this->data['sipcode']= $this->post('sipcode');
		$this->data['doors']= $this->post('doors');
		$this->data['passengers']= $this->post('passengers');
		$this->data['bags']= $this->post('bags');
		$this->data['class_name']= $this->class_name;
	
		//GUARDAR
		if(!empty($_POST)){
			if($this->form_validation->run () == TRUE){
				//Insertar
				$id = $this->Car_model->insert(array(
					'name' => $this->data['name'],
					'transmission' => $this->data['transmission'],
					'class_id' => $this->data['class_id'],
					'sipcode' => $this->data['sipcode'],
					'doors' => $this->data['doors'],
					'passengers' => $this->data['passengers'],
					'bags' => $this->data['bags'],
				));
				
				if(!empty($id)){
					//Imagen
					if (!empty($_FILES)) {
						//Ruta Destino
						$public_dir = "/uploads/cars/".md5($id)."/";
						$real_path = FCPATH.$public_dir;
						$this->checkDir($real_path);
					
						$file = $_FILES['file']['name'];
						if(!empty($file) && $this->security->xss_clean($file, TRUE)){
							//Mover imagen
							$file_name = md5(date('YmdHis').$this->generateRandomString(3)).'.'.pathinfo($file, PATHINFO_EXTENSION);
							$r = move_uploaded_file($_FILES['file']['tmp_name'], $real_path.$file_name);
							if($r==1){
								$this->Car_model->update($id, array('image' => $public_dir.$file_name));
							}
						}
					}
					
					$this->session->set_flashdata('success', $this->lang->line("car_saved"));
					redirect("/$this->class_name");
				}
			}
		}
		//Clases
		$this->data['classes']= $this->Car_Class_model->getAll(false);
		
		$this->template->write_view('content', "/$this->class_name/form", $this->data, TRUE);
		$this->template->render();
	}
	
	/**
	 * Editar
	 */
	public function update(){
		$car = $this->Car_model->getById($this->get('id'));
		if(empty($car)){
			redirect("/$this->class_name");
		}
		
		//REGLAS VALIDACION
		$this->load->library('form_validation');
		$this->form_validation->set_rules ('name', $this->lang->line("name"), 'trim|required|max_length[65]');
		$this->form_validation->set_rules ('transmission', $this->lang->line("transmission"), 'trim|required');
		$this->form_validation->set_rules ('class_id', $this->lang->line("class"), 'trim');
		$this->form_validation->set_rules ('sipcode', $this->lang->line("code"), 'trim|max_length[20]');
		$this->form_validation->set_rules ('doors', $this->lang->line("doors"), 'trim|integer|required');
		$this->form_validation->set_rules ('passengers', $this->lang->line("passengers"), 'trim|integer|required');
		$this->form_validation->set_rules ('bags', $this->lang->line("bags"), 'trim|integer|required');
		$this->form_validation->set_message ('required', $this->lang->line("validation_required"));
		$this->form_validation->set_message ('max_length', $this->lang->line("validation_max_length"));
		
		//PARAMETROS
		$this->data['mode']= 'update';
		$this->data['url']= base_url("/$this->class_name/update?id=".md5($car->id));
		$this->data['name']= $this->post('name');
		$this->data['transmission']= $this->post('transmission');
		$this->data['class_id']= $this->post('class_id');
		$this->data['sipcode']= $this->post('sipcode');
		$this->data['doors']= $this->post('doors');
		$this->data['passengers']= $this->post('passengers');
		$this->data['bags']= $this->post('bags');
		$this->data['class_name']= $this->class_name;
		
		//GUARDAR
		if(!empty($_POST)){
			if($this->form_validation->run () == TRUE){
				$attr = array(
					'name' => $this->data['name'],
					'transmission' => $this->data['transmission'],
					'class_id' => $this->data['class_id'],
					'sipcode' => $this->data['sipcode'],
					'doors' => $this->data['doors'],
					'passengers' => $this->data['passengers'],
					'bags' => $this->data['bags'],
				);
		
				//Imagen
				if (!empty($_FILES)) {
					//Ruta Destino
					$public_dir = "/uploads/cars/".md5($car->id)."/";
					$real_path = FCPATH.$public_dir;
					$this->checkDir($real_path);
						
					$file = $_FILES['file']['name'];
					if(!empty($file) && $this->security->xss_clean($file, TRUE)){
						//Mover imagen
						$file_name = md5(date('YmdHis').$this->generateRandomString(3)).'.'.pathinfo($file, PATHINFO_EXTENSION);
						$r = move_uploaded_file($_FILES['file']['tmp_name'], $real_path.$file_name);
						if($r==1){
							$attr['image']= $public_dir.$file_name;
						}
					}
				}
				
				//Actualizar
				$this->Car_model->update($car->id, $attr);
				
				$this->session->set_flashdata('success', $this->lang->line("car_saved"));
				redirect("/$this->class_name");
			}
		}else{
			$this->data['name']= $car->name;
			$this->data['transmission']= $car->transmission;
			$this->data['class_id']= $car->class_id;
			$this->data['sipcode']= $car->sipcode;
			$this->data['doors']= $car->doors;
			$this->data['passengers']= $car->passengers;
			$this->data['bags']= $car->bags;
			$this->data['image']= $car->image;
		}
		//Clases
		$this->data['classes']= $this->Car_Class_model->getAll(false);
		
		$this->template->write_view('content', "/$this->class_name/form", $this->data, TRUE);
		$this->template->render();
	}
	
	/**
	 * Elimina
	 */
	public function delete(){
		$car = $this->Car_model->getById($this->get('id'));
		if(!empty($car)){
			$this->Car_model->update($car->id, array('deleted' => 1));
		}
				
		$this->session->set_flashdata('success', $this->lang->line("car_deleted"));
		redirect("/$this->class_name");
	}
	
	/**
	 * Importar
	 */
	public function import(){
		//PARAMETROS
		$data = array(
			'url' => base_url("$this->class_name/import"),
			'class_name' => $this->class_name,
		);
	
		if(!empty($_FILES['file'])){
			$file = $_FILES['file']['name'];
			if(!empty($file) && $this->security->xss_clean($file, TRUE)){
	
				$imported = 0;
	
				//Apertura de fichero
				if (($handle = fopen($_FILES['file']['tmp_name'], "r")) !== FALSE) {
					$i = 0;
					$classes = $this->Car_Class_model->getAll(false);
					while (($row = fgetcsv($handle, 1000, ";")) !== FALSE) {
						if($i>=1 && $i<=1000){
							$class = trim(utf8_encode($row[0]));
							if(!array_key_exists($class, $classes)){
								$this->Car_Class_model->insert(array(
									'id' => $class,
									'name' => trim(utf8_encode($row[7])),
								));
								$classes[$class]= $class; 
							}
							
							$attr = array(
								'class_id' => $class,
								'sipcode' => trim(utf8_encode($row[1])),
								'name' => trim(utf8_encode($row[2])),
								'transmission' => trim(utf8_encode($row[3])),
								'doors' => intval(trim(utf8_encode($row[4]))),
								'passengers' => intval(trim(utf8_encode($row[5]))),
								'bags' => intval(trim(utf8_encode($row[6]))),
								'deleted' => intval(trim(utf8_encode($row[9]))),
							);
								
							$item = $this->Car_model->getByClass($class);
							$id = null;
							if(!empty($item)){
								$this->Car_model->update($item->id, $attr);
								$id = $item->id;
							}else{
								$id = $this->Car_model->insert($attr);
							}
							
							if(!empty($id)){
								$imported++;
								$image = trim(utf8_encode($row[8]));
								if(!empty($image)){
									$image = file_get_contents($image);
									if(!empty($image)){
										//Ruta Destino
										$public_dir = "/uploads/cars/".md5($id)."/";
										$real_path = FCPATH.$public_dir;
										$this->checkDir($real_path);
										
										$filename = md5(date('YmdHis').$this->generateRandomString(3)).'.png';
										file_put_contents($real_path.$filename, $image, FILE_APPEND);
										$this->Car_model->update($id, array(
											'image' => $public_dir.$filename
										));
									}
								}
							}
						}
						$i++;
					}
						
					$this->session->set_flashdata('success', sprintf($this->lang->line("cars_imported"), $imported, $i-1));
					redirect("$this->class_name/import");
				}
			}
		}
	
		$this->template->write_view('content', "$this->class_name/import", $data, TRUE);
		$this->template->render();
	}
	
	/**
	 * Formato para importar
	 */
	public function import_format(){
		$public_dir = "/uploads/tmp/";
		$real_path = FCPATH.$public_dir;
		$this->checkDir($real_path);
		$file_name = 'importador_autos_'.time().'.csv';
	
		$fp = fopen($real_path.$file_name, 'w');
	
		fputcsv($fp, [
			utf8_decode('CATEGORÍA'),
			utf8_decode('SIPCODE'),
			utf8_decode('NOMBRE'),
			utf8_decode('TRANSMISIÓN'),
			utf8_decode('PUERTAS'),
			utf8_decode('PASAJEROS'),
			utf8_decode('MALETAS'),
			utf8_decode('TIPO'),
			utf8_decode('IMAGEN'),
			utf8_decode('ELIMINAR'),
		], ';');
	
		fclose($fp);
		redirect(base_url($public_dir.$file_name));
	}
	
	/**
	 * Paginacion listado
	 */
	public function pagination(){
		//PARAMETROS
		$query_filter = $this->input->post('search');
		$query_filter = strtolower($query_filter['value']);
		$query_sort = $this->input->post('order');
		$query_sort_direction = strtolower($query_sort[0]['dir']);
		$query_sort = strtolower($query_sort[0]['column']);
		$query_offset = $this->input->post('start');
		$query_limit = $this->input->post('length');
		$query_draw = $this->input->post('draw');
	
		//Resultado TOTAL
		$result_total = $this->db
			->select('COUNT(*) AS total')
			->where('deleted', 0)
			->get(Car_model::TABLE)
			->row();
		$result_total_filtered = $result_total = $result_total->total;
	
		//FILTROS y SELECT
		if(!empty($query_filter)){
			$this->db->where("(t.name like '%$query_filter%' 
					OR t.sipcode like '%$query_filter%'
					OR t.class_id like '%$query_filter%'
					OR c.name like '%$query_filter%')");
		}
		$select = array('t.name', 't.sipcode', 't.class_id', 't.id');
		$this->db->select(implode(',', $select).', c.name AS class_name');
		$this->db->where('t.deleted', 0);
	
		//Resultado PAGINADO
		$this->db->from(Car_model::TABLE.' AS t');
		$this->db->join(Car_Class_model::TABLE.' AS c', "c.id=t.class_id", 'LEFT');
		if(isset($select[$query_sort])){
			$this->db->order_by($select[$query_sort], $query_sort_direction);
		}else{
			$this->db->order_by('t.name', 'ASC');
		}
		$this->db->limit($query_limit, $query_offset);
		$query = $this->db->get();
	
		$result_data = array();
		foreach($query->result() AS $row){
			//Botones de Accion
			$toolbar = '<div class="btn-group">';
			$toolbar.= '<a href="'.base_url("$this->class_name/update").'?id='.md5($row->id).'" class="btn btn-info btn-sm" title="'.$this->lang->line("edit").'"><i class="fa fa-edit fa-lg"></i> '.$this->lang->line('edit').'</a>';
			$toolbar.= '<a class="btn btn-danger btn-sm btn-confirm-delete" data-toggle="modal" data-target="#deleteModal" data-action="'.base_url("$this->class_name/delete")."?id=".$row->id.'" data-backdrop="true" title="'.$this->lang->line("delete").'"><i class="fa fa-trash fa-lg"></i> '.$this->lang->line('delete').'</a>';
			$toolbar.= '</div>';
	
			//Fila
			$result_data[]=array(
				$row->name,
				$row->sipcode,
				"$row->class_id - $row->class_name",
				$toolbar,
			);
		}
		$result_total_filtered = !empty($query_filter) ? count($result_data) : $result_total;
	
		//Respuesta
		echo json_encode(array(
			'draw' => intval($query_draw),
			'recordsTotal' => intval($result_total),
			'recordsFiltered' => intval($result_total_filtered),
			'data' => $result_data,
		));
	}
	
}