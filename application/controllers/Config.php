<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/MainController.php';

class Config extends MainController {
	
	private $class_name = 'config';

	/**
	 * Editar Politicas
	 */
	public function index(){
		$config = $this->Config_model->get();
		if(empty($config)){
			redirect("/");
		}
		
		//REGLAS VALIDACION
		$this->load->library('form_validation');
		$this->form_validation->set_rules ('privacy', $this->lang->line("privacy"), 'trim');
		$this->form_validation->set_rules ('terms', $this->lang->line("terms"), 'trim');
		$this->form_validation->set_message ('required', $this->lang->line('validation_required'));
		$this->form_validation->set_message ('max_length', $this->lang->line('validation_max_length'));
		
		//PARAMETROS
		$this->data['url']= base_url("$this->class_name");
		$this->data['privacy']= $this->post('privacy', false);
		$this->data['terms']= $this->post('terms', false);
		$this->data['class_name']= $this->class_name;
		
		//GUARDAR
		if(!empty($_POST)){
			if($this->form_validation->run () == TRUE){
				$this->Config_model->update($config->id, array(
					'privacy' => $this->data['privacy'],
					'terms' => $this->data['terms'],
				));
		
				$this->session->set_flashdata('success', $this->lang->line('information_saved'));
			}
		}else{
			//CARGAR DE BD
			$this->data['privacy']= $config->privacy;
			$this->data['terms']= $config->terms;
		}

		$this->template->write_view('content', "$this->class_name/index", $this->data, TRUE);
		$this->template->render();
	}
	
	/**
	 * Editar Emails
	 */
	public function email(){
		$config = $this->Config_model->get();
		if(empty($config)){
			redirect("/");
		}
	
		//REGLAS VALIDACION
		$this->load->library('form_validation');
		$this->form_validation->set_rules ('email_polls', $this->lang->line("email_polls"), 'trim|required|max_length[45]|valid_email');
		$this->form_validation->set_rules ('email_sos', $this->lang->line("email_sos"), 'trim|required|max_length[45]|valid_email');
		$this->form_validation->set_message ('required', $this->lang->line('validation_required'));
		$this->form_validation->set_message ('max_length', $this->lang->line('validation_max_length'));
	
		//PARAMETROS
		$this->data['url']= base_url("$this->class_name/email");
		$this->data['email_polls']= $this->post('email_polls');
		$this->data['email_sos']= $this->post('email_sos');
		$this->data['class_name']= $this->class_name;
	
		//GUARDAR
		if(!empty($_POST)){
			if($this->form_validation->run () == TRUE){
				$this->Config_model->update($config->id, array(
					'email_polls' => $this->data['email_polls'],
					'email_sos' => $this->data['email_sos'],
				));
	
				$this->session->set_flashdata('success', $this->lang->line('information_saved'));
			}
		}else{
			//CARGAR DE BD
			$this->data['email_polls']= $config->email_polls;
			$this->data['email_sos']= $config->email_sos;
		}
	
		$this->template->write_view('content', "$this->class_name/email", $this->data, TRUE);
		$this->template->render();
	}

}